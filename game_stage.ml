(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open Config;;
open Video;;
open Object;;
open Interface;;
open Event;;
open Iface_event;;
open Stage;;
open Network;;

open Game_main;;
open Game_interface;;
open Game_cursors;;
open Game_select;;
open Game_object;;
open Game_event;;
open Game_map;;
open Game_init;;

(** Stage objects class definitions *)


let stages=stages_create curs;;

(* this must be in a game_stage.ml *)
class stage_stats iam vict resource boucherie comb =
object
  inherit stage curs
  val mutable iface=new interface "medias/interface/bgs/stats-bg.png" 800 600 
  val mutable title_fnt=new font_object "medias/fonts/samos.ttf" 48 
  val mutable fnt=new font_object "medias/fonts/samos.ttf" 32
  val mutable title=
    (if vict==true then "Victoire des " else "Defaite des ")^(if iam=1 then "Rebelles !" else "NeoNazs !")

    method  on_load()=
	iface#add_object_n ("title_stats") (new iface_label_static title_fnt (0,0,0) title);
	let obj=(iface#get_object_char ("title_stats")) in
	obj#move (f_size_w 100) (f_size_h 60);
	obj#show();

	iface#add_object_n ("resource_s") (new iface_label_static fnt (0,0,0) (string_of_int resource));
	let obj=(iface#get_object_char ("resource_s")) in
	obj#move (f_size_w 125) (f_size_h 505);
	obj#show();

(*	iface#add_object_n ("boucherie_s") (new iface_label_static fnt (0,0,0) ((string_of_int boucherie)^"%"));
	let obj=(iface#get_object_char ("boucherie_s")) in
	obj#move (f_size_w 100) (f_size_h 20);
*)
	iface#add_object_n ("combo_s") (new iface_label_static fnt (0,0,0) (string_of_int comb#get_combo));
	let obj=(iface#get_object_char ("combo_s")) in
	obj#move (f_size_w 620) (f_size_h 165);
	obj#show();

	iface#add_object_n ("serial_killer_s") (new iface_label_static fnt (0,0,0) (string_of_int comb#get_serial_killer));
	let obj=(iface#get_object_char ("serial_killer_s")) in
	obj#move (f_size_w 620) (f_size_h 200);
	obj#show();

	iface#add_object_n ("megakill_s") (new iface_label_static fnt (0,0,0) (string_of_int comb#get_megakill));
	let obj=(iface#get_object_char ("megakill_s")) in
	obj#move (f_size_w 620) (f_size_h 240);
	obj#show();

	iface#add_object_n ("barbarian_s") (new iface_label_static fnt (0,0,0) (string_of_int comb#get_barbarian));
	let obj=(iface#get_object_char ("barbarian_s")) in
	obj#move (f_size_w 620) (f_size_h 275);
	obj#show();

	iface#add_object_n ("butchery_s") (new iface_label_static fnt (0,0,0) (string_of_int comb#get_butchery));
	let obj=(iface#get_object_char ("butchery_s")) in
	obj#move (f_size_w 620) (f_size_h 310);
	obj#show();

	iface#add_object (new iface_label_static fnt (0,0,0) "RETOUR");
	let nbut=iface#get_cur_obj - 1 in
	let prev_but=(iface#get_object_num nbut) in
	prev_but#move (f_size_w 550) (f_size_h 500);
	prev_but#set_release (function()->(
	  fadeout();
	  stage_continue stages "play_menu"));
	prev_but#show();

	iface#update(); 
	fadein();



     method on_loop()=
	clear_iface_ev_stack curs iface;
	iface#update(); 
	curs#put();    
	flip();
end;;


class stage_game p w h m t=
  object
    inherit stage curs
    val mutable chans=(stdin,stdout) 


    val mutable gm=
      let charge()=
	fadeout();	
	let fnt1=new font_object "medias/fonts/samos.ttf" 48 in
	let txt=fnt1#create_text (n("Loading")) (255,255,255) in
	tile_put txt ((!scr_w - (tile_get_w txt))/2) ((!scr_h - (tile_get_h txt))/2);
	tile_free txt;
	fadein();	
	flip() in
      

      match m with 
      | 0 -> charge();game_map_freestyle_init w h p t
      | 1 -> charge();(let gm=game_map_mission_init "medias/missions/mission1.png" 96 64 p in game_set_cond_mission1 gm;gm)
      | 2 -> charge();game_map_network_init w h p t 
      | _ -> charge();game_map_freestyle_init 64 64 p t;

    val mutable mt=m

    val mutable lose=false
    val mutable win=false
    val mutable fnt1=new font_object "medias/fonts/samos.ttf" 12
    val mutable fnt2=new font_object "medias/fonts/samos.ttf" 48

    val mutable up_f=0
    val mutable up_c=0;
    val mutable up_net=0

     method on_leave()=
	fadeout();

      method on_reinit()=
	print_string "free btile";print_newline();
	gm#get_tile_layer#free_btile();

      method on_load()=	

	if m=2 then
	  chans<-client_connect (get_my_addr()) 5000;    

	gm#get_iface#add_object (new iface_button "medias/interface/buttons/menu_button.png" 82 20);
	let nbut=gm#get_iface#get_cur_obj - 1 in
	let if_but=(gm#get_iface#get_object_num nbut) in
	if_but#move (f_size_w 76) (f_size_h 1);
	if_but#show();
	
	if_but#set_release (
	  function()->(	  
	    match mt with
	      |0->  stage_continue stages "game_freestyle_menu"
	      |1->();
	      |2->  stage_continue stages "game_network_menu"
	      |_->();
	  ));	

	(
	  match mt with 
	    | 0 -> gm#update_freestyle();
	    | 1 -> gm#update_freestyle();
	    | 2 -> gm#sync_network chans; 
	    | _ -> gm#update_freestyle();
	);

 	gm#update_graphic(); 
	fadein();

      method on_loop()=
      let t1=ref 0 and
	  t2=ref 0 in
      let toc1=ref 0 and
	  toc2=ref 0 in

      toc1:=time_get();      
      clear_ev_stack gm;
	   
      (* here come (later) IA and network from other player *)


(*      gm#update_freestyle(); *)
      let t=ref 0 in
	  
	timeget_init t;
      (
	match mt with 
	  | 0 -> gm#update_freestyle();
	  | 1 -> gm#update_freestyle();
	  | 2 -> gm#update_main();
	  | _ -> gm#update_freestyle();
      );
(*      timeget_result "update_map : " t;*)

      if mt=2 then (
	if up_net=40 then (
	  print_string "CLIENT: sync network";print_newline();	  
	  gm#sync_network chans;
	  up_net<-0;
	)
	else up_net<-up_net+1;
      );

      lose<-gm#get_lose_cond();
      win<-gm#get_win_cond();
	   
      if lose=true || win=true then
	 (
	   let txt=ref "" in
	   if win=true then txt:="VICTOIRE";
	   if lose=true then txt:="DEFAITE";
	   fadeout();
	   let dtxt=tile_text fnt2#get_font (!txt) (255,255,255) in
	   tile_put dtxt (( !scr_w - (tile_get_w dtxt))/2) (( !scr_h - (tile_get_h dtxt))/2);
	   tile_free dtxt;
	   fadeout();
	   let resource=if gm#get_iam_string="rebel" then "nature" 
	   else if gm#get_iam_string="neonaz" then "pollution" else "none" in
	   stage_add stages "stats" (new stage_stats gm#get_iam win ((gm#get_player_layer gm#get_iam)#resource_get resource) ((gm#get_player_layer gm#get_iam)#resource_get "boucherie") gm#get_comb);
	   stage_load stages "stats";			      
	  );
      
(*
gm#update_graphic();
curs#put();
flip();
*)

      toc2:=time_get();      
      t1:=time_get();      

      if up_c>=up_f then ( 
	  timeget_init t;
          gm#update_graphic(); 	  
(*	  timeget_result "update_graphic : " t; *)
	  up_c<-0;
      )
      else
	up_c<-up_c+1;
      
      curs#put();    
      
      if up_c=0 then
	flip();
      
      t2:=time_get();


      let fps_time=1000/ !fps in
	
	if (!t2 - !t1)<>0 then (
	  if (!t2 - !t1)<(fps_time * 4) then up_f<-3;
	  if (!t2 - !t1)<(fps_time * 3) then up_f<-2;
	  if (!t2 - !t1)<(fps_time * 2) then up_f<-1;
	  if (!t2 - !t1)<fps_time then up_f<-0;
	);
	
	if up_f=0 then
	  (
	    if ((!toc2 - !toc1) + (!t2 - !t1))<fps_time then
	      (* time_sleep(fps_time - ((!toc2 - !toc1) + (!t2 - !t1))); *)

	      frame_delay();
	  );

	
	
  end;;

(*
class stage_loading=
object
    inherit stage
    val mutable fnt1=new font_object "medias/fonts/samos.ttf" 48
    initializer
      on_reinit<-(function()->(
       ));

      on_leave<-(function()->(
	fadeout();
       ));

      on_load<-(function()->(
	let txt=fnt1#create_text "Chargement" (255,255,255) in
	tile_put txt (( !scr_w - (tile_get_w txt))/2) (( !scr_h - (tile_get_h txt))/2);
	tile_free txt;
	fadein();	
	flip();
	stage_add "game" (new stage_game 1);
	stage_load "game";
       ));
      on_loop<-(function()->(

	flip();
       ));
end;;
*)

class stage_intro=
  object
    inherit stage curs
    val mutable text_intro=
      let txt=
	"Il etait une fois un monde merveilleux\n
	  Ou vivait des gens heureux\n
	  Dans ce pays, qu'on nommait Rashitoul,\n
	  La nature etait reine et le paysage ...";

	      in
      let fnt=new font_object "medias/fonts/samos.ttf" 18 in
      new iface_text fnt (255,255,255) txt
    val mutable yp= !scr_h

     method on_leave()=
	fadeout();

     method on_load()=
	fadein();

     method on_loop()=
	video_blank_screen();
	text_intro#move 100 yp;
	text_intro#put ();
	yp<-yp-1;
	flip();

end;;

class stage_quit=
object
  inherit stage curs
    method on_load()=
      bfr_exit()
    
    method on_continue()=
      bfr_exit()
end;;

class stage_movie vid from=
  object
    inherit stage curs

      method on_leave()=
	fadeout();

      method on_continue()=
	if  (Sys.file_exists vid) then ( 
	  let m=movie_load vid in
	  movie_play m;
	  movie_free m;
	 );
	stage_continue stages from;

      method on_load()=
	if  (Sys.file_exists vid) then ( 
	  let m=movie_load vid in
	  movie_play m;
	  movie_free m;
	 );
	stage_load stages from;

end;;



class stage_interlude_mission1=
object
  inherit stage curs
  val mutable iface=new interface "medias/missions/mission1-bg.png" 800 600 
  val mutable fnt=new font_object "medias/fonts/samos.ttf" 18 

      method on_load()=
	fadein();
	iface#add_object_n ("mission1_text") (new iface_text fnt (255,0,0) 
	    "Apr�s son arriv�e sur Terre, J.C Salamon\n
	      se retrouve poursuivi par les n�o-naz.\n
	      Afin de les semer, vous devez suivre un ancien\n
	      chemin que vous connaissiez avant l'arriv�e des Toui"
	    );
	let obj=(iface#get_object_char ("mission1_text")) in
	obj#move (f_size_w 100) (f_size_h 200);
	obj#show();

	iface#add_object_n ("mission1_text2") (new iface_text fnt (0,0,0) 
	    "Apr�s son arriv�e sur Terre, J.C Salamon\n
	      se retrouve poursuivi par les n�o-naz.\n
	      Afin de les semer, vous devez suivre un ancien\n
	      chemin que vous connaissiez avant l'arriv�e des Toui");

	let obj=(iface#get_object_char ("mission1_text2")) in
	obj#move (f_size_w 102) (f_size_h 202);

	obj#show();

      method on_loop()=
	clear_iface_ev_stack curs iface;
	iface#update(); 
	curs#put();    
	flip();

end;;






class stage_option_game_menu=
  object 
    inherit stage curs
    val mutable iface=new interface "medias/interface/bgs/intro_bg-bot2.png" 800 600 
    val mutable fnt1=new font_object "medias/fonts/samos.ttf" 18
    val mutable arbre2=new graphic_scr_resized_object 326 542 "medias/interface/bgs/arbre2.png" false false


      method on_leave()=
	clear_stack();
	fadeout();
      
      method on_load()=
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	

        (* buttons decl start*)

	iface#add_object_n "opt_game1" (new iface_checkbox "medias/interface/buttons/checkbox.png" fnt1 (n("Recognize team")));
	let nbut=iface#get_cur_obj - 1 in
	let o1_but=(iface#get_object_num nbut) in
        o1_but#set_data this_config.game_opt1;
	o1_but#move (f_size_w 128) (f_size_h 64);   
	o1_but#set_click (function()->(
	 ));

	iface#add_object_n "opt_game2" (new iface_checkbox "medias/interface/buttons/checkbox.png" fnt1 (n("Lifebar")) ) ;
	let nbut=iface#get_cur_obj - 1 in
	let o2_but=(iface#get_object_num nbut) in
        o2_but#set_data this_config.game_opt2;
	o2_but#move (f_size_w 128) (f_size_h 128);   
	o2_but#set_click (function()->(
	 ));

	iface#add_object_n "opt_game3" (new iface_checkbox "medias/interface/buttons/checkbox.png" fnt1 (n("In game help")) ) ;
	let nbut=iface#get_cur_obj - 1 in
	let o3_but=(iface#get_object_num nbut) in
        o3_but#set_data this_config.game_opt3;
	o3_but#move (f_size_w 128) (f_size_h 192);   
	o3_but#set_click (function()->(
	 ));

	iface#add_object (new iface_label_static fnt1 (255,255,255) (n("Ok")));
	let nbut=iface#get_cur_obj - 1 in
	let prev_but=(iface#get_object_num nbut) in
	prev_but#move (f_size_w 128) (f_size_h 272);
	prev_but#set_release (function()->(
          this_config.game_opt1<-o1_but#get_data;
          this_config.game_opt2<-o2_but#get_data;
          this_config.game_opt3<-o3_but#get_data;

	  fadeout();          
	  stage_continue stages "option_menu"));

	(* buttons decl end *)

	o1_but#show();
	o2_but#show();
	o3_but#show();
	prev_but#show();
	iface#update(); 
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	fadein();

      method on_loop()=
	clear_iface_ev_stack curs iface;
	iface#update(); 
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	curs#put();    
	flip();

  end;;


class stage_option_video_menu=
  object 
    inherit stage curs
    val mutable iface=new interface "medias/interface/bgs/intro_bg-bot2.png" 800 600 
    val mutable fnt1=new font_object "medias/fonts/samos.ttf" 18
    val mutable arbre2=new graphic_scr_resized_object 326 542 "medias/interface/bgs/arbre2.png" false false

    method on_leave()=
	clear_stack();
	fadeout();
      
    method on_load()=
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	

        (* buttons decl start*)

	iface#add_object_n "opt_video1" (new iface_checkbox "medias/interface/buttons/checkbox.png" fnt1 (n("Show shadows")) );
	let nbut=iface#get_cur_obj - 1 in
	let o1_but=(iface#get_object_num nbut) in
        o1_but#set_data this_config.video_opt1;
	o1_but#move (f_size_w 128) (f_size_h 64);   
	o1_but#set_click (function()->(
	 ));

	iface#add_object_n "opt_video2" (new iface_checkbox "medias/interface/buttons/checkbox.png"  fnt1 (n("Fullscreen")) );
	let nbut=iface#get_cur_obj - 1 in
	let o2_but=(iface#get_object_num nbut) in
        o2_but#set_data this_config.video_opt2;
	o2_but#move (f_size_w 128) (f_size_h 96);   
	o2_but#set_click (function()->(
	 ));

	iface#add_object (new iface_label_static fnt1 (255,255,255) (n("Screensize : ")));
	let nbut=iface#get_cur_obj - 1 in
	let l1_but=(iface#get_object_num nbut) in
	l1_but#move (f_size_w 128) (f_size_h 128);

	let videosize_sb=iface_add_object iface (new iface_selectbox2 fnt1 [|"640x480";"800x600";"1024x768"|]) in
	videosize_sb#move (f_size_w 300) (f_size_h 128);
        videosize_sb#set_data this_config.screen_size;


	iface#add_object (new iface_label_static fnt1 (255,255,255) (n("Ok")));
	let nbut=iface#get_cur_obj - 1 in
	let prev_but=(iface#get_object_num nbut) in
	prev_but#move (f_size_w 128) (f_size_h 272);
	prev_but#set_release (function()->(
          this_config.video_opt1<-o1_but#get_data;
          this_config.video_opt2<-o2_but#get_data;
          this_config.screen_size<-videosize_sb#get_data;

	  fadeout();
	  stage_continue stages "option_menu"));

	(* buttons decl end *)

	o1_but#show();
	o2_but#show();

	l1_but#show();

	videosize_sb#show();

	prev_but#show();
	iface#update(); 
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	fadein();

      method on_loop()=
	clear_iface_ev_stack curs iface;
	iface#update(); 
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	curs#put();    
	flip();

  end;;

class stage_option_audio_menu=
  object 
    inherit stage curs
    val mutable iface=new interface "medias/interface/bgs/intro_bg-bot2.png" 800 600 
    val mutable fnt1=new font_object "medias/fonts/samos.ttf" 18
    val mutable arbre2=new graphic_scr_resized_object 326 542 "medias/interface/bgs/arbre2.png" false false

    method on_leave()=
	clear_stack();
	fadeout();
      
    method on_load()=
      iface#update();
      arbre2#move (f_size_w 475) (f_size_h 60);
      arbre2#put();   	

      (* buttons decl start*)
      
      let l1_but=iface_add_object iface (new iface_label_static fnt1 (255,255,255) (n("Sound volume : "))) in
	l1_but#move (f_size_w 32) (f_size_h 128);
      let vol_sound=iface_add_object iface (new iface_volume 16 2 8 4) in
      vol_sound#move (f_size_w 256) (f_size_h 96);
      vol_sound#set_data (this_config.audio_vol);
      vol_sound#set_click (function()->(
	audio_set_volume ((vol_sound#get_data*128)/16);
	 ));
      
	let l2_but=iface_add_object iface (new iface_label_static fnt1 (255,255,255) (n("Music volume : "))) in
	  l2_but#move (f_size_w 32) (f_size_h 192);
      let vol_music=iface_add_object iface (new iface_volume 16 2 8 4) in
      vol_music#move (f_size_w 256) (f_size_h 160);
      vol_music#set_data (this_config.music_vol);
      vol_music#set_click (function()->(
	let n=music_set_volume ((vol_music#get_data*128)/16) in ()
	 ));

	let prev_but=iface_add_object iface (new iface_label_static fnt1 (255,255,255) (n("Ok"))) in
	  prev_but#move (f_size_w 64) (f_size_h 272);
	  prev_but#set_release (function()->(
          this_config.audio_vol<-vol_sound#get_data;
          this_config.music_vol<-vol_music#get_data;
	  fadeout();
	  stage_continue stages "option_menu"));

	  (* buttons decl end *)

	vol_sound#show();
	vol_music#show();

	l1_but#show();
	l2_but#show();


	prev_but#show();
	iface#update(); 
	  arbre2#move (f_size_w 475) (f_size_h 60);
	  arbre2#put();   	
	  fadein();

      method on_loop()=
	clear_iface_ev_stack curs iface;
	iface#update(); 
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	curs#put();    
	flip();

  end;;


class stage_option_menu=
  object 
    inherit stage curs
    val mutable iface=new interface "medias/interface/bgs/intro_bg-bot-poteau.png" 800 600 
    val mutable fnt1=new font_object "medias/fonts/samos.ttf" 18
    val mutable fnt2=new font_object "medias/fonts/samos.ttf" 12
    val mutable arbre2=new graphic_scr_resized_object 326 542 "medias/interface/bgs/arbre2.png" false false

    method on_leave()=
	clear_stack();
	fadeout();
      
    method on_continue()=
	iface#show_all();
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	fadein();

    method on_load()=
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	

        (* buttons decl start*)

	iface#add_object (new iface_button_with_label fnt1 (n("Game")) "medias/interface/buttons/button2.png" 128 64);
	let nbut=iface#get_cur_obj - 1 in
	let gam_but=(iface#get_object_num nbut) in
	gam_but#move (f_size_w 128) (f_size_h 64);   
	gam_but#set_release (function()->(
	  if (stage_get stages "option_game_menu")#get_initialized==false then
	    stage_load stages "option_game_menu"
	  else
	    stage_continue stages "option_game_menu"
	
	 ));

	iface#add_object (new iface_button_with_label fnt1 (n("Video")) "medias/interface/buttons/button2.png" 128 64);
	let nbut=iface#get_cur_obj - 1 in
	let vid_but=(iface#get_object_num nbut) in
	vid_but#move (f_size_w 128) (f_size_h 140);   
	vid_but#set_release (function()->(
	  if (stage_get stages "option_video_menu")#get_initialized==false then
	    stage_load stages "option_video_menu"
	  else
	    stage_continue stages "option_video_menu"
	 ));

	iface#add_object (new iface_button_with_label fnt1 (n("Audio")) "medias/interface/buttons/button2.png" 128 64);
	let nbut=iface#get_cur_obj - 1 in
	let aud_but=(iface#get_object_num nbut) in
	aud_but#move (f_size_w 128) (f_size_h 216);   
	aud_but#set_release (function()->(
	  if (stage_get stages "option_audio_menu")#get_initialized==false then
	    stage_load stages "option_audio_menu"
	  else
	    stage_continue stages "option_audio_menu"
	 ));


	iface#add_object (new iface_button_with_label fnt1 (n("Back")) "medias/interface/buttons/button2.png" 128 64);
	let nbut=iface#get_cur_obj - 1 in
	let prev_but=(iface#get_object_num nbut) in
	prev_but#move (f_size_w 128) (f_size_h 400);
	prev_but#set_release (function()->(
	  fadeout();
	  stage_continue stages "main_menu"));

	(* buttons decl end *)

	gam_but#show();
	vid_but#show();
	aud_but#show();
	prev_but#show();
	iface#update(); 
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	fadein();

	
      method on_loop()=
	clear_iface_ev_stack curs iface;
	iface#update(); 
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	curs#put();    
	flip();

  end;;



class stage_play_freestyle_menu=
  object 
    inherit stage curs
    val mutable iface=new interface "medias/interface/bgs/intro_bg-bot2.png" 800 600 
    val mutable fnt1=new font_object "medias/fonts/samos.ttf" 24
    val mutable fnt2=new font_object "medias/fonts/samos.ttf" 18
    val mutable arbre2=new graphic_scr_resized_object 326 542 "medias/interface/bgs/arbre2.png" false false
    val mutable cont_b=0
    val mutable is_playing=false


    method on_leave()=
	clear_stack();
	fadeout();

    method  on_continue()=
	let continue_but=(iface#get_object_num cont_b) in
	if is_playing==true then continue_but#show();
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	fadein();

    method on_load()=
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	


	iface#add_object (new iface_label_static fnt1 (127,127,127) (n("Freestyle Game")));
	let nbut=iface#get_cur_obj - 1 in
	let f_but=(iface#get_object_num nbut) in
	f_but#move (f_size_w 32) (f_size_h 16);


        (* buttons decl start*)

	iface#add_object (new iface_label_static fnt2 (255,255,255) (n("Map type : ")));
	let nbut=iface#get_cur_obj - 1 in
	let l3_but=(iface#get_object_num nbut) in
	l3_but#move (f_size_w 32) (f_size_h 128);

	let maptype_sb=iface_add_object iface (new iface_selectbox2 fnt2 ([|(n("Root"));
(n("Beach"))|])) in
	maptype_sb#move (f_size_w 256) (f_size_h 128);


	iface#add_object (new iface_label_static fnt2 (255,255,255) (n("Map size: ")));
	let nbut=iface#get_cur_obj - 1 in
	let l2_but=(iface#get_object_num nbut) in
	l2_but#move (f_size_w 32) (f_size_h 96);

	let mapsize_sb=iface_add_object iface (new iface_selectbox2 fnt2 [|(n("Small"));
(n("Medium"));
(n("Large"))|]) in
	mapsize_sb#move (f_size_w 256) (f_size_h 96);


	iface#add_object (new iface_label_static fnt2 (255,255,255) (n("Team: ")));
	let nbut=iface#get_cur_obj - 1 in
	let l1_but=(iface#get_object_num nbut) in
	l1_but#move (f_size_w 32) (f_size_h 64);

	let camp_sb=iface_add_object iface (new iface_selectbox2 fnt2 [|(n("Rebel"));
(n("NeoNaz"))|]) in
	camp_sb#move (f_size_w 256) (f_size_h 64);

(*	let camp_sb=selectbox_text iface fnt2 [|"Rebel";"NeoNaz"|] in
	camp_sb#move (f_size_w 256) (f_size_h 64);
*)





	iface#add_object (new iface_button_with_label fnt2 (n("Play")) "medias/interface/buttons/button2.png" 128 64);
	let nbut=iface#get_cur_obj - 1 in
	let fs_but=(iface#get_object_num nbut) in
	fs_but#move (f_size_w 32) (f_size_h 260);
	fs_but#set_release (function()->(
	  is_playing<-true;
	  let w=ref 64 and h=ref 64 in
          if mapsize_sb#get_data=0 then (w:=64;h:=64);
          if mapsize_sb#get_data=1 then (w:=96;h:=96);
          if mapsize_sb#get_data=2 then (w:=128;h:=128);
 	  let p=ref 1 in
          if camp_sb#get_data=0 then p:=1;
          if camp_sb#get_data=1 then p:=2;

          let t=ref 9 in
          if maptype_sb#get_data=0 then t:=9;
          if maptype_sb#get_data=1 then t:=1;
	
	  stage_add stages "game_freestyle" (new stage_game (!p) (!w) (!h) 0 (!t));
	  stage_load stages "game_freestyle"
	 ));



	iface#add_object (new iface_button_with_label fnt2 (n("Continue")) "medias/interface/buttons/button2.png" 128 64);
	let nbut=iface#get_cur_obj - 1 in
	let continue_but=(iface#get_object_num nbut) in
	continue_but#move (f_size_w 32) (f_size_h 330);
	continue_but#set_release (function()->(
	  stage_continue stages "game_freestyle"
	 ));
	cont_b<-nbut;

	iface#add_object (new iface_button_with_label fnt2 (n("Back")) "medias/interface/buttons/button2.png" 128 64);
	let nbut=iface#get_cur_obj - 1 in
	let prev_but=(iface#get_object_num nbut) in
	prev_but#move (f_size_w 32) (f_size_h 400);
	prev_but#set_release (function()->(
	  fadeout();
	  stage_continue stages "play_menu"));

        

	(* buttons decl end *)
	f_but#show();
	fs_but#show();

	camp_sb#show(); 
        maptype_sb#show();
        mapsize_sb#show();

	l1_but#show();
	l2_but#show();
	l3_but#show();

	prev_but#show();
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	

	fadein();
	
      method on_loop()=
	clear_iface_ev_stack curs iface;
	iface#update(); 
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	curs#put();    
	flip();

  end;;


class stage_play_network_menu=
  object 
    inherit stage curs
    val mutable iface=new interface "medias/interface/bgs/intro_bg-bot2.png" 800 600 
    val mutable fnt1=new font_object "medias/fonts/samos.ttf" 24
    val mutable fnt2=new font_object "medias/fonts/samos.ttf" 18
    val mutable arbre2=new graphic_scr_resized_object 326 542 "medias/interface/bgs/arbre2.png" false false
    val mutable cont_b=0
    val mutable is_playing=false


    method on_leave()=
	clear_stack();
	fadeout();

    method  on_continue()=
	let continue_but=(iface#get_object_num cont_b) in
	if is_playing==true then continue_but#show();
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	fadein();

    method on_load()=
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	


	iface#add_object (new iface_label_static fnt1 (127,127,127) (n("Network Game")));
	let nbut=iface#get_cur_obj - 1 in
	let f_but=(iface#get_object_num nbut) in
	f_but#move (f_size_w 32) (f_size_h 16);


        (* buttons decl start*)

	iface#add_object (new iface_label_static fnt2 (255,255,255) (n("Map type : ")));
	let nbut=iface#get_cur_obj - 1 in
	let l3_but=(iface#get_object_num nbut) in
	l3_but#move (f_size_w 32) (f_size_h 128);

	let maptype_sb=iface_add_object iface (new iface_selectbox2 fnt2 ([|(n("Root"));
(n("Beach"))|])) in
	maptype_sb#move (f_size_w 256) (f_size_h 128);


	iface#add_object (new iface_label_static fnt2 (255,255,255) (n("Map size: ")));
	let nbut=iface#get_cur_obj - 1 in
	let l2_but=(iface#get_object_num nbut) in
	l2_but#move (f_size_w 32) (f_size_h 96);

	let mapsize_sb=iface_add_object iface (new iface_selectbox2 fnt2 [|(n("Small"));
(n("Medium"));
(n("Large"))|]) in
	mapsize_sb#move (f_size_w 256) (f_size_h 96);


	iface#add_object (new iface_label_static fnt2 (255,255,255) (n("Team: ")));
	let nbut=iface#get_cur_obj - 1 in
	let l1_but=(iface#get_object_num nbut) in
	l1_but#move (f_size_w 32) (f_size_h 64);

	let camp_sb=iface_add_object iface (new iface_selectbox2 fnt2 [|(n("Rebel"));
(n("NeoNaz"))|]) in
	camp_sb#move (f_size_w 256) (f_size_h 64);


	iface#add_object (new iface_label_static fnt1 (255,255,255) (n("Play")));
	let nbut=iface#get_cur_obj - 1 in
	let fs_but=(iface#get_object_num nbut) in
	fs_but#move (f_size_w 32) (f_size_h 300);
	fs_but#set_release (function()->(
	  is_playing<-true;
	  let w=ref 64 and h=ref 64 in
          if mapsize_sb#get_data=0 then (w:=64;h:=64);
          if mapsize_sb#get_data=1 then (w:=96;h:=96);
          if mapsize_sb#get_data=2 then (w:=128;h:=128);
 	  let p=ref 1 in
          if camp_sb#get_data=0 then p:=1;
          if camp_sb#get_data=1 then p:=2;

          let t=ref 9 in
          if maptype_sb#get_data=0 then t:=9;
          if maptype_sb#get_data=1 then t:=1;
	
	  stage_add stages "game_network" (new stage_game (!p) (!w) (!h) 2 (!t));
	  stage_load stages "game_network"
	 ));


	iface#add_object (new iface_label_static fnt1 (255,255,255) (n("Continue")));
	let nbut=iface#get_cur_obj - 1 in
	let continue_but=(iface#get_object_num nbut) in
	continue_but#move (f_size_w 32) (f_size_h 350);
	continue_but#set_release (function()->(
	  stage_continue stages "game_network"
	 ));
	cont_b<-nbut;

	iface#add_object (new iface_label_static fnt1 (255,255,255) (n("Back")));
	let nbut=iface#get_cur_obj - 1 in
	let prev_but=(iface#get_object_num nbut) in
	prev_but#move (f_size_w 32) (f_size_h 400);
	prev_but#set_release (function()->(
	  fadeout();
	  stage_continue stages "play_menu"));

        

	(* buttons decl end *)
	f_but#show();
	fs_but#show();

	camp_sb#show(); 
        maptype_sb#show();
        mapsize_sb#show();

	l1_but#show();
	l2_but#show();
	l3_but#show();

	prev_but#show();
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	

	fadein();
	
      method on_loop()=
	clear_iface_ev_stack curs iface;
	iface#update(); 
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	curs#put();    
	flip();

  end;;


class stage_play_menu=
  object 
    inherit stage curs
    val mutable iface=new interface "medias/interface/bgs/intro_bg-bot2.png" 800 600 
    val mutable fnt1=new font_object "medias/fonts/samos.ttf" 24
    val mutable fnt2=new font_object "medias/fonts/samos.ttf" 18
    val mutable arbre2=new graphic_scr_resized_object 326 542 "medias/interface/bgs/arbre2.png" false false
    val mutable cont_b=0
    val mutable is_playing=false

    method on_leave()=
	clear_stack();
	fadeout();

    method on_continue()=
	let continue_but=(iface#get_object_num cont_b) in
	if is_playing==true then continue_but#show();
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	fadein();

     method on_load()=
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	

        (* buttons decl start*)


	iface#add_object (new iface_button_with_label fnt2 (n("Freestyle")) "medias/interface/buttons/button2.png" 128 64);
	let nbut=iface#get_cur_obj - 1 in
	let fs_but=(iface#get_object_num nbut) in
	fs_but#move (f_size_w 32) (f_size_h 64);
	fs_but#set_release (function()->(
	  is_playing<-true;
	  stage_add stages "game_freestyle_menu" (new stage_play_freestyle_menu);
	  stage_load stages "game_freestyle_menu";
	 ));


	iface#add_object (new iface_button_with_label fnt2 (n("Network")) "medias/interface/buttons/button2.png" 128 64);
	let nbut=iface#get_cur_obj - 1 in
	let net_but=(iface#get_object_num nbut) in
	net_but#move (f_size_w 32) (f_size_h 128);
	net_but#set_release (function()->(
	  is_playing<-true;
	  stage_add stages "game_network_menu" (new stage_play_network_menu);
	  stage_load stages "game_network_menu";
	 ));

	iface#add_object (new iface_button_with_label fnt2 (n("Back")) "medias/interface/buttons/button2.png" 128 64);
	let nbut=iface#get_cur_obj - 1 in
	let prev_but=(iface#get_object_num nbut) in
	prev_but#move (f_size_w 32) (f_size_h 400);
	prev_but#set_release (function()->(
	  fadeout();
	  stage_continue stages "main_menu"));

        

	(* buttons decl end *)

	fs_but#show();
	net_but#show();

	prev_but#show();
	iface#update();
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	

	fadein();
	

      method on_loop()=
	clear_iface_ev_stack curs iface;
	iface#update(); 
	arbre2#move (f_size_w 475) (f_size_h 60);
	arbre2#put();   	
	curs#put();    
	flip();
  end;;



class stage_menu=
  object 
    inherit stage curs
    val mutable iface=new interface "medias/interface/bgs/intro_bg-bot-chaine.png" 800 600 
    val mutable fnt1=new font_object "medias/fonts/samos.ttf" 18
    val mutable intro_top=new graphic_scr_resized_object 800 600 "medias/interface/bgs/intro_bg-top2.png" false false
    val mutable arbre=new graphic_scr_resized_object 106 541 "medias/interface/bgs/arbre.png" false false
    val mutable arbre2=new graphic_scr_resized_object 326 542 "medias/interface/bgs/arbre2.png" false false
    val mutable pan=new graphic_scr_resized_object 245 80 "medias/interface/bgs/panneau.png" false false
    val mutable lezard=new graphic_scr_resized_object 64 128 "medias/interface/bgs/lezard_anim.png" false false    
    val mutable dyn_lez=new graphic_dyn_object ("lezard_anim") 1 (function k->(tile_empty()))
    val mutable lanim=0
    initializer
      dyn_lez<-(new graphic_dyn_object ("lezard_anim") 1 (function k->(
	let t=ref 0 in
	if (k mod 16)<9 then t:=0 else t:=1;
	match k with
	| i when 50<i && i<201 -> (
	    let ti=tile_rotozoom (lezard#get_tile !t) (float_of_int(30-i/4)) 1.0 in
	    ti
	   )
	| i when 300<i && i<451 -> (
	    tile_rotozoom (lezard#get_tile !t) (float_of_int(45 + i/4)) 1.0 
	   )
	| _ -> tile_copy (lezard#get_tile 0)   
	      
       )));


     method on_leave()=
	clear_stack();
	iface#hide_all();
	fadeout();


      method on_continue()=
	iface#show_all();
	iface#update();
	intro_top#move (f_size_w (-30)) (f_size_h(-30));
	intro_top#put();
	fadein();

	let dx=680 and dy=480 in
	for i=0 to 60 do
	  iface#update(); 
	  intro_top#move (f_size_w (-i*2) -30) (f_size_h (-i*2) -30);
	  intro_top#put();
	  arbre#move (f_size_w dx) (f_size_h (dy-((i-20)*10)));   
	  arbre#put();
	  
	  flip();
	done;
	
	for i=0 to 10 do
	  iface#update(); 
	  intro_top#move (f_size_w (-150)) (f_size_h (-150));
	  intro_top#put();
	  arbre2#move (f_size_w 475) (f_size_h 60);

	  arbre2#put();
	  
	  flip();
	done;
	flip();
	flip();

	intro_top#move (f_size_w (-30)) (f_size_h (-30));
	intro_top#put(); 

	arbre2#put();



      
      method on_load()=
	iface#update(); 
	intro_top#move (f_size_w(-30)) (f_size_h(-30));
	intro_top#put();
	pan#move (f_size_w 485) (f_size_h 111);
	pan#put();


        (* buttons decl start*)

	iface#add_object (new iface_button_menu fnt1 (n("Play")) "medias/interface/buttons/button1.png" 128 94);
	let nbut=iface#get_cur_obj - 1 in
	let play_but=(iface#get_object_num nbut) in
	play_but#move (f_size_w 393) (f_size_h 12);
	play_but#set_release (function()->(
	  if (stage_get stages "play_menu")#get_initialized==false then
	    stage_load stages "play_menu"
	  else
	    stage_continue stages "play_menu"
	 ));

		
	iface#add_object (new iface_button_menu fnt1 (n("Options")) "medias/interface/buttons/button1.png" 128 94);
	let nbut=iface#get_cur_obj - 1 in
	let opt_but=(iface#get_object_num nbut) in
	opt_but#move (f_size_w 321) (f_size_h 106);   
	opt_but#set_release (function()->(
	  if (stage_get stages "option_menu")#get_initialized==false then
	    stage_load stages "option_menu"
	  else
	    stage_continue stages "option_menu"
	 ));



	iface#add_object (new iface_button_menu fnt1 (n("Intro")) "medias/interface/buttons/button1.png" 128 94);
	let nbut=iface#get_cur_obj - 1 in
	let intro_but=(iface#get_object_num nbut) in
	intro_but#move (f_size_w 256) (f_size_h 217);   
	intro_but#set_release (function()->(
	  stage_continue stages "intro_poc";
	 ));

	iface#add_object (new iface_button_menu fnt1 (n("Credits")) "medias/interface/buttons/button1.png" 128 94);
	let nbut=iface#get_cur_obj - 1 in
	let cred_but=(iface#get_object_num nbut) in
	cred_but#move  (f_size_w 154) (f_size_h 290);   
	cred_but#set_release (function()->(
	  stage_continue stages "credits";
	 ));

	iface#add_object (new iface_button_menu fnt1 (n("Quit")) "medias/interface/buttons/button1.png" 128 94);
	let nbut=iface#get_cur_obj - 1 in
	let quit_but=(iface#get_object_num nbut) in
	quit_but#move (f_size_w 9) (f_size_h 327);
	quit_but#set_release (function()->(fadeout();stage_continue stages "credits_quit"));


(*
	iface#add_object (new iface_text_entry fnt1 (127,127,127));
	let nbut=iface#get_cur_obj - 1 in
	let e=(iface#get_object_num nbut) in
	  e#move (f_size_w 16) (f_size_h 16);
	  e#show();
*)
	play_but#show();
	cred_but#show();
	intro_but#show();
	opt_but#show();
	quit_but#show();
	iface#update(); 
	(* buttons decl end *)
	  intro_top#put();
	fadein();
   
	let dx=680 and dy=480 in
	for i=0 to 60 do
	  iface#update(); 
	  intro_top#move (f_size_w (-i*2 -30)) (f_size_h (-i*2 -30));
	  intro_top#put();
	  arbre#move (f_size_w dx) (f_size_h (dy-((i-20)*10)));   
	  arbre#put();
	  
	  flip();
	done;
	
	for i=0 to 10 do
	  iface#update(); 
	  intro_top#move (f_size_w (-150)) (f_size_h (-150));
	  intro_top#put();
	  arbre2#move (f_size_w 475) (f_size_h 60);
	  arbre2#put();
	  
	  flip();
	done;
	flip();
	flip();
	
	dyn_lez#move (f_size_w (500)) (f_size_h (600));

      method on_loop()=
	clear_iface_ev_stack curs iface;	
	iface#update(); 
	intro_top#move (f_size_w (-150)) (f_size_w (-150));
	intro_top#put();
	dyn_lez#set_cur_tile lanim; 
	if 50<lanim && lanim<201 then
	  dyn_lez#move (f_size_w (500 + (lanim - 51))) (f_size_h (600 -(lanim - 51)));
	if 300<lanim && lanim<451 then
	  dyn_lez#move (f_size_w (500 + 150 - (lanim - 301))) (f_size_h (600 - 150 + (lanim - 301)));
	dyn_lez#put(); 
	arbre2#put();
	curs#put();    
	flip();
	if lanim<451 then
	  lanim<-lanim+1
	else lanim<-0;
(*	print_string !cur_key;print_newline(); *)
  end;;






