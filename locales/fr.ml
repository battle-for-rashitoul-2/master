open Locale;;

Hashtbl.add locales "fr" (new locale "fr");
let loc e t=(Hashtbl.find locales "fr")#add e t in

loc	"Abdel, mont� comme un ... Jama�cain sur un mountain board
	      et arm� de son canif ( le couteau pas le p'tit fien!)"
	"";

loc	"Music volume : "
	"Volume musique : ";

loc	"Fullscreen"
	"Plein �cran";

loc	"ZORK, le h�ros Rebelle, avec son oeil de lynx
	      vous permettra de balayer le terrain pour placer
	      les b�timents, avec sa cl� � fourche double... CASSER du Naz."
	"";

loc	"Back"
	"Retour";

loc	"La taverne est le lieu de rencontre des paysans du coin et le jukebox attire les zicos.
		En gros, apr�s vous pouvez construire la ferme et le festival.
		...Heu , au fait, suivez Zork pour la placer !"
	"";

loc	"Freestyle"
	"";

loc	"frame per second"
	"frame par seconde";

loc	"C'est ici que vous rencontrerez des potes qui se joindront � vous."
	"";

loc	"Screensize : "
	"Taille de l'�cran:";

loc	"Le Bazooka, unit� compos� d'un �norme canon
	      afin de compenser la taille de sa .... boite cranienne."
	"";

loc	"Le patrouilleur s'est vu confier une raquette de ping pong
	      � l'issue du championnat du monde de ping pong (2x) de Biroued,
	      il se sent alors investi des pouvoirs des moines shaoline du ping pong (3x).
	      Enfin si �a avait pas �t� une raquette de ping pong,
	      il aurait p�ter du chevelu avec un annuaire...� l'ancienne."
	"";

loc	"Video"
	"";

loc	"Lifebar"
	"";

loc	"Medium"
	"Moyenne";

loc	"Map type : "
	"Type de carte: ";

loc	"Map size: "
	"Taille carte : ";

loc	"Rebel"
	"";

loc	"screen height"
	"";

loc	"Le Brad lab est apparament la seule unit� humaine Naz.
	      Issue de multiple manipulations g�n�tiques il est le r�sultat
	      d'un croisement entre un porte manteau et une borne kilom�trique."
	"";

loc	"Show shadows"
	"";

loc	"Le mitrailleur poss�de une �norme paire de
	 ... heu... mitrailles aux bouts......des bras."
	"";

loc	"V�t�ran des campagnes de Biroued, il conna�t les plantes de la r�gion
	      et s'est confectionn� un power lanceur de power bud super compact�e."
	"";

loc	"Ami des plantes vertes, c'est l'indice de bioactivit� qui indique comnbien vous avez la main verte.
		C'est ce qui vous permettra de rallier de nombreux amis � votre cause.
		Amateur d'h�moglobine et de silicone combustionn�, le taux de boucherie indique votre
		performance � l'annihilation de votre adversaire. Au taquet c'est gagn� !!!"
	"";

loc	"Recognize team"
	"Differencer les camps";

loc	"Play"
	"Jouer";

loc	"Vous pouvez... construire des v�hi... cules si tu avances si tu avances."
	"";

loc	"Les joies de la vie en groupe vous permettrons de former
		une super arm�e de super men de la mort qui tue !"
	"";

loc	"Loading"
	"Chargement";

loc	"Credits"
	"";

loc	"Options"
	"";

loc	"fullscreen mode"
	"mode plein �cran";

loc	"Hooo La ferme est le b�timent qui permet de planter
	      les ressources dont vous avez... besoin ;-)"
	"";

loc	"Ca c'est l'indice de pollution qui indique � quel point vous �tes un pollueur de tourner en rond.
	      C'est ce qui permet entre autre de produire des unit�s.
	      Amateur d'h�moglobine et de silicone combustionn�, le taux de boucherie indique votre
	      performance � l'annihilation de votre adversaire. Au taquet c'est gagn� !!!"
	"";

loc	"Construction en cours"
	"";

loc	"NeoNaz"
	"";

loc	"Le Gymnaz est le premier b�timent NeoNaz � construire
		...Essayer de suivre Brad Lab pour le placer!"
	"";

loc	"default language"
	"langue par defaut";

loc	"Adepte du lancer de p�pino manuel, il atteint
	      le sommet de son art en r�alisant son Lance P�pino."
	"";

loc	"Quit"
	"Quitter";

loc	"screen width"
	"largeur de l'�cran";

loc	"Large"
	"";

loc	"Beach"
	"Plage";

loc	"Audio"
	"";

loc	"Sound volume : "
	"Volume son : ";

loc	"Team: "
	"";

loc	"Ok"
	"";

loc	"Root"
	"";

loc	"Game"
	"";

loc	"Le labo permet de d�poser des cochoncet�s tout partout."
	"";

loc	"Sorte d'unit� hybride entre un char d'assault et un tir-bouchon.
	      Il fait tr�s peur mais il va pas vite!"
	"";

loc	"Intro"
	"";

loc	"Freestyle Game"
	"";

loc	"In game help"
	"";

loc	"Small"
	"";

loc	"Continue"
	"";

loc	"Houlala la.... La punkette, son truc � elle c'est le coup de boules
	      et la bonne humeur. et puis... pendant que..."
	"";

loc	"windowed mode"
	"mode fen�tr�";


