(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Rect;;

(** Game layer class definition *)

class game_layer wi hi=
  object (self)
    val mutable rect=new rectangle 0 0 wi hi

    val mutable lay=
      Array.make_matrix wi hi 0

    method out_of_lay x y=
      if x>=0 && y>=0 && x<wi && y<hi then false else true 

    method print_para w x y=print_string ("GAME LAYER PARACHUTE : "^w^" "^string_of_int(x)^"-"^string_of_int(y)^" OUT OF ARRAY");print_newline();

    method get_rect=rect

    (* FIXME : unsafe *)
    (* NOTE : only used by minimap *)
    method foreach_map_entry d=
      Array.iteri (fun i v->(Array.iteri (fun j w->(d i j w)) v))  lay

	

    method get_lay=lay 

    method get_position x y=
	lay.(x).(y)
  
    method set_position x y v=
	lay.(x).(y)<-v

   end;;
