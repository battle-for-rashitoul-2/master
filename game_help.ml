(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open Rect;;
open Config;;
open Video;;
open Object;;
open Music;;
open Interface;;
open File;;

open Game_main;;
open Game_player_type;;
open Game_interface;;
open Game_cursors;;
open Game_object;;
open Game_select;;
open Game_object_type;;


let create_help ifa iam_string=
      let help_fnt=new font_object "medias/fonts/samos.ttf" 10 in
	 (* HELP *)
	 let add_help nm txt=
	   ifa#add_object_n (nm^":help") (new iface_help help_fnt txt);
	   let obj=(ifa#get_object_char (nm^":help")) in
	   obj#move ((f_size_w 635)-obj#get_rect#get_w) ((f_size_h 400)-obj#get_rect#get_h) in

	 (* REBELLES *)

	 (* taverne *)
	 add_help "taverne" 
	   (n("La taverne est le lieu de rencontre des paysans du coin et le jukebox attire les zicos.
		En gros, apr�s vous pouvez construire la ferme et le festival.
		...Heu , au fait, suivez Zork pour la placer !"));
		
	 (* ferme *)
	 add_help "ferme" 
	 (n("Hooo La ferme est le b�timent qui permet de planter
	      les ressources dont vous avez... besoin ;-)"));
		 
	 (* festival *)
	 add_help "festival" 
	 (n("C'est ici que vous rencontrerez des potes qui se joindront � vous."));

	 (* zork *)
	 add_help "zork" 
	 (n("ZORK, le h�ros Rebelle, avec son oeil de lynx
	      vous permettra de balayer le terrain pour placer
	      les b�timents, avec sa cl� � fourche double... CASSER du Naz.")); 
		   

	 (* lance_pepino *)
	 add_help "lance_pepino" 
	 (n("Adepte du lancer de p�pino manuel, il atteint
	      le sommet de son art en r�alisant son Lance P�pino."));


	 (* guerrier_bud *)
	 add_help "guerrier_bud" 
	 (n("V�t�ran des campagnes de Biroued, il conna�t les plantes de la r�gion
	      et s'est confectionn� un power lanceur de power bud super compact�e.")); 


	 (* punkette *)
	 add_help "punkette" 
	 (n("Houlala la.... La punkette, son truc � elle c'est le coup de boules
	      et la bonne humeur. et puis... pendant que..."));


	 (* abdel *)
	 add_help "abdel" 
	 (n("Abdel, mont� comme un ... Jama�cain sur un mountain board
	      et arm� de son canif ( le couteau pas le p'tit fien!)"));

	 
	 (* NEONAZS *)
	 (* gymnaz *)
	 add_help "gymnaz" 
	   (n("Le Gymnaz est le premier b�timent NeoNaz � construire
		...Essayer de suivre Brad Lab pour le placer!"));


	 (* labo *)
	 add_help "labo" 
	   (n("Le labo permet de d�poser des cochoncet�s tout partout."));


	 (* caserne *)
	 add_help "caserne" 
	   (n("Les joies de la vie en groupe vous permettrons de former
		une super arm�e de super men de la mort qui tue !"));


	 (* usine_vehicule *)
	 add_help "usine_vehicule" 
	   (n("Vous pouvez... construire des v�hi... cules si tu avances si tu avances."));



	 add_help "brad_lab" 
	 (n("Le Brad lab est apparament la seule unit� humaine Naz.
	      Issue de multiple manipulations g�n�tiques il est le r�sultat
	      d'un croisement entre un porte manteau et une borne kilom�trique."));


	 add_help "patrouilleur" 
	 (n("Le patrouilleur s'est vu confier une raquette de ping pong
	      � l'issue du championnat du monde de ping pong (2x) de Biroued,
	      il se sent alors investi des pouvoirs des moines shaoline du ping pong (3x).
	      Enfin si �a avait pas �t� une raquette de ping pong,
	      il aurait p�ter du chevelu avec un annuaire...� l'ancienne."));  


	 add_help "bazooka" 
	 (n("Le Bazooka, unit� compos� d'un �norme canon
	      afin de compenser la taille de sa .... boite cranienne."));


	 add_help "mitrailleur" 
	 (n("Le mitrailleur poss�de une �norme paire de
	 ... heu... mitrailles aux bouts......des bras."));


	 add_help "super_guerrier" 
	 (n("Sorte d'unit� hybride entre un char d'assault et un tir-bouchon.
	      Il fait tr�s peur mais il va pas vite!"));

	 

	 if iam_string="rebel" then (
	 ifa#add_object_n "resourcebar:help" (new iface_help help_fnt 
	   (n("Ami des plantes vertes, c'est l'indice de bioactivit� qui indique comnbien vous avez la main verte.
		C'est ce qui vous permettra de rallier de nombreux amis � votre cause.
		Amateur d'h�moglobine et de silicone combustionn�, le taux de boucherie indique votre
		performance � l'annihilation de votre adversaire. Au taquet c'est gagn� !!!"));
	 )	     
	  );
	 if iam_string="neonaz" then
           (
	    ifa#add_object_n "resourcebar:help" (new iface_help help_fnt 
	      (n("Ca c'est l'indice de pollution qui indique � quel point vous �tes un pollueur de tourner en rond.
	      C'est ce qui permet entre autre de produire des unit�s.
	      Amateur d'h�moglobine et de silicone combustionn�, le taux de boucherie indique votre
	      performance � l'annihilation de votre adversaire. Au taquet c'est gagn� !!!"));
	      
	    )
	   );
	     
	 let obj=(ifa#get_object_char "resourcebar:help") in
         obj#move ((f_size_w 635)-obj#get_rect#get_w) ((f_size_h 400)-obj#get_rect#get_h);

      if iam_string="rebel" then (
	ifa#add_object_n "resourcebar" (new iface_resourcebar "medias/interface/bgs/indice_bio.png" "medias/interface/bgs/indice_bio-bg.png" 430 50 1);
	let obj=(ifa#get_object_char "resourcebar") in
	obj#move (f_size_w 196) (f_size_h 0);
	obj#show();

      obj#set_mouseover(function()->(
	if (ifa#is_object "resourcebar:help")==true then (
	  let hobj=(ifa#get_object_char "resourcebar:help") in
	  hobj#show();
	 );
       ));
      obj#set_mouseout(function()->(
	if (ifa#is_object "resourcebar:help")==true then (
	  let hobj=(ifa#get_object_char "resourcebar:help") in
	  hobj#hide();
	 )
       ));

       );
      if iam_string="neonaz" then
	(
	 ifa#add_object_n "resourcebar" (new iface_resourcebar "medias/interface/bgs/indice_pol.png" "medias/interface/bgs/indice_pol-bg.png" 378 41 2);
	 let obj=(ifa#get_object_char "resourcebar") in
	 obj#move (f_size_w 203) (f_size_h (0));
	 obj#show();
      obj#set_mouseover(function()->(
	if (ifa#is_object "resourcebar:help")==true then (
	  let hobj=(ifa#get_object_char "resourcebar:help") in
	  hobj#show();
	 );
       ));
      obj#set_mouseout(function()->(
	if (ifa#is_object "resourcebar:help")==true then (
	  let hobj=(ifa#get_object_char "resourcebar:help") in
	  hobj#hide();
	 )
       ));


	)

;;
