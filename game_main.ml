(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open File;;
open Config;;
open Video;;
open Locale;;
open Game_version;;


let configfile=
if Sys.os_type="Unix" then (Sys.getenv("HOME")^"/.bfr.conf")
else "bfr.conf";;

let this_config=load_config configfile;;

let save_config()=save_file configfile this_config;;

at_exit (save_config);;


let set_lang l=this_config.lang<-l;;
let n e=(Hashtbl.find locales this_config.lang)#get e;;

(* get configs *)
if this_config.screen_size=0 then (scr_w:=640;scr_h:=480);; 
if this_config.screen_size=1 then (scr_w:=800;scr_h:=600);; 
if this_config.screen_size=2 then (scr_w:=1024;scr_h:=768);; 
if this_config.video_opt2=1 then (fullscreen:=true);;


print_string "---------------------------------------------------\n";
print_string "Battle For Rashitoul version ";print_string version;print_string ", ";
print_string "Copyright (C) 2003 POC\n";
print_string "
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA\n";
print_string "---------------------------------------------------\n";
print_newline();
print_string "Welcome to BFR !";
print_newline();;

let server_mode=ref false;;

let args=[
("-fs",Arg.Set (fullscreen),(n("fullscreen mode")));
("-ws",Arg.Set (windowed),(n("windowed mode")));
("-server",Arg.Set (server_mode),(n("server mode")));
("-w",Arg.Int (set_scr_w),(n("screen width")));
("-h",Arg.Int (set_scr_h),(n("screen height")));
("-fps",Arg.Int (set_fps),(n("frame per second")));
("-lang",Arg.String (set_lang),(n("default language")))] in
let usage= "usage : bfr [-fs] [-ws] [-w width] [-h height] [-fps fps] [-lang lang]" in
Arg.parse args (fun s -> ()) usage;;

all_init();
if !server_mode==false then (
if !windowed=true then fullscreen:=false;


video_init (!scr_w) (!scr_h) 16 (!fullscreen);
audio_init 44100 2 ;
wm_set_caption ("Battle For Rashitoul "^version) "medias/misc/bfr_rebel.xpm";

audio_set_volume ((this_config.audio_vol*128)/16);
music_set_volume ((this_config.music_vol*128)/16);

frame_init();
frame_set(!fps);
)

