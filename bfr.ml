(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open File;;
open Config;;
open Video;;
open Object;;
open Event;;
open Music;;
open Stage;;
open Interface;;
open Iface_event;;
open Network;;


open Game_main;;
open Game_select;;
open Game_object;;
open Game_event;;
open Game_map;;
open Game_network;;
open Game_stage;;
open Game_init;;


  type new_game={
    vtype:string;
    w:int;		   
    h:int;
    p:int;
  };;
(** Main module *)





if !server_mode==false then (

stage_add stages "quit" (new stage_quit);
stage_add stages "intro_poc" (new stage_movie "medias/movies/intro.mpg" "main_menu");
stage_add stages "credits_quit" (new stage_movie "medias/movies/credits.mpg" "quit");
stage_add stages "credits" (new stage_movie "medias/movies/credits.mpg" "main_menu");

stage_add stages "main_menu" (new stage_menu);
stage_add stages "play_menu" (new stage_play_menu);
stage_add stages "option_menu" (new stage_option_menu);
stage_add stages "option_game_menu" (new stage_option_game_menu);
stage_add stages "option_video_menu" (new stage_option_video_menu);
stage_add stages "option_audio_menu" (new stage_option_audio_menu);

stage_load stages "intro_poc";
)
else
(
  print_string "SERVER MODE";print_newline();
  let gm=game_map_init 64 64 1 in
    game_map_set_types gm;
    (*    game_set_cond_freestyle gm;*)
    (*   gm#freestyle_init(); *)
    gm#freestyle_resource 10 10;
    (*
      for i=1 to gm#get_num_players do
      game_map_set_ai gm i;
      done;
    *)

  let game_serv=new game_server gm in
    game_serv#run();

)


