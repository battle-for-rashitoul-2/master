(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open Rect;;
open Config;;
open Video;;
open Object;;
open Music;;
open Interface;;
open File;;
open Network;;

open Game_interface;;
open Game_cursors;;
open Game_object;;
open Game_select;;
open Game_object_type;;
open Game_layer;;
open Game_tile_layer;;
open Game_object_layer;;
open Game_player_layer;;
open Game_ai;;
open Game_map;;


(** Game init functions *)

(* init game *)
let game_map_init w h player=
  new game_map w h (!tile_w) (!tile_h) 0 0 [|"rebel";"neonaz"|] player;;

let game_map_set_types gm=
  gm#create_action_buttons();
  (* add object types *)

  (* decors *)

  (* resources *)

  let init_nature=new graphic_object 32 32 "medias/resources/nature.png" false false in
  let init_pollution=new graphic_object 32 32 "medias/resources/pollution.png" false false in

  gm#add_obj_type "nature1" "none" "medias/resources/nature-icon.png" (
  function()->(
    new game_resource "nature1" 0 0 [|64;32;32;32;32;32|] [|98;32;32;32;32;32|]
      [|(true,"medias/resources/nature1.png");
	(false,"medias/resources/nature2.png");
	(false,"medias/resources/nature3.png");
	(false,"medias/resources/nature4.png");
	(false,"medias/resources/nature5.png");
	(false,"medias/resources/nature6.png");
      |]
      1 1 0
      0 true
      init_nature
   ));

  gm#add_obj_type "nature"  "none" "medias/resources/nature-icon.png" (
  function()->(
    new game_resource_random "nature" 0 0 [|64;32;32;32;32;32;64;64|] [|98;32;32;32;32;32;128;128|] 
      [|(false,"medias/resources/nature1.png");
	(false,"medias/resources/nature2.png");
	(false,"medias/resources/nature3.png");
	(false,"medias/resources/nature4.png");
	(false,"medias/resources/nature5.png");
	(false,"medias/resources/nature6.png");
	(false,"medias/resources/nature7.png");
	(false,"medias/resources/nature8.png");
      |]
      1 1 0
      init_nature 
   ));

 gm#add_obj_type "pollution"  "none" "medias/resources/pollution-icon.png" (
  function()->(
    new game_resource_random "pollution" 0 0 [|32;64;32;32;32;64;64|] [|32;64;32;32;64;96;67|] 
      [|(false,"medias/resources/pollution1.png");
	(false,"medias/resources/pollution2.png");
	(false,"medias/resources/pollution3.png");
	(false,"medias/resources/pollution4.png"); 
	(false,"medias/resources/pollution5.png"); 
	(false,"medias/resources/pollution6.png"); 
	(false,"medias/resources/pollution7.png"); 
      |]
      1 1 0
      init_pollution
   ));
  
  (* units + buildings *)
  (* units : vitesse attaque1 defense1 attaque2 defense2 vie vue att_vue*)
  (* rebel *)
  gm#add_obj_type "guerrier_bud"  "medias/units/rebels/guerrier_bud/pics/guerrier_bud-face.png" "medias/units/rebels/guerrier_bud/pics/guerrier_bud-icon.png"(
  function()->(
  new game_unit_char "guerrier_bud" 
      0 0 43 45 
      "medias/units/rebels/guerrier_bud/pics/guerrier_bud.png"
      1 1 
      [|
	"medias/units/rebels/guerrier_bud/sounds/gb_selec01.wav";
	"medias/units/rebels/guerrier_bud/sounds/gb_selec02.wav"
      |]

      {frames=[|0;3|];refresh=8;action=(function()->());sound=[|"none"|]}
      {frames=[|0;1;0;2|];refresh=2;action=(function()->());sound=
       [|
	 "medias/units/rebels/guerrier_bud/sounds/gb_action01.wav";
	 "medias/units/rebels/guerrier_bud/sounds/gb_action02.wav";
	 "medias/units/rebels/guerrier_bud/sounds/gb_action03.wav";
       |]} 
      {frames=[|4;5;5;5|];refresh=8;action=(function()->());sound=[|
       "medias/units/rebels/guerrier_bud/sounds/gb_attaque01.wav";
       "medias/units/rebels/guerrier_bud/sounds/gb_attaque02.wav";
       "medias/units/rebels/guerrier_bud/sounds/gb_attaque03.wav";
     |]}
      {frames=[|8;9|];refresh=8;action=(function()->());sound=[|"none"|]}
      4 4 2 4 2 5 3 2
      (new game_projectile "bud" 32 32 "medias/projectiles/bud.png" true {frames=[|0;1;2;3|];refresh=1;action=(function()->());sound=[|"medias/projectiles/tir_bud.wav"|]})
 ));

  (* units : vitesse attaque1 defense1 attaque2 defense2 vie vue att_vue*)
  gm#add_obj_type "lance_pepino"  "medias/units/rebels/lance_pepino/pics/lance_pepino-face.png" "medias/units/rebels/lance_pepino/pics/lance_pepino-icon.png" (
  function()->(
  new game_unit_char "lance_pepino" 
      0 0 45 55 
      "medias/units/rebels/lance_pepino/pics/lance_pepino.png"
      1 1 
      [|    
	"medias/units/rebels/lance_pepino/sounds/lp_selec01.wav";
	"medias/units/rebels/lance_pepino/sounds/lp_selec02.wav";
	"medias/units/rebels/lance_pepino/sounds/lp_selec03.wav";
	  |]
      {frames=[|0;0|];refresh=8;action=(function()->());sound=[|"none"|]}
      {frames=[|0;1;0;2|];refresh=2;action=(function()->());sound=[|
	"medias/units/rebels/lance_pepino/sounds/lp_action01.wav";
	"medias/units/rebels/lance_pepino/sounds/lp_action02.wav";
	"medias/units/rebels/lance_pepino/sounds/lp_action03.wav";
     |]} 
      {frames=[|4;5;5;5|];refresh=8;action=(function()->());sound=[|
       "medias/units/rebels/lance_pepino/sounds/lp_attaque01.wav";
       "medias/units/rebels/lance_pepino/sounds/lp_attaque02.wav";
     |]}
      {frames=[|6;8;9|];refresh=8;action=(function()->());sound=[|"none"|]}
      4 1 2 3 2 3 3 2
      (new game_projectile "pepino" 32 32 "medias/projectiles/pepino.png" true {frames=[|2;1;0;1|];refresh=1;action=(function()->());sound=[|"medias/projectiles/pepino.wav"|]})
 ));

  (* units : vitesse attaque1 defense1 attaque2 defense2 vie vue att_vue*)
  gm#add_obj_type "punkette"  "medias/units/rebels/punkette/pics/punkette-face.png" "medias/units/rebels/punkette/pics/punkette-icon.png" (
  function()->(
  new game_unit_char "punkette" 
      0 0 35 41 
      "medias/units/rebels/punkette/pics/punkette.png"
      1 1 
(* select *)
      [|
       "medias/units/rebels/punkette/sounds/pk_selec01.wav";
       "medias/units/rebels/punkette/sounds/pk_selec02.wav";
       "medias/units/rebels/punkette/sounds/pk_selec03.wav";
      |]
      {frames=[|0;0|];refresh=8;action=(function()->());sound=[|"none"|]}
(* move *)
      {frames=[|0;1;0;2|];refresh=2;action=(function()->());sound=[|
       "medias/units/rebels/punkette/sounds/pk_action02.wav";
       "medias/units/rebels/punkette/sounds/pk_action03.wav";
     |]} 
      {frames=[|3;4;5|];refresh=8;action=(function()->());sound=[|
       "medias/units/rebels/punkette/sounds/pk_attaque01.wav";
       "medias/units/rebels/punkette/sounds/pk_attaque02.wav";
       "medias/units/rebels/punkette/sounds/pk_attaque03.wav";
     |]}
      {frames=[|6;7;8|];refresh=8;action=(function()->());sound=[|"none"|]}
      4 4 3 0 2 3 3 0
      (new game_projectile "none" 32 32 "none" true {frames=[|2;1;0;1|];refresh=1;action=(function()->());sound=[|"none"|]})
 ));

  gm#add_obj_type "zork" "medias/units/rebels/zork/pics/zork-face.png" "medias/units/rebels/zork/pics/zork-icon.png"(
  function()->(
  new game_unit_char "zork" 
      0 0 45 55 
      "medias/units/rebels/zork/pics/zork.png"
      1 1 
      [|"medias/units/rebels/zork/sounds/zo_selec01.wav";
	"medias/units/rebels/zork/sounds/zo_selec02.wav";
	"medias/units/rebels/zork/sounds/zo_selec03.wav";
	"medias/units/rebels/zork/sounds/zo_selec04.wav";
	"medias/units/rebels/zork/sounds/zo_selec05.wav"|]


      {frames=[|0;0|];refresh=8;action=(function()->());sound=[|"none"|]}
      {frames=[|0;1;0;2|];refresh=2;action=(function()->());sound=
       [|
	 "medias/units/rebels/zork/sounds/zo_action01.wav";
	 "medias/units/rebels/zork/sounds/zo_action02.wav";
	 "medias/units/rebels/zork/sounds/zo_action03.wav";
	 "medias/units/rebels/zork/sounds/zo_action04.wav";
       |]

} 
      {frames=[|3;4|];refresh=4;action=(function()->());sound=[|
	 "medias/units/rebels/zork/sounds/zo_attaque01.wav";
	 "medias/units/rebels/zork/sounds/zo_attaque02.wav";
	 "medias/units/rebels/zork/sounds/zo_attaque03.wav";
	 "medias/units/rebels/zork/sounds/zo_attaque04.wav";
     |]}
      {frames=[|8;9;9|];refresh=8;action=(function()->());sound=[|"none"|]}
      4 3 3 0 3 4 4 0
      (new game_projectile "none" 32 32 "none" true {frames=[|0|];refresh=1;action=(function()->());sound=[|"none"|]})

 ));

  gm#add_obj_type "abdel" "medias/units/rebels/abdel/pics/abdel-face.png" "medias/units/rebels/abdel/pics/abdel-icon.png"(
  function()->(
  new game_unit_char "abdel" 
      0 0 60 65
      "medias/units/rebels/abdel/pics/abdel.png"
      1 1 
      [|"none"|]

      {frames=[|0;0|];refresh=8;action=(function()->());sound=[|"none"|]}
      {frames=[|0;1;3;4|];refresh=6;action=(function()->());sound=
      [|"none"|]} 
      {frames=[|5;6;7|];refresh=4;action=(function()->());sound=
      [|"none"|]}
      {frames=[|8;9;10|];refresh=8;action=(function()->());sound=[|"none"|]}
      8 6 5 0 4 6 3 0
      (new game_projectile "none" 32 32 "none" true {frames=[|0|];refresh=1;action=(function()->());sound=[|"none"|]})

 ));
  
  gm#add_obj_type "taverne" "none" "medias/buildings/rebels/taverne/pics/taverne-icon.png" (
  function()->(
    new game_unit_building "taverne" 0 1 213 148 "medias/buildings/rebels/taverne/pics/taverne.png" 
      6 4
      [|"none"|]
      {frames=[|0|];refresh=8;action=(function()->());sound=[|"none"|]}
      [|0;1;2;3|]
      5 5 4 2
   ));

  gm#add_obj_type "ferme"  "none" "medias/buildings/rebels/ferme/pics/ferme-icon.png" (
  function()->(
    new game_unit_building "ferme" 0 2 160 180 "medias/buildings/rebels/ferme/pics/ferme.png" 
      5 4
      [|"none"|]
      {frames=[|0|];refresh=8;action=(function()->());sound=[|"none"|]}
      [|0;1;2;3|]
      3 5 4 2
   ));

  gm#add_obj_type "festival"  "none" "medias/buildings/rebels/festival/pics/festival-icon.png" (
  function()->(
    new game_unit_building "festival" 0 0 320 364 "medias/buildings/rebels/festival/pics/festival.png" 
      6 4
      [|"none"|]
      {frames=[|0;1;2;3;4;5;6;7;8;9;10|];refresh=8;action=(function()->());sound=[|"none"|]}
      [|0;0;0;0|]
      5 5 4 2
   ));

  gm#add_obj_type "squat"  "none" "medias/buildings/rebels/squat/pics/squat-icon.png" (
  function()->(
    new game_unit_building "squat" 0 3 256 224 "medias/buildings/rebels/squat/pics/squat.png" 
      8 4
      [|"none"|]
      {frames=[|0|];refresh=8;action=(function()->());sound=[|"none"|]}
      [|0;1;2;3|]
      6 5 5 2
   ));


  gm#add_obj_type "tourelle" "none" "medias/buildings/rebels/tourelle/pics/tourelle-icon.png" (
  function()->(
    new game_unit_tower "tourelle" 
      0 4 128 192 
      "medias/buildings/rebels/tourelle/pics/tourelle.png"
      4 2
      [|"none"|]
      {frames=[|0|];refresh=8;action=(function()->());sound=[|"none"|]}
      {frames=[|0;1|];refresh=4;action=(function()->());sound=[|"none"|]}
      [|0;1;2;3|]
      6 4 6 4 6 4 3
      (new game_projectile "none" 32 32 "none" true {frames=[|0|];refresh=1;action=(function()->());sound=[|"medias/projectiles/snipe.wav"|]})
   ));

  (* neo naz *)
  gm#add_obj_type "brad_lab" "medias/units/neonazs/brad_lab/pics/brad_lab-face.png" "medias/units/neonazs/brad_lab/pics/brad_lab-icon.png"(
  function()->(
  new game_unit_char "brad_lab" 
      0 0 35 42
      "medias/units/neonazs/brad_lab/pics/brad_lab.png"
      1 1 
      [|
	"medias/units/neonazs/brad_lab/sounds/brad_selec01.wav";
	"medias/units/neonazs/brad_lab/sounds/brad_selec02.wav";
	"medias/units/neonazs/brad_lab/sounds/brad_selec03.wav";
	"medias/units/neonazs/brad_lab/sounds/brad_selec04.wav"
      |]
      {frames=[|0;0|];refresh=8;action=(function()->());sound=[|"none"|]}
      {frames=[|0;1;0;2|];refresh=2;action=(function()->());sound=[|
       "medias/units/neonazs/brad_lab/sounds/brad_action01.wav";
       "medias/units/neonazs/brad_lab/sounds/brad_action02.wav";
       "medias/units/neonazs/brad_lab/sounds/brad_action03.wav"
     |]} 
      {frames=[|3;4|];refresh=4;action=(function()->());sound=[|
       "medias/units/neonazs/brad_lab/sounds/brad_attaque01.wav";
       "medias/units/neonazs/brad_lab/sounds/brad_attaque02.wav";
       "medias/units/neonazs/brad_lab/sounds/brad_attaque03.wav";
     |]}
      {frames=[|5;6;7|];refresh=8;action=(function()->());sound=[|"none"|]}
      4 3 3 0 3 3 4 0
      (new game_projectile "none" 32 32 "none" true {frames=[|0|];refresh=1;action=(function()->());sound=[|"none"|]})

 ));



  gm#add_obj_type "mitrailleur"  "medias/units/neonazs/mitrailleur/pics/mitrailleur-face.png" "medias/units/neonazs/mitrailleur/pics/mitrailleur-icon.png" (
  function()->(
  new game_unit_char "mitrailleur" 
      0 0 43 60  
      "medias/units/neonazs/mitrailleur/pics/mitrailleur.png"
      1 1 
      [|"none"|]
      {frames=[|0;0|];refresh=8;action=(function()->());sound=[|"none"|]}
      {frames=[|0;1;0;2|];refresh=2;action=(function()->());sound=[|"none"|]} 
      {frames=[|3;4;5;6;7;8;6;7;8;6;7;8;5;4;3|];refresh=4;action=(function()->());sound=[|"none"|]}
      {frames=[|9;10;11;12;13;14;15;16|];refresh=8;action=(function()->());sound=[|"none"|]}
      4 2 2 3 3 4 3 2
      (new game_projectile "none" 32 32 "none" true {frames=[|0|];refresh=1;action=(function()->());sound=[|"medias/projectiles/mitraille.wav"|]})
 ));

  (* units : vitesse attaque1 defense1 attaque2 defense2 vie vue att_vue*)
  gm#add_obj_type "super_guerrier"  "medias/units/neonazs/super_guerrier/pics/super_guerrier-face.png" "medias/units/neonazs/super_guerrier/pics/super_guerrier-icon.png" (
  function()->(
  new game_unit_char "super_guerrier" 
      0 0 92 64  
      "medias/units/neonazs/super_guerrier/pics/super_guerrier.png"
      1 1 
      [|"none"|]
      {frames=[|0;0|];refresh=8;action=(function()->());sound=[|"none"|]}
      {frames=[|0;1|];refresh=2;action=(function()->());sound=[|"none"|]} 
      {frames=[|2;3;4;5;6;7;8;8;10;7;8;11;12;7;8;3;4;2;1|];refresh=4;action=(function()->());sound=[|"none"|]}
      {frames=[|13;14;15;16;17;18;19|];refresh=8;action=(function()->());sound=[|"none"|]}
      1 4 3 6 4 10 3 2
      (new game_projectile "none" 32 32 "none" true {frames=[|0|];refresh=1;action=(function()->());sound=[|"medias/projectiles/mitraille.wav"|]})
 ));


  gm#add_obj_type "bazooka" "medias/units/neonazs/bazooka/pics/bazooka-face.png"  "medias/units/neonazs/bazooka/pics/bazooka-icon.png" (
  function()->(
  new game_unit_char "bazooka" 
      0 0 43 46 
      "medias/units/neonazs/bazooka/pics/bazooka.png"
      1 1 

      [|
	"medias/units/neonazs/bazooka/sounds/baz_selec01.wav";
	"medias/units/neonazs/bazooka/sounds/baz_selec02.wav";
	"medias/units/neonazs/bazooka/sounds/baz_selec03.wav";
      |]
      {frames=[|0;0|];refresh=8;action=(function()->());sound=[|"none"|]}
      {frames=[|0;1;0;2|];refresh=2;action=(function()->());sound=[|
	"medias/units/neonazs/bazooka/sounds/baz_action01.wav";
	"medias/units/neonazs/bazooka/sounds/baz_action02.wav";
     |]} 
      {frames=[|3;4;5;5|];refresh=4;action=(function()->());sound=[|
       "medias/units/neonazs/bazooka/sounds/baz_attaque01.wav";
       "medias/units/neonazs/bazooka/sounds/baz_attaque02.wav";
       "medias/units/neonazs/bazooka/sounds/baz_attaque03.wav";       
     |]}
      {frames=[|6;7;8;9;10;11;12;13|];refresh=8;action=(function()->());sound=[|"none"|]}
      4 1 2 4 2 3 3 2
      (new game_projectile "rockette" 64 64 "medias/projectiles/rockette.png" true {frames=[|1;0;0;0;0;0;0;0|];refresh=1;action=(function()->());sound=[|"medias/projectiles/rockette.wav"|]})
 ));

  gm#add_obj_type "patrouilleur" "medias/units/neonazs/patrouilleur/pics/patrouilleur-face.png" "medias/units/neonazs/patrouilleur/pics/patrouilleur-icon.png" (
  function()->(
  new game_unit_char "patrouilleur" 
      0 0 43 52  
      "medias/units/neonazs/patrouilleur/pics/patrouilleur.png"
      1 1 
      [|
	"medias/units/neonazs/patrouilleur/sounds/pat_selec01.wav";
	"medias/units/neonazs/patrouilleur/sounds/pat_selec02.wav";
	"medias/units/neonazs/patrouilleur/sounds/pat_selec03.wav";
      |]
      {frames=[|0;0|];refresh=8;action=(function()->());sound=[|"none"|]}
      {frames=[|0;1;0;2|];refresh=2;action=(function()->());sound=[|
	"medias/units/neonazs/patrouilleur/sounds/pat_action01.wav";
	"medias/units/neonazs/patrouilleur/sounds/pat_action02.wav";
	"medias/units/neonazs/patrouilleur/sounds/pat_action03.wav";
     |]} 
      {frames=[|0;3;4;5;3;4;5;3;4;5;3;4;5|];refresh=4;action=(function()->());sound=[|
       "medias/units/neonazs/patrouilleur/sounds/pat_attaque01.wav";
       "medias/units/neonazs/patrouilleur/sounds/pat_attaque02.wav";
       "medias/units/neonazs/patrouilleur/sounds/pat_attaque03.wav";
       "medias/units/neonazs/patrouilleur/sounds/pat_attaque04.wav";
     |]}
      {frames=[|6;7;8;9;10;11;12;13|];refresh=8;action=(function()->());sound=[|"none"|]}
      4 3 3 0 2 3 3 0
      (new game_projectile "none" 32 32 "none" true {frames=[|0|];refresh=1;action=(function()->());sound=[|"none"|]})
 ));

  gm#add_obj_type "gymnaz" "none"  "medias/buildings/neonazs/gymnaz/pics/gymnaz-icon.png" (
  function()->(
    new game_unit_building "gymnaz" 0 1 224 209 "medias/buildings/neonazs/gymnaz/pics/gymnaz.png" 
      6 4
      [|"none"|]
      {frames=[|0|];refresh=8;action=(function()->());sound=[|"none"|]}
      [|0;1;2;3|]
      5 5 4 2
   ));

  gm#add_obj_type "labo"  "none" "medias/buildings/neonazs/labo/pics/labo-icon.png" (
  function()->(
    new game_unit_building "labo" 0 2 192 201 "medias/buildings/neonazs/labo/pics/labo.png" 
      6 4
      [|"none"|]
      {frames=[|0|];refresh=8;action=(function()->());sound=[|"none"|]}
      [|0;1;2;3|]
      4 5 4 2
   ));

  gm#add_obj_type "caserne"  "none" "medias/buildings/neonazs/caserne/pics/caserne-icon.png" (
  function()->(
    new game_unit_building "caserne" 0 4 256 256 "medias/buildings/neonazs/caserne/pics/caserne.png" 
      8 4
      [|"none"|]
      {frames=[|0;1|];refresh=8;action=(function()->());sound=[|"none"|]}
      [|0;2;4;6|]
      5 5 4 2
   ));

  gm#add_obj_type "usine_vehicule" "none" "medias/buildings/neonazs/usine_vehicule/pics/usine_vehicule-icon.png" (
  function()->(
    new game_unit_building "usine_vehicule" 0 2 224 181 "medias/buildings/neonazs/usine_vehicule/pics/usine_vehicule.png" 
      6 4
      [|"none"|]
      {frames=[|0|];refresh=8;action=(function()->());sound=[|"none"|]}
      [|0;1;2;3|]
      6 5 4 2
   ));

  (* units : attaque1 defense1 attaque2 defense2 vie vue att_vue*)
  gm#add_obj_type "mirador" "none" "medias/buildings/neonazs/mirador/pics/mirador-icon.png" (
  function()->(
    new game_unit_tower "mirador" 
      0 10 144 399 
      "medias/buildings/neonazs/mirador/pics/mirador.png"
      4 2
      [|"none"|]
      {frames=[|0|];refresh=8;action=(function()->());sound=[|"none"|]}
      {frames=[|0;1;2|];refresh=4;action=(function()->());sound=[|"none"|]}
      [|0;1;2;3|]
      6 4 6 4 6 4 3
      (new game_projectile "none" 32 32 "none" true {frames=[|0|];refresh=1;action=(function()->());sound=[|"medias/projectiles/mitraille.wav"|]})
   ));;
 
(*
  gm#add_obj_type "camp_torture" "none" "none" (
  function()->(
    new game_unit_building "camp_torture" 1 10 192 287 "medias/buildings/neonazs/camp_torture/pics/camp_torture.png" 
      6 4
      [|"none"|]
      {frames=[|0|];refresh=8;action=(function()->());sound=[|"none"|]}
      [|0;5;10;15|]
      5 5 4 2
   ));
*)


let game_map_set_ai gm pnum=
  if (gm#get_player_layer pnum)#get_name="rebel" then
    (
      gm#foreach_ennemy_player (
	function i->(

	  gm#get_ai#add_rule		       
		       (function()->(gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 10 "punkette")) 
		       (function()->(gm#get_ai#attack (gm#get_player_layer pnum) (gm#get_player_layer i) "punkette" 10 gm#unit_move_for_attack));

	  gm#get_ai#add_rule 
	    (function()->(gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 15 "lance_pepino")) 
	    (function()->(gm#get_ai#attack (gm#get_player_layer pnum) (gm#get_player_layer i) "lance_pepino" 10 gm#unit_move_for_attack));
     
	  gm#get_ai#add_rule 
	    (function()->(gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 8 "guerrier_bud")) 
	    (function()->(gm#get_ai#attack (gm#get_player_layer pnum) (gm#get_player_layer i) "guerrier_bud" 5 gm#unit_move_for_attack));
	  
	  gm#get_ai#add_rule 
	    (function()->(gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 0 "taverne")) 
	    (function()->(gm#get_ai#build_first (gm#get_player_layer pnum) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move));
	  
	  gm#get_ai#add_rule 
	    (function()->((gm#get_ai#have_more_resource (gm#get_player_layer pnum) 20 "nature")) 
	       && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 3 "ferme"))
	       && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 1 "taverne")))
	    (function()->(gm#get_ai#build (gm#get_player_layer pnum) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move "taverne" "ferme"));
	  
	  gm#get_ai#add_rule 
	    (function()->((gm#get_ai#have_more_resource (gm#get_player_layer pnum) 30 "nature")) 
	       && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 0 "festival")))
	    (function()->(gm#get_ai#build (gm#get_player_layer pnum) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move "taverne" "festival"));
	  

	  gm#get_ai#add_rule 
	    (function()->((gm#get_ai#have_more_resource (gm#get_player_layer pnum) 40 "nature")) 
	       && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 1 "festival"))
	       && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 3 "tourelle")))
	    (function()->(gm#get_ai#build (gm#get_player_layer pnum) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move "festival" "tourelle"));

     
	  gm#get_ai#add_rule 
	    (function()->((gm#get_ai#have_more_resource (gm#get_player_layer pnum) 20 "nature")) 
	       && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 1 "festival"))
	       && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 1) 10 "punkette")))
	    (function()->(gm#get_ai#train (gm#get_player_layer pnum) "festival" "punkette" gm#train_unit));
	  
	  
	  gm#get_ai#add_rule 
	    (function()->((gm#get_ai#have_more_resource (gm#get_player_layer pnum) 20 "nature")) 
	       && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 1 "festival"))
	       && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 15 "lance_pepino")))
	    (function()->(gm#get_ai#train (gm#get_player_layer pnum) "festival" "lance_pepino" gm#train_unit));
	  
	  gm#get_ai#add_rule 
	    (function()->((gm#get_ai#have_more_resource (gm#get_player_layer pnum) 30 "nature")) 
	       && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 1 "festival"))
	       && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 8 "guerrier_bud")))
	    (function()->(gm#get_ai#train (gm#get_player_layer pnum) "festival" "guerrier_bud" gm#train_unit));
	  
	  gm#get_ai#add_rule 
	    (function()->(gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 4 "ferme"))
	    (function()->(gm#get_ai#plant (gm#get_player_layer pnum) "ferme" "nature" gm#plant_around gm#pay));
	  
	  gm#get_ai#add_rule 
	    (function()->(
	       (gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 2 "zork"))
	       && (gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 1 "taverne") 
	       && (gm#get_ai#have_more_resource (gm#get_player_layer pnum) 21 "nature")) 
	    (function()->(gm#get_ai#train (gm#get_player_layer pnum) "taverne" "zork" gm#train_unit));

	) 
      ) pnum "neonaz"	  
    );

  if (gm#get_player_layer pnum)#get_name="neonaz" then
    (
      gm#foreach_ennemy_player (
	function i->(
	  gm#get_ai#add_rule 
		       (function()->(gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 10 "patrouilleur")) 
		       (function()->(gm#get_ai#attack (gm#get_player_layer pnum) (gm#get_player_layer i) "patrouilleur" 10 gm#unit_move_for_attack));
	  
	  gm#get_ai#add_rule 
	    (function()->(gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 5 "mitrailleur")) 
	    (function()->(gm#get_ai#attack (gm#get_player_layer pnum) (gm#get_player_layer i) "mitrailleur" 5 gm#unit_move_for_attack));
	  
	  gm#get_ai#add_rule 
	    (function()->(gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 5 "bazooka")) 
	    (function()->(gm#get_ai#attack (gm#get_player_layer pnum) (gm#get_player_layer i) "bazooka" 5 gm#unit_move_for_attack));
	  
	  gm#get_ai#add_rule 
	    (function()->(gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 0 "gymnaz")) 
	    (function()->(gm#get_ai#build_first (gm#get_player_layer pnum) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move));
	  
	  gm#get_ai#add_rule 
	    (function()->((gm#get_ai#have_more_resource (gm#get_player_layer pnum) 20 "pollution")) 
	       && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 2 "labo"))
	       && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 1 "gymnaz")))
	    (function()->(gm#get_ai#build (gm#get_player_layer pnum) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move "gymnaz" "labo"));
	  
	  
	  gm#get_ai#add_rule 
	    (function()->((gm#get_ai#have_more_resource (gm#get_player_layer pnum) 40 "pollution")) 
	       && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 1 "caserne"))
	       && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 3 "mirador")))
	    (function()->(gm#get_ai#build (gm#get_player_layer pnum) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move "caserne" "mirador"));
	  
	  
	  gm#get_ai#add_rule 
	    (function()->((gm#get_ai#have_more_resource (gm#get_player_layer pnum) 20 "pollution")) 
	       && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 1 "caserne"))
	       && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 10 "patrouilleur")))
	    (function()->(gm#get_ai#train (gm#get_player_layer pnum) "caserne" "patrouilleur" gm#train_unit));
	  
	  gm#get_ai#add_rule 
	    (function()->((gm#get_ai#have_more_resource (gm#get_player_layer pnum) 30 "pollution")) 
	       && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 1 "caserne"))
	       && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 5 "mitrailleur")))
	    (function()->(gm#get_ai#train (gm#get_player_layer pnum) "caserne" "mitrailleur" gm#train_unit));

	  gm#get_ai#add_rule 
	    (function()->((gm#get_ai#have_more_resource (gm#get_player_layer pnum) 20 "pollution")) 
	       && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 1 "caserne"))
	       && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 5 "bazooka")))
	    (function()->(gm#get_ai#train (gm#get_player_layer pnum) "caserne" "bazooka" gm#train_unit));

	  gm#get_ai#add_rule 
	    (function()->(gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 3 "labo"))
	    (function()->(gm#get_ai#plant (gm#get_player_layer pnum) "labo" "pollution" gm#plant_around gm#pay));
	  
	  gm#get_ai#add_rule 
	    (function()->(
	       (gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 2 "brad_lab"))
	       && (gm#get_ai#have_more_units_of_type (gm#get_player_layer pnum) 1 "gymnaz") 
	       && (gm#get_ai#have_more_resource (gm#get_player_layer pnum) 21 "pollution")) 
	    
	    (function()->(gm#get_ai#train (gm#get_player_layer pnum) "gymnaz" "brad_lab" gm#train_unit));
	  
	  gm#get_ai#add_rule 
	    (function()->((gm#get_ai#have_more_resource (gm#get_player_layer pnum) 30 "pollution")) 
	       && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer pnum) 0 "caserne")))
	    (function()->(gm#get_ai#build (gm#get_player_layer pnum) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move "gymnaz" "caserne"));
	  
	  
	)
      ) pnum "rebel"
    )  

let game_map_set_ai_OLD gm double_ia=
  if gm#get_iam_string="neonaz" || double_ia==true then 
    (


     gm#get_ai#add_rule 
       (function()->(gm#get_ai#have_more_units_of_type (gm#get_player_1) 10 "punkette")) 
       (function()->(gm#get_ai#attack (gm#get_player_layer 1) (gm#get_player_layer 2) "punkette" 10 gm#unit_move_for_attack));

     gm#get_ai#add_rule 
       (function()->(gm#get_ai#have_more_units_of_type (gm#get_player_layer 1) 15 "lance_pepino")) 
       (function()->(gm#get_ai#attack (gm#get_player_layer 1) (gm#get_player_layer 2) "lance_pepino" 10 gm#unit_move_for_attack));
     
     gm#get_ai#add_rule 
       (function()->(gm#get_ai#have_more_units_of_type (gm#get_player_layer 1) 8 "guerrier_bud")) 
       (function()->(gm#get_ai#attack (gm#get_player_layer 1) (gm#get_player_layer 2) "guerrier_bud" 5 gm#unit_move_for_attack));

     gm#get_ai#add_rule 
       (function()->(gm#get_ai#have_less_units_of_type (gm#get_player_layer 1) 0 "taverne")) 
       (function()->(gm#get_ai#build_first (gm#get_player_layer 1) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move));

     gm#get_ai#add_rule 
       (function()->((gm#get_ai#have_more_resource (gm#get_player_layer 1) 20 "nature")) 
	   && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 1) 3 "ferme"))
	   && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer 1) 1 "taverne")))
       (function()->(gm#get_ai#build (gm#get_player_layer 1) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move "taverne" "ferme"));

     gm#get_ai#add_rule 
       (function()->((gm#get_ai#have_more_resource (gm#get_player_layer 1) 30 "nature")) 
	   && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 1) 0 "festival")))
       (function()->(gm#get_ai#build (gm#get_player_layer 1) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move "taverne" "festival"));


     gm#get_ai#add_rule 
       (function()->((gm#get_ai#have_more_resource (gm#get_player_layer 1) 40 "nature")) 
	   && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer 1) 1 "festival"))
	   && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 1) 3 "tourelle")))
       (function()->(gm#get_ai#build (gm#get_player_layer 1) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move "festival" "tourelle"));

     
     gm#get_ai#add_rule 
       (function()->((gm#get_ai#have_more_resource (gm#get_player_layer 1) 20 "nature")) 
	   && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer 1) 1 "festival"))
	   && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 1) 10 "punkette")))
       (function()->(gm#get_ai#train (gm#get_player_layer 1) "festival" "punkette" gm#train_unit));


     gm#get_ai#add_rule 
       (function()->((gm#get_ai#have_more_resource (gm#get_player_layer 1) 20 "nature")) 
	   && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer 1) 1 "festival"))
	   && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 1) 15 "lance_pepino")))
       (function()->(gm#get_ai#train (gm#get_player_layer 1) "festival" "lance_pepino" gm#train_unit));

     gm#get_ai#add_rule 
       (function()->((gm#get_ai#have_more_resource (gm#get_player_layer 1) 30 "nature")) 
	   && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer 1) 1 "festival"))
	   && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 1) 8 "guerrier_bud")))
       (function()->(gm#get_ai#train (gm#get_player_layer 1) "festival" "guerrier_bud" gm#train_unit));

     gm#get_ai#add_rule 
       (function()->(gm#get_ai#have_less_units_of_type (gm#get_player_layer 1) 4 "ferme"))
       (function()->(gm#get_ai#plant (gm#get_player_layer 1) "ferme" "nature" gm#plant_around gm#pay));

     gm#get_ai#add_rule 
       (function()->(
	 (gm#get_ai#have_less_units_of_type (gm#get_player_layer 1) 2 "zork"))
	   && (gm#get_ai#have_more_units_of_type (gm#get_player_layer 1) 1 "taverne") 
	   && (gm#get_ai#have_more_resource (gm#get_player_layer 1) 21 "nature")) 
       (function()->(gm#get_ai#train (gm#get_player_layer 1) "taverne" "zork" gm#train_unit));
     
     

    );

  if gm#get_iam_string="rebel" || double_ia==true then 
    (
     
     gm#get_ai#add_rule 
       (function()->(gm#get_ai#have_more_units_of_type (gm#get_player_layer 2) 10 "patrouilleur")) 
       (function()->(gm#get_ai#attack (gm#get_player_layer 2) (gm#get_player_layer 1) "patrouilleur" 10 gm#unit_move_for_attack));
     
     gm#get_ai#add_rule 
       (function()->(gm#get_ai#have_more_units_of_type (gm#get_player_layer 2) 5 "mitrailleur")) 
       (function()->(gm#get_ai#attack (gm#get_player_layer 2) (gm#get_player_layer 1) "mitrailleur" 5 gm#unit_move_for_attack));

     gm#get_ai#add_rule 
       (function()->(gm#get_ai#have_more_units_of_type (gm#get_player_layer 2) 5 "bazooka")) 
       (function()->(gm#get_ai#attack (gm#get_player_layer 2) (gm#get_player_layer 1) "bazooka" 5 gm#unit_move_for_attack));

     gm#get_ai#add_rule 
       (function()->(gm#get_ai#have_less_units_of_type (gm#get_player_layer 2) 0 "gymnaz")) 
       (function()->(gm#get_ai#build_first (gm#get_player_layer 2) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move));
     
     gm#get_ai#add_rule 
       (function()->((gm#get_ai#have_more_resource (gm#get_player_layer 2) 20 "pollution")) 
	   && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 2) 2 "labo"))
	   && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer 2) 1 "gymnaz")))
       (function()->(gm#get_ai#build (gm#get_player_layer 2) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move "gymnaz" "labo"));


     gm#get_ai#add_rule 
       (function()->((gm#get_ai#have_more_resource (gm#get_player_layer 2) 40 "pollution")) 
	   && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer 2) 1 "caserne"))
	   && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 2) 3 "mirador")))
       (function()->(gm#get_ai#build (gm#get_player_layer 2) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move "caserne" "mirador"));


     gm#get_ai#add_rule 
       (function()->((gm#get_ai#have_more_resource (gm#get_player_layer 2) 20 "pollution")) 
	   && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer 2) 1 "caserne"))
	   && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 2) 10 "patrouilleur")))
       (function()->(gm#get_ai#train (gm#get_player_layer 2) "caserne" "patrouilleur" gm#train_unit));

     gm#get_ai#add_rule 
       (function()->((gm#get_ai#have_more_resource (gm#get_player_layer 2) 30 "pollution")) 
	   && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer 2) 1 "caserne"))
	   && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 2) 5 "mitrailleur")))
       (function()->(gm#get_ai#train (gm#get_player_layer 2) "caserne" "mitrailleur" gm#train_unit));

     gm#get_ai#add_rule 
       (function()->((gm#get_ai#have_more_resource (gm#get_player_layer 2) 20 "pollution")) 
	   && ((gm#get_ai#have_more_units_of_type (gm#get_player_layer 2) 1 "caserne"))
	   && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 2) 5 "bazooka")))
       (function()->(gm#get_ai#train (gm#get_player_layer 2) "caserne" "bazooka" gm#train_unit));

     gm#get_ai#add_rule 
       (function()->(gm#get_ai#have_less_units_of_type (gm#get_player_layer 2) 3 "labo"))
       (function()->(gm#get_ai#plant (gm#get_player_layer 2) "labo" "pollution" gm#plant_around gm#pay));
     
     gm#get_ai#add_rule 
       (function()->(
	 (gm#get_ai#have_less_units_of_type (gm#get_player_layer 2) 2 "brad_lab"))
	   && (gm#get_ai#have_more_units_of_type (gm#get_player_layer 2) 1 "gymnaz") 
	   && (gm#get_ai#have_more_resource (gm#get_player_layer 2) 21 "pollution")) 

       (function()->(gm#get_ai#train (gm#get_player_layer 2) "gymnaz" "brad_lab" gm#train_unit));
     
     gm#get_ai#add_rule 
       (function()->((gm#get_ai#have_more_resource (gm#get_player_layer 2) 30 "pollution")) 
	   && ((gm#get_ai#have_less_units_of_type (gm#get_player_layer 2) 0 "caserne")))
       (function()->(gm#get_ai#build (gm#get_player_layer 2) gm#construct gm#get_construct gm#can_construct_here gm#get_obj_type gm#unit_move "gymnaz" "caserne"));
     
    );;

(* FIXME : must fix for multiplayer *)
let game_set_cond_freestyle gm=
  let resource_my=
    match gm#get_iam_string with
      | "rebel" -> "nature"
      | "neonaz" -> "pollution"
      | _ -> "none" in
(*  and


*)
    gm#set_lose_cond (function()->(
			let cond=ref false in
			cond:=((gm#get_player_layer) (gm#get_iam))#count_obj==0;
			for oth=1 to gm#get_num_players do
			  if oth<>gm#get_iam && (gm#get_player_layer oth)#get_name<>gm#get_iam_string then (
			    let resource_oth=
			      match (gm#get_player_layer oth)#get_name with
				| "rebel" -> "nature"
				| "neonaz" -> "pollution"
				| _ -> "none" in
			      cond:=!cond || ((gm#get_player_layer oth)#resource_get resource_oth)>=1000;
			      cond:=!cond || ((gm#get_player_layer oth)#resource_get "boucherie")>=100 
			  )
			done;
			!cond
		      ));

    gm#set_win_cond (function()->(
		       let cond=ref false in

			 gm#foreach_ennemy_player (
			   function oth -> (
			     cond:= !cond || ((gm#get_player_layer) (oth))#count_obj==0
			 ));

		       
		       cond:= !cond || ((gm#get_player_layer gm#get_iam)#resource_get resource_my)>=1000;
		       cond:= !cond || ((gm#get_player_layer gm#get_iam)#resource_get "boucherie")>=100;
		       !cond
		     ));;

(* FIXME : must fix for multiplayer *)
let game_set_cond_mission1 gm=
  let oth=if gm#get_iam=1 then 2 else 1 in
  gm#set_lose_cond (function()->(
  ((gm#get_player_layer) (gm#get_iam))#count_obj==0
 ));
  gm#set_win_cond (function()->(
    let r=ref false in
    for k=1 to (gm#get_player_layer gm#get_iam)#get_cur_obj do
      let x=((gm#get_player_layer gm#get_iam)#get_obj k)#get_rect#get_x and
	  y=((gm#get_player_layer gm#get_iam)#get_obj k)#get_rect#get_y in
      if x>=90 && y>=56 then r:=true;      
    done;
    !r
(*  ((gm#get_player_layer) (oth))#count_obj==0 *)
 ));;


let ai_patrol p gm x y dx dy=
  gm#get_ai#add_rule 
  (function()->(gm#get_ai#is_unit_at (gm#get_player_layer p) x y))
  (function()->(gm#get_ai#move_one (gm#get_player_layer p) x y dx dy gm#unit_move));
  gm#get_ai#add_rule 
  (function()->(gm#get_ai#is_unit_at (gm#get_player_layer p) dx dy))
  (function()->(gm#get_ai#move_one (gm#get_player_layer p) dx dy x y gm#unit_move));;

let game_set_ai_mission1 gm=
  ai_patrol 2 gm 55 8 71 16; 
  ai_patrol 2 gm 54 17 44 13; 
  ai_patrol 2 gm 48 42 27 14; 
  ai_patrol 2 gm 31 57 32 46; 
  ai_patrol 2 gm 37 39 47 31; 
  ai_patrol 2 gm 7 45 6 27; 
  ai_patrol 2 gm 78 32 68 39; 
  ai_patrol 2 gm 82 62 85 52;;

        
let game_map_freestyle_init w h player t=
  let gm=game_map_init w h player in
    game_map_set_types gm;

    gm#foreach_other_player (
      function i->(			
	game_map_set_ai gm i;
      ));

    game_set_cond_freestyle gm;
    gm#freestyle_init();
    gm#tile_init t;
    gm#freestyle_resource 10 10;
    gm;;


let game_map_network_init w h player t=
  let gm=game_map_init w h player in
    game_map_set_types gm;
    gm#tile_init t;
    gm#network_init player;
    
    gm;;


let game_map_mission_init filename w h player=
  let gm=game_map_init w h player in
  game_map_set_types gm;
  gm#load_map w h filename;
  gm;;

