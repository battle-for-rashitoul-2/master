(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Rect;;
open Low;;

(** Game object selection class definition *) 

class game_selection nb=
  object
    val mutable selected=Array.make nb 0
    val mutable last_selected=ref 0

    method get_nb_selected= !last_selected

    method add_selected num=
      if !last_selected < nb-1 then (
	selected.(!last_selected)<-num;
	last_selected:=!last_selected+1;
       )
    method del_selected num=
      Array.blit selected (num+1) selected (num) (!last_selected - num);
      last_selected:=!last_selected-1;

    method del_all_selected ()=
      for i=0 to nb-1 do
	selected.(i)<-0;
      done;
      last_selected:=0;

    method get_selected num=selected.(num)
    method set_selected num v=selected.(num)<-v
      
  
  end;;



class game_multiple_selection=
  object (self)
    val mutable pressed=false
    val mutable start_sel=false
    val mutable rect=new rectangle 0 0 0 0;

    method get_pressed=pressed;
    method set_pressed v=pressed<-v;

    method get_start_sel=start_sel;
    method set_start_sel v=start_sel<-v;

    method get_rect=rect

    method clear()=
      if start_sel==false then (
	rect#set_size 0 0;
       )
    method create()=
      if rect#get_x<>0 && rect#get_y<>0 && rect#get_w<>0 && rect#get_h<>0 then (
	let rx=ref 0 and ry=ref 0 and rw=ref 0 and rh=ref 0 in
	if rect#get_w >= rect#get_x then (rx:= rect#get_x;rw:= rect#get_w) 
	else (rx:= rect#get_w;rw:= rect#get_x); 
	if rect#get_h >= rect#get_y then (ry:= rect#get_y;rh:= rect#get_h) 
	else (ry:= rect#get_h;rh:= rect#get_y);     
(*	rect#set_position (!rx) (!ry);
	rect#set_size (!rw) (!rh);
*)
	let sel_rect=tile_rect (!rw - !rx) (!rh - !ry) (0,0,255) in
	tile_put sel_rect !rx !ry;
	tile_free sel_rect;
       )

end;;

(*
let selected=Array.make 10 0;;
let last_selected=ref 0;;
let add_selected num=
  selected.(!last_selected)<-num;
  last_selected:=!last_selected+1;;

let del_selected num=
  for i=0 to !last_selected do
    if selected.(i)=num then (
      selected.(i)<-0;
      Array.blit selected (i+1) selected (i) (!last_selected - i);
      last_selected:=!last_selected-1;
     )
done;;

let del_all_selected ()=
  for i=0 to !last_selected do
      selected.(i)<-0;
      last_selected:=0;
done;;
*)
