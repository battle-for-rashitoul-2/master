(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)

open Low;;
open Rect;;
open Config;;
open Video;;
open Object;;
open Music;;
open Interface;;
open File;;

open Game_main;;

class iface_help fnt txt=
  object
    inherit iface_text fnt (0,0,0) txt as super
    val mutable corner=new graphic_scr_resized_object 32 32 "medias/interface/bgs/pic_bulle.png" false false
    val mutable coner=new graphic_scr_resized_object 176 240 "medias/interface/bgs/nico.png" false false
    method put()=
      if showing==true then (
	coner#move (graphic#get_rect#get_x + graphic#get_rect#get_w + 16) (graphic#get_rect#get_y - graphic#get_rect#get_h - 100);
	coner#put();
	let pic=corner#get_tile 0 in
	let re=tile_box (graphic#get_rect#get_w+10) (graphic#get_rect#get_h+10) (192,192,192) in 
	tile_put re (graphic#get_rect#get_x-5) (graphic#get_rect#get_y-5);
	tile_put pic (graphic#get_rect#get_x + graphic#get_rect#get_w - 3) (graphic#get_rect#get_y - corner#get_rect#get_h + 3);
	tile_free re;
	super#put();
       );

  end;;

class iface_player fnt sw sh=
object (self)
  inherit iface_graphic_file_object "medias/lecteur/lecteur_bg.png" 231 111 as super
      
  val mutable musics=load_musics()
  val mutable music=music_empty()
  val mutable c_music=2

  val mutable music_label_y=0
  val mutable music_label_x=0
  val mutable cur_music_x=0
  val mutable music_label=new iface_label_dynamic fnt (0,0,0)

  val mutable music_vol=new iface_volume 12 2 4 2
      
  val mutable play_but=new iface_button "medias/lecteur/lecteur_but_play.png" 24 24
  val mutable stop_but=new iface_button "medias/lecteur/lecteur_but_stop.png" 24 24
  val mutable previous_but=new iface_button "medias/lecteur/lecteur_but_previous.png" 24 24
  val mutable next_but=new iface_button "medias/lecteur/lecteur_but_next.png" 24 24

  initializer 
    Callback.register "music_end" (let e()=
      if (is_music musics (c_music+1)) then (
	c_music<-c_music+1;
	cur_music_x<-music_label#get_rect#get_w;
	music_label#set_data_text musics.(c_music);
	
	music_free music;
	music<-play_music musics c_music
       )
      else
	(
	 c_music<-2;
	 cur_music_x<-music_label#get_rect#get_w;
	 music_label#set_data_text musics.(c_music);
	
	 music_free music;
	 music<-play_music musics c_music
	    
	)	
    in e);
    on_music_end(); 

    music_label#set_data_text musics.(c_music);
    cur_music_x<-music_label#get_rect#get_w;

    previous_but#set_click (function()->(
      if c_music>2 then (
	print_string "iface_player:previous";print_newline();
	c_music<-c_music-1;
	cur_music_x<-music_label#get_rect#get_w;
	music_label#set_data_text musics.(c_music);

	if music_playing()=1 then (
	  music_free music;
	  music<-play_music musics c_music
	 )

       )

     ));

    next_but#set_click (function()->(
      if (is_music musics (c_music+1)) then (
	print_string "iface_player:next";print_newline();
	c_music<-c_music+1;
	cur_music_x<-music_label#get_rect#get_w;
	music_label#set_data_text musics.(c_music);

	if music_playing()=1 then (
	  music_free music;
	  music<-play_music musics c_music
	 )

       )
     ));

    play_but#set_click (function()->(
     if music_playing()<>1 then (
       print_string "iface_player:play";print_newline();
       music<-play_music musics c_music
      )
     ));

    stop_but#set_click (function()->(
     if music_playing()=1 then (
       print_string "iface_player:stop";print_newline();
       music_free music
      )
     ));

    music_set_volume (127);
    music_vol#set_data (12);
    music_vol#set_click (function()->(
      (*print_int ((music_vol#get_data*128)/12);print_newline();*)
      let n=music_set_volume ((music_vol#get_data*128)/12) in ()
     ));

  method move x y=
    super#move x y;
    music_label#move (x+(f_size_w 65)) (y+(f_size_h 35));
    music_label_x<-(x+(f_size_w 65));
    music_label_y<-(y+(f_size_h 35));
    play_but#move (x+(f_size_w 40)) (y+(f_size_h 80));
    stop_but#move (x+(f_size_w 70)) (y+(f_size_h 80));
    previous_but#move (x+(f_size_w 10)) (y+(f_size_h 80));
    next_but#move (x+(f_size_w 100)) (y+(f_size_h 80));
    music_vol#move (x+(f_size_w 145)) (y+(f_size_h 65));

  method show()=
    super#show();
    music_label#show();
    play_but#show();
    stop_but#show();
    previous_but#show();
    next_but#show();
    music_vol#show();

  method hide()=
    super#hide();
    music_label#hide();
    play_but#hide();
    stop_but#hide();
    previous_but#hide();
    next_but#hide();
    music_vol#hide();

  method put()=
    super#put();

    video_set_clip (rect#get_x + (f_size_w 65)) (rect#get_y + (f_size_h 35)) (f_size_w 150) (f_size_h 20);
    if abs(cur_music_x)>music_label#get_rect#get_w*16 then
      cur_music_x<-music_label#get_rect#get_w*8
    else
      cur_music_x<-cur_music_x-8;

    music_label#move (music_label_x + cur_music_x/16) (music_label_y);    
    music_label#put();

    video_set_clip 0 0 sw sh;

    play_but#put();
    stop_but#put();
    previous_but#put();
    next_but#put();
    music_vol#put();

  method private click_pos obj x y=
    if x > obj#get_rect#get_x 
	&& x < (obj#get_rect#get_w + obj#get_rect#get_x) 
	&& y > obj#get_rect#get_y 
	&& y < (obj#get_rect#get_h + obj#get_rect#get_y) 
    then
      obj#on_click x y

  method private release_pos obj x y=
    if x > obj#get_rect#get_x 
	&& x < (obj#get_rect#get_w + obj#get_rect#get_x) 
	&& y > obj#get_rect#get_y 
	&& y < (obj#get_rect#get_h + obj#get_rect#get_y) 
    then
      obj#on_release x y

  method on_click x y=
    super#on_click x y;
    self#click_pos play_but x y;
    self#click_pos stop_but x y;
    self#click_pos previous_but x y;
    self#click_pos next_but x y;
    self#click_pos music_vol x y;

  method on_release x y=
    super#on_release x y;
    self#release_pos play_but x y;
    self#release_pos stop_but x y; 
    self#release_pos previous_but x y; 
    self#release_pos next_but x y;
    self#release_pos music_vol x y;     
end;;

(* constructbar widget *)
class iface_constructbar=
  object (self)
    inherit iface_object 0 0 as super

    method put()=
      if showing==true then (
	let tmp=tile_levelbar data data1 (0,255,0) (f_size_w(192),f_size_h(32))  in 
	tile_put tmp (rect#get_x) (rect#get_y);
	tile_free tmp;      

)
  end;;


class iface_resourcebar file filef wi hi c=
  object (self)
    inherit iface_graphic_file_object file wi hi as super
    val bg=new graphic_scr_resized_object wi hi filef false false
    method move x y=super#move x y;graphic#move x y;bg#move x y
    val mutable resource=0
    val mutable resource_tot=0
    val mutable boucherie=0
    val mutable boucherie_tot=0


    method set_data v=
      if data_text="resource" then
	resource<-v;
      if data_text="boucherie" then
	boucherie<-v;
    method get_data=
      if data_text="resource" then
	resource
      else
	boucherie;
    method set_data1 v=
      if data_text="resource" then
	resource_tot<-v;
      if data_text="boucherie" then
	boucherie_tot<-v;
      
    method put()=
      if showing==true then (

	bg#put();
	let nx=if c=1 then (f_size_w 215) else (f_size_w 200) in
	let color=if c=1 then (0,255,0) else (51,51,51) in
	let tmp=tile_levelbar resource resource_tot color (graphic#get_rect#get_w/2,graphic#get_rect#get_h - (f_size_h 10)) in 
	tile_put tmp (graphic#get_rect#get_x + (f_size_w 8)) (graphic#get_rect#get_y + (f_size_h 8));
	tile_free tmp;      
	let tmp=tile_levelbar  boucherie boucherie_tot (255,0,0) (graphic#get_rect#get_w/2,graphic#get_rect#get_h - (f_size_h 10)) in 
	tile_put tmp (graphic#get_rect#get_x + (f_size_w 8) + nx) (graphic#get_rect#get_y + (f_size_h 8));
	tile_free tmp;      
	   
	graphic#put();
       )
  end;;


(* (unit) face widget  *)
class iface_face_object file w h fnt=
  object
    inherit iface_graphic_file_object file w h as super
    method move x y=super#move x y;graphic#move x y
    method put()=
      if showing==true then (
	graphic#put();
	let nb=tile_text fnt#get_font (string_of_int(data)) (0,0,0) in
	tile_put nb (graphic#get_rect#get_x + (graphic#get_rect#get_w - (tile_get_w nb) - (f_size_w 12) )) (graphic#get_rect#get_y + (f_size_h 12));
	tile_free nb
       )
  end;;

class iface_button_construct file w h=
  object
    inherit iface_button file w h as super
    method put()=
      if showing==true then (
      if data=0 then
	super#put();
      if data=1 then (
	graphic#set_cur_tile 2;
	graphic#put()
       );
      if data=2 then (
	if is_clicked==false then
	  graphic#set_cur_tile 3
	else 
	  graphic#set_cur_tile 4;
	graphic#put();
       );
       )
  end;;




class iface_button_menu fnt txt file w h=
  object
    inherit iface_button_with_label fnt txt file w h as super
    val mutable p=0
    val mutable dyn_obj=new graphic_dyn_object ("label/button/dynamic/"^txt^":"^(string_of_int fnt#get_size)) 1 (function k->(tile_empty()))
    initializer

      dyn_obj<-(new graphic_dyn_object ("label/button/dynamic/"^txt^":"^(string_of_int fnt#get_size)) 1 (function k->(
	if k<31 then
	  (
	   let n=(-15) in
	   let bo=tile_copy(graphic#get_tile 0) in
	   let lo=label#get_tile 0 in	   
	   tile_put_to lo bo (((graphic#get_rect#get_w - label#get_rect#get_w)/2)) (((graphic#get_rect#get_h - label#get_rect#get_h)/2)); 
	   let bot=tile_rotozoom bo (float_of_int(n+(k*1))) (1.0) in
	   tile_free bo;
	   bot
	  )
	else
	  (
	   let n=15 in
	   let bo=tile_copy(graphic#get_tile 0) in
	   let lo=label#get_tile 0 in	   
	   tile_put_to lo bo (((graphic#get_rect#get_w - label#get_rect#get_w)/2)) (((graphic#get_rect#get_h - label#get_rect#get_h)/2));
	   let bot=tile_rotozoom bo (float_of_int(n-((k - 31)*1))) (1.0) in
	   tile_free bo; 
	   bot 
	  )

       )
													)
	       )	       

    method on_mouseout x y=super#on_mouseout x y;p<-0
    
    method put()=
      if showing==true then 
	(
	 if is_clicked==true then (
	   graphic#set_cur_tile 1;
	   is_mouseover<-false
	  )
	 else
	   graphic#set_cur_tile 0;


	 if is_mouseover==true && is_clicked==false then 
	   (
	    let nx=ref 0  and ny=ref 0 in
	    nx:=(graphic#get_rect#get_x); 
	    ny:=(graphic#get_rect#get_y)-(graphic#get_rect#get_h/3);
	    if p<16 then
	      (
	       let fact=float_of_int(p)/.float_of_int(15) in
	       let hf=fact*.float_of_int(graphic#get_rect#get_h/3) in
	       ny:=!ny+int_of_float(hf);
	      );
	    if 15<p && p<31 then
	      (
	       let fact=float_of_int(p-16)/.float_of_int(15) in
	       let hf=fact*.float_of_int(graphic#get_rect#get_h/3) in
	       ny:=!ny-int_of_float(hf) + (graphic#get_rect#get_h/3);
	      );
	    if 30<p && p<46 then
	      (
	       let fact=float_of_int(p-31)/.float_of_int(15) in
	       let hf=fact*.float_of_int(graphic#get_rect#get_h/3) in
	       ny:=!ny+int_of_float(hf);
	      );

	    if 45<p && p<61 then
	      (
	       let fact=float_of_int(p-46)/.float_of_int(15) in
	       let hf=fact*.float_of_int(graphic#get_rect#get_h/3) in
	       ny:=!ny-int_of_float(hf) + (graphic#get_rect#get_h/3);
	      );

	    dyn_obj#move (!nx) (!ny);
	    dyn_obj#set_cur_tile p; 
	    dyn_obj#put(); 
	    if p<61 then p<-p+1
	    else p<-0;
	      
	   )	     
	 else (
	   super#put();
	  )
	)

  end;;





let init_iface ifa iam_string=
      let player_fnt=new font_object "medias/fonts/samos.ttf" 12 in
      let infos_fnt=new font_object "medias/fonts/samos.ttf" 18 in
	ifa#add_object_n "constructbar" (new iface_constructbar);
	let obj=(ifa#get_object_char "constructbar") in
	  
	  obj#set_data 1;
	  obj#set_data1 1;

	  if iam_string="rebel" then
	    obj#move (f_size_w 290) (f_size_h 520);
	  if iam_string="neonaz" then
	    obj#move (f_size_w 350) (f_size_h 490); 
	  
	  ifa#add_object_n "construct_label" (new iface_label_static infos_fnt (0,0,0) (n("Construction en cours")));
	  let obj=(ifa#get_object_char "construct_label") in

	    if iam_string="rebel" then     
	      obj#move (f_size_w 290) (f_size_h 490);
	    if iam_string="neonaz" then
	      obj#move (f_size_w 350) (f_size_h 460);
	    
	    if iam_string="rebel" then
	      ifa#add_object_n "cancelbutton" (new iface_button "medias/interface/buttons/annuler_rebelle.png" 48 48);
	    if iam_string="neonaz" then
	      ifa#add_object_n "cancelbutton" (new iface_button "medias/interface/buttons/annuler_naz.png" 48 48);
	    
	    let obj=(ifa#get_object_char "cancelbutton") in
	      if iam_string="rebel" then
		obj#move (f_size_w 566) (f_size_h 477);
	      if iam_string="neonaz" then
		obj#move (f_size_w 635) (f_size_h 440); 
	      obj#hide();
	      
	      (* add infos objects *)
	      ifa#add_object_n "iface_pv" (new iface_label_static infos_fnt (0,0,0) "PV:");
	      ifa#add_object_n "iface_pv_val" (new iface_label_dynamic infos_fnt (0,0,0));
	      ifa#add_object_n "iface_ac" (new iface_label_static infos_fnt (0,0,0) "AC:");
	      ifa#add_object_n "iface_ac_val" (new iface_label_dynamic infos_fnt (0,0,0));
	      ifa#add_object_n "iface_dc" (new iface_label_static infos_fnt (0,0,0) "DC:");
	      ifa#add_object_n "iface_dc_val" (new iface_label_dynamic infos_fnt (0,0,0));
	      ifa#add_object_n "iface_ae" (new iface_label_static infos_fnt (0,0,0) "AE:");
	      ifa#add_object_n "iface_ae_val" (new iface_label_dynamic infos_fnt (0,0,0));
	      ifa#add_object_n "iface_de" (new iface_label_static infos_fnt (0,0,0) "DE:");
	      ifa#add_object_n "iface_de_val" (new iface_label_dynamic infos_fnt (0,0,0));
	      
	      (* set infos objects *)
	      let mw=(if iam_string="rebel" then 0 else if iam_string="neonaz" then 64 else 0) in
	      let obj=(ifa#get_object_char "iface_pv") in
		obj#move (f_size_w (mw+420)) (f_size_h 452);
		obj#hide();
		let obj=(ifa#get_object_char "iface_pv_val") in
		  obj#move (f_size_w (mw+450)) (f_size_h 452);
		  obj#hide();

		  let obj=(ifa#get_object_char "iface_ac") in
		    obj#move (f_size_w (mw+420)) (f_size_h 472);
		    obj#hide();
		    let obj=(ifa#get_object_char "iface_ac_val") in
		      obj#move (f_size_w (mw+450)) (f_size_h 472);
		      obj#hide();

		      let obj=(ifa#get_object_char "iface_dc") in
			obj#move (f_size_w (mw+420)) (f_size_h 492);
			obj#hide();
			let obj=(ifa#get_object_char "iface_dc_val") in
			  obj#move (f_size_w (mw+450)) (f_size_h 492);
			  obj#hide();
			  
			  let obj=(ifa#get_object_char "iface_ae") in
			    obj#move (f_size_w (mw+420)) (f_size_h 512);
			    obj#hide();
			    let obj=(ifa#get_object_char "iface_ae_val") in
			      obj#move (f_size_w (mw+450)) (f_size_h 512);
			      obj#hide();
			      
			      let obj=(ifa#get_object_char "iface_de") in
				obj#move (f_size_w (mw+420)) (f_size_h 532);
				obj#hide();
				let obj=(ifa#get_object_char "iface_de_val") in
				  obj#move (f_size_w (mw+450)) (f_size_h 532);
				  obj#hide();
				  
				  ifa#add_object_n "player" (new iface_player player_fnt (!scr_w) (!scr_h));
				  let pobj=(ifa#get_object_char "player") in
				    if iam_string="rebel" then	
				      pobj#move (f_size_w 290) (f_size_h 460);
				    if iam_string="neonaz" then
				      pobj#move (f_size_w 350) (f_size_h 465);
				    ifa
;;



