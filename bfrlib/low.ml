(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
(** Low-level (C) operations *)

(* types *)
type tile
type font
type color=int * int * int
type rect=int * int
type sound  
type music  
type movie
type event={
    mutable etype:string;
    mutable eval:string;
    mutable ebut:int;
    mutable ex:int;
    mutable ey:int;
  }

(* init *)
external all_init : unit -> unit = "all_init"

(* wm *)
external wm_set_caption : string -> string -> unit = "wm_set_caption"

(* video *)
external video_init : int -> int -> int -> bool -> tile = "video_init"
external video_blank_screen : unit -> unit = "video_blank_screen"
external video_update : unit-> unit = "video_update"
external video_surface_get : unit -> tile = "video_surface_get"
external video_set_clip : int -> int -> int -> int -> unit = "video_set_clip"
external is_video : unit -> bool = "is_video"

(* tile *)
external tile_free : tile -> unit = "tile_free"
external tile_load : string -> tile = "tile_load"
external tile_get_w : tile -> int = "tile_get_w"
external tile_get_h : tile -> int = "tile_get_h"
external tile_resize : tile -> float -> float -> tile = "tile_resize"
external tile_rotozoom : tile -> float -> float -> tile = "tile_rotozoom"
external tile_copy : tile -> tile = "tile_copy"
external tile_empty : unit -> tile = "tile_empty"
external tiles_load : string -> int -> int -> tile array= "tiles_load"
external tiles_load_with_mirror_space : string -> int -> int -> tile array= "tiles_load_with_mirror_space"
external tile_set_alpha : tile -> int -> int -> int -> unit = "tile_set_alpha"
external tile_put : tile -> int -> int -> unit = "tile_put"
external tile_put_to : tile -> tile -> int -> int -> unit = "tile_put_to"
external tile_put_rect : tile -> int -> int -> int -> int -> unit = "tile_put_rect"
external tiles_put : tile array -> int -> int ->  tile = "tiles_put"
external tile_rect : int -> int -> color -> tile = "tile_rect"
external tile_box : int -> int -> color -> tile = "tile_box"
external tile_get : tile -> int -> int -> int -> int -> tile = "tile_get"
external tile_mirror : tile -> tile =  "tile_mirror" 
external tile_blackit : tile -> tile = "tile_blackit"
external tile_shade : tile -> tile = "tile_shade"
external tile_ellipse : int -> int -> color -> tile = "tile_ellipse"

(* FIXME : BFR specific *)
external tile_minimap : int array array -> int array array -> int array array -> int -> int -> tile = "tile_minimap"
external tile_levelbar : int -> int -> color -> rect -> tile = "tile_levelbar"
external tile_lifebar : int -> int -> tile = "tile_lifebar"


(* font *)
external font_empty : unit -> font = "font_empty"
external font_height : font -> int = "font_height"
external font_sizeof : font -> string -> ( int * int ) = "font_sizeof"
external font_load : string -> int -> font = "font_load"
external font_free : font -> unit = "font_free"
external tile_text : font -> string -> color -> tile = "tile_text"

(* FX *)
external fadein : unit -> unit = "fadein"
external fadeout : unit -> unit = "fadeout"

(* frame *)
external frame_init : unit -> unit = "frame_init"
external frame_set : int -> unit = "frame_init"
external frame_delay : unit -> unit = "frame_delay"

(* audio *)
external audio_init : int -> int -> unit = "audio_init"
external audio_set_volume : int -> unit = "audio_set_volume"
external audio_close : unit -> unit = "audio_close"
external is_audio : unit -> bool = "is_audio"

(* sound *)
external sound_load : string -> sound = "sound_load"
external sound_empty : unit -> sound = "sound_empty"
external sound_free : sound -> unit = "sound_free"
external sound_set_volume : sound -> int -> unit = "sound_set_volume"
external sound_play : sound ->  int = "sound_play"
external sound_len : sound ->  int = "sound_len"
external sound_playing : int ->  bool = "sound_playing"
external sound_position : int -> int -> int ->  unit = "sound_position"
external sound_chan_set_volume : int -> int -> int = "sound_chan_set_volume"
external sound_chan_stop : int -> unit = "sound_chan_stop"

(* music *)
external music_load : string -> music = "music_load"
external music_play : music -> unit = "music_play"
external music_set_volume : int -> int = "music_set_volume"
external music_free : music -> unit = "music_free"
external music_playing : unit -> int = "music_playing"
external music_empty : unit -> music = "music_empty"
external on_music_end : unit -> int = "on_music_end"

(* movie *)
external movie_load : string -> movie = "movie_load"
external movie_play : movie -> unit = "movie_play"
external movie_free :  movie -> unit = "movie_free"

(* time *)
external time_get : unit -> int = "time_get"
external time_sleep : int -> unit = "time_sleep"

(* others *)
external get_mouse_button : int -> bool = "get_mouse_button"
external map_load : string -> (int array) = "map_load"
external event_loop : event Stack.t -> int -> unit = "event_loop"


