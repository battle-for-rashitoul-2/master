/*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <SDL.h>
#include <SDL_mixer.h>

#include <caml/alloc.h>
#include <caml/mlvalues.h>
#include <caml/callback.h>

#include "audio.h"
#include "common.h"

#define ML_SOUND(sample) (value)sample
#define C_SOUND(sample) ((Mix_Chunk *)sample)

#define ML_MUSIC(mus) (value)mus
#define C_MUSIC(mus)  ((Mix_Music *)mus)

#define CHANS_NUM 16

int audio=1;

int is_audio_available()
{
  return audio;
}

value is_audio(value t)
{
  if (audio==1) 
    return Val_true; 
  else 
    return Val_false;
}


value audio_init(value freq, value chans)
{
  // start SDL with audio support
  if(audio==1)
    {
  if(SDL_Init(SDL_INIT_AUDIO)==-1) {
    print_error("audio_init","Couldn't initialize SDL audio",SDL_GetError());
    exit(1);
    }
    }
  // open 44.1KHz, signed 16bit, system byte order,
  //      stereo audio, using 1024 byte chunks
  if(Mix_OpenAudio(Int_val(freq), MIX_DEFAULT_FORMAT, Int_val(chans), 4096)==-1) {
    print_error("audio_init","Error opening audio",Mix_GetError());
    printf("Bfrlib:Sound disabled.\n");
    //    exit(2); 
  } else audio=1;
  if (audio==1)
    Mix_AllocateChannels(CHANS_NUM);
}



value audio_set_volume(value vol)
{
  Mix_Volume(-1,Int_val(vol));
}

value audio_close(value r)
{
  Mix_CloseAudio();
}


value sound_load(value filename)
{
  Mix_Chunk *sample;
  sample=Mix_LoadWAV(String_val(filename));
  if(!sample) {
    print_error("sound_load","Error loading sound",Mix_GetError());
  }
  return ML_SOUND(sample);
  
}


value sound_empty(value r)
{
  Mix_Chunk *sample=NULL;
  return ML_SOUND(sample);
}

value sound_free(value s)
{
  Mix_Chunk *sample=C_SOUND(s);
  Mix_FreeChunk(sample);
  sample=NULL; 
}

// between 0 and 128
value sound_set_volume(value s, value vol)
{
  Mix_Chunk *sample=C_SOUND(s);
  Mix_VolumeChunk(sample, Int_val(vol));
}

value sound_chan_stop(value c)
{
  Mix_HaltChannel(Int_val (c));
}

value sound_chan_set_volume(value c, value vol)
{
  return Val_int(Mix_Volume(Int_val(c),Int_val(vol)));  
  return 0;
}
/* play sound and return channel where it is playing on */
value sound_play(value s)
{
  int r;
  Mix_Chunk *sample=C_SOUND(s);
  if (sample!=NULL){
    r=Mix_PlayChannel(-1, sample, 0);
    if (r==-1)
      print_error("sound_play","Error during play",Mix_GetError());
  }
  return Val_int(r);
}

value sound_len(value s)
{

  Mix_Chunk *sample=C_SOUND(s);
  return Val_int(sample->alen);

}

value sound_playing(value chan)
{
      int r;
      

      if (Int_val(chan)>CHANS_NUM)
	{
	  print_error("sound_playing","Error in channel num",Mix_GetError());
	  return Val_false;
	}
      r=Mix_Playing(Int_val(chan));
      


      if (r==1){ 
	return Val_true;
      }
      return Val_false;
}

value sound_stop(value chan)
{
  Mix_HaltChannel(Int_val(chan));
}

/* set the position for playing */
value sound_position(value chan,value distance, value angle)
{
  if(!Mix_SetPosition(Int_val(chan), Int_val(angle),Int_val(distance))) {
    printf("Mix_SetPosition: %s\n", Mix_GetError());
  }
    // no position effect, is it ok?
}


value music_load(value filen)
{
  Mix_Music *mus=Mix_LoadMUS(String_val(filen));
  if (!mus) {
    print_error("music_load","Error loading music",Mix_GetError());
  }
  return ML_MUSIC(mus);
  
}

value music_play(value m)
{
  Mix_PlayMusic(C_MUSIC(m), 1);
}

value music_free(value m)
{
  Mix_FreeMusic(C_MUSIC(m));
}

value music_set_volume(value vol)
{
  return Val_int(Mix_VolumeMusic(Int_val(vol)));
}

value music_playing(value r)
{
  return Val_int(Mix_PlayingMusic());
}

value music_empty(value r)
{
  return ML_MUSIC(NULL);
}

void cb_music_end(void)
{
  value f;
  f=*caml_named_value("music_end");
  callback(f,Val_unit);
}

value on_music_end(value c)
{
   Mix_HookMusicFinished(cb_music_end);
  return 0;
}
