/*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <SDL.h>
#include <SDL_ttf.h>

#include <caml/alloc.h>
#include <caml/mlvalues.h>
#include <caml/callback.h>
#include <caml/fail.h>
#include <caml/memory.h>

value all_init(value v)
{
 if ( SDL_Init(SDL_INIT_TIMER |  SDL_INIT_VIDEO) < 0 ) {
   print_error("video_init","Couldn't initialize SDL Video",SDL_GetError());
   exit(2);
 }
 atexit(SDL_Quit);

 /* Initialize the TTF library */
 if ( TTF_Init() < 0 ) {
   print_error("video_init", "Couldn't initialize TTF",SDL_GetError());
   exit(2);
 }
 atexit(TTF_Quit);

}

int print_error(char *func,char *err1,char *err2)
{
  printf("Bfrlib:%s:%s:%s\n",func,err1,err2);
return 0;
}
