/*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <SDL/SDL.h>
#include <SDL_mixer.h>
#include <smpeg/smpeg.h>

#include <caml/alloc.h>
#include <caml/mlvalues.h>
#include <caml/callback.h>
#include <caml/fail.h>

#include "audio.h"

#define ML_MOVIE(mov) (value)mov
#define C_MOVIE(mov) (SMPEG *)mov

void update(SDL_Surface *screen, Sint32 x, Sint32 y, Uint32 w, Uint32 h)
{
  SDL_Flip(SDL_GetVideoSurface());
}

value movie_load (value filename)
{
  SMPEG *mpeg;
  SMPEG_Info info;
  int use_audio=is_audio_available();
  SDL_Surface *scr=SDL_GetVideoSurface();
  mpeg = SMPEG_new(String_val(filename), &info, 0);
  if ( SMPEG_error(mpeg) ) {
    printf("movie: %s: %s\n", String_val(filename), SMPEG_error(mpeg));
    SMPEG_delete(mpeg);
  }

  if(use_audio==1)
    {    
      SDL_AudioSpec audiofmt;
      Uint16 format;
      int freq, channels;
      
      /* Tell SMPEG what the audio format is */
      Mix_QuerySpec(&freq, &format, &channels);
      audiofmt.format = format;
      audiofmt.freq = freq;
      audiofmt.channels = channels;
      SMPEG_actualSpec(mpeg, &audiofmt);
      
      /* Hook in the MPEG music mixer */
      Mix_HookMusic(SMPEG_playAudioSDL, mpeg);
    }
  SMPEG_enableaudio(mpeg, use_audio);

  SMPEG_enablevideo(mpeg, 1);
  SMPEG_setdisplay(mpeg, scr, NULL, NULL);
  /*  SMPEG_scaleXY(mpeg, scr->w, scr->h); */
 
 return ML_MOVIE(mpeg);
}

value movie_play (value mpeg)
{
  int q=0;
  SMPEG_play(C_MOVIE(mpeg));
  SDL_Event event;
  while (SMPEG_status(mpeg) == SMPEG_PLAYING && q==0)
    {
      while (SDL_PollEvent(&event)) {
	  switch(event.type)
	    {
	    case SDL_KEYUP:
	      if(event.key.keysym.sym==SDLK_ESCAPE)
		 {
		   q=1;
		 }
	      break;
	    default:
	      break;
	    }
      }
      SDL_Delay(40); 
    } 
   Mix_HookMusic(NULL, NULL);
   return Val_unit;
}


value movie_free(value mpeg)
{
  SMPEG_delete(C_MOVIE(mpeg));
  return Val_unit;
}
