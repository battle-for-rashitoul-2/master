/*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <SDL/SDL.h>
#include <SDL_framerate.h>
#include <caml/alloc.h>
#include <caml/mlvalues.h>
#include <caml/callback.h>

static char must_exit_loop;
FPSmanager fpsm;

value get_mouse_button(value b)
{
  SDL_PumpEvents();
 if(SDL_GetMouseState(NULL, NULL)&SDL_BUTTON(Int_val(b)))
   return Val_true;
 return Val_false;
}

value frame_init(value v)
{
  SDL_initFramerate(&fpsm);
  return Val_unit;
}

value frame_set(value fr)
{
  SDL_setFramerate(&fpsm, Int_val(fr)); 
  return Val_unit;
}

value frame_delay(value v)
{
  SDL_framerateDelay(&fpsm);
  return Val_unit;
}

value event_loop(value e,value fr)
{
  SDL_Event event;
  int t=0;
    int q=0;
  int t1,t2;
  int fps=Int_val(fr);
  value *add_event;
  value event_args[5];
  add_event=caml_named_value("add_event");

  q = 0;
  must_exit_loop = 0;
  
  //  SDL_setFramerate(&fpsm, Int_val(fr)); 
 
 while (q==0) 
    {
      //      SDL_Delay(2);
      if (SDL_PollEvent(&event)) 
	{
	  /*	  if ((SMPEG_status(cur_movie) != SMPEG_PLAYING) ) */
	  switch(event.type)
	    {
	    case SDL_KEYUP:
	      {
	      value f;
	      f=*caml_named_value("release_key");
	      callback(f,Val_int(event.key.keysym.sym));
	      break;
	      }
	    case SDL_KEYDOWN:
	      {
	      value f;
	      f=*caml_named_value("parse_key");
	      callback(f,Val_int(event.key.keysym.sym));
	      break;
	      }
	    case SDL_MOUSEBUTTONDOWN:	      
	    case SDL_MOUSEBUTTONUP:
	      {
		
		event_args[0]=copy_string("mouse");
		if(event.button.state == SDL_PRESSED)
		  event_args[1]=copy_string("pressed");
		else
		  event_args[1]=copy_string("released");
		switch (event.button.button )
		  {
		  case SDL_BUTTON_LEFT:
		    event_args[2]=Val_int(0);
		    break;
		  case SDL_BUTTON_MIDDLE:
		    event_args[2]=Val_int(1);
		    break;
		  case SDL_BUTTON_RIGHT:
		    event_args[2]=Val_int(2);
		    break;
		  default:
		    break;
		  }
		event_args[3]=Val_int(event.button.x);
		event_args[4]=Val_int(event.button.y);

		callbackN(*add_event,5,event_args);
	      }
	      break;
	    case SDL_MOUSEMOTION:
	      {
		event_args[0]=copy_string("mouse");
		event_args[1]=copy_string("motion");
		event_args[2]=Val_int(0);
		event_args[3]=Val_int(event.motion.x);
		event_args[4]=Val_int(event.motion.y);
		callbackN(*add_event,5,event_args);
	      }
	      break;
	    default:
	      break;
	    }
	}
	    else
	      {
		value f;	  
		/* t1=SDL_GetTicks(); */
		f=*caml_named_value("loop");
		callback(f,Val_int(t));
		/*
		t2=SDL_GetTicks();
		printf("fps: %i\n",1000/(t2-t1));
		if ((1000/(t2-t1))>fps)
		  {
		    SDL_Delay(1000/fps - (t2 - t1));
		  }
		*/
		  
		/*		t=SDL_getFramerate(&fpsm);		
		SDL_framerateDelay(&fpsm);
		*/
		//t=0;
	    }
	}
      

return Val_unit;		   
}
