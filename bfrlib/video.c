/*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
#include <SDL.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_imageFilter.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include "fading.h"

#include <caml/alloc.h>
#include <caml/mlvalues.h>
#include <caml/callback.h>
#include <caml/fail.h>
#include <caml/memory.h>

#include <math.h>

#include "common.h"

#define ML_TILE(surface) (value)surface
#define C_TILE(surface) ((SDL_Surface *)surface)

#define ML_FONT(font) (value)font
#define C_FONT(font) ((TTF_Font *)font)


int video=0;

SDL_Surface *display_format(SDL_Surface *sur);

/* internal funcs */
/* simple convert to display binding which freed old sur) */
SDL_Surface *display_format(SDL_Surface *sur)
{
  SDL_Surface *sur2=SDL_DisplayFormat(sur);
  if (sur2==NULL)
    {
      //      print_error("display_format","Error converting surface","");
      return sur;
    }
  else
    {
      SDL_FreeSurface(sur);
      return sur2;
    }
  }



/* WM */
value wm_set_caption(value title, value icon)
{  
  
  SDL_Surface *sicon=IMG_Load(String_val(icon));
  SDL_WM_SetCaption(String_val(title),NULL);
  if(sicon==NULL) 
    print_error("wm_set_caption","Can't load icon",IMG_GetError()); 
  SDL_WM_SetIcon(sicon,NULL);

  return Val_unit;
}

/* general video part */
/* init sdl video & more */
value video_init(value w, value h, value bpp, value fs)
{
  SDL_VideoInfo *vi;
  int flags=0;
  SDL_Surface *sur;

  vi=SDL_GetVideoInfo();

  /* Because doesn't work on windows */
  #ifndef WIN32  
    flags=(SDL_HWSURFACE | SDL_DOUBLEBUF);
  #endif

  if (Bool_val(fs)!=0) flags=flags | SDL_FULLSCREEN;
 
 

 sur = SDL_SetVideoMode(Int_val(w),
			 Int_val(h),
			 Int_val(bpp),
			flags );
 
 //printf("MMX : %i\n",SDL_imageFilterMMXdetect());
 SDL_ShowCursor(SDL_DISABLE);
 video=1;
 return ML_TILE(sur); 
}

value is_video(value t)
{
  if (video==1) 
    return Val_true; 
  else 
    return Val_false;
}

value video_surface_get(value t)
{
  return ML_TILE(SDL_GetVideoSurface());
}

/* update screen */
value video_update(value t)
{
  SDL_Flip(SDL_GetVideoSurface());
  return Val_unit;
}

value video_blank_screen(value t)
{
  SDL_FillRect(SDL_GetVideoSurface(), NULL, SDL_MapRGB((SDL_GetVideoSurface())->format, 0,0,0));
  return Val_unit;
}

/* tile part */

/* free tile */
value tile_free(value t)
{
  SDL_FreeSurface(C_TILE(t));
  return Val_unit;
}


value tile_get_w(value t)
{
  return Val_int(C_TILE(t)->w);
}


value tile_get_h(value t)
{
  return Val_int(C_TILE(t)->h);
}

value tile_resize(value s,value x,value y)
{
  SDL_Surface *sur1=C_TILE(s);
  SDL_Surface *sur2;
  SDL_SetColorKey(sur1, 0,NULL);
  sur2=zoomSurface (sur1, Double_val(x), Double_val(y), 0);
  SDL_SetColorKey(sur1, SDL_SRCCOLORKEY | SDL_RLEACCEL, SDL_MapRGB(sur1->format, 255,255,255));
  SDL_SetColorKey(sur2, SDL_SRCCOLORKEY | SDL_RLEACCEL, SDL_MapRGB(sur2->format, 255,255,255));
  return ML_TILE(display_format(sur2));
}


value tile_rotozoom(value s,value a,value z)
{
  SDL_Surface *sur1=C_TILE(s);
  SDL_Surface *sur2;
  sur2=rotozoomSurface (sur1, Double_val(a), Double_val(z), 0);
  return ML_TILE(sur2);
}


value tile_empty(value n)
{
  SDL_Surface *sur=NULL;
  return ML_TILE(sur);
}


void tile_finalize (value v)  
{ 
  SDL_Surface *s=C_TILE(v);
  printf("FINALIZE TILE\n") ;
  SDL_FreeSurface(s);
}

/* load tile from image */
value tile_load(value filename)
{
  SDL_Surface *sur,*sur2;
  char *fname=String_val(filename);
  sur2=IMG_Load(fname);
  if (!sur2) {
    print_error("tile_load","Error loading file",IMG_GetError());
    exit(2);
  }
  sur=display_format(sur2);
  return ML_TILE(sur);
}


value tile_copy(value t)
{
  SDL_Surface *sur=C_TILE(t);
  SDL_Surface *sur2=SDL_DisplayFormat(sur);
  SDL_Rect tmprect;  
  return ML_TILE(sur2);
}


value tile_ellipse(value w, value h, value bg)
{
  SDL_Surface *sur=display_format(SDL_CreateRGBSurface(SDL_HWSURFACE, Int_val(w), Int_val(h), 16, 0,0,0,0));
  SDL_Color backg = { (unsigned char)Int_val( Field(bg, 0)),
		  (unsigned char)Int_val( Field(bg, 1)), 
		  (unsigned char)Int_val( Field(bg, 2)), 
		      0 };


  Uint32 white=SDL_MapRGBA(sur->format, 255,255,255,255);  
  SDL_FillRect(sur, NULL, white);
  filledEllipseRGBA(sur, Int_val(w)/2, Int_val(h)/2, Int_val(w)/2, Int_val(h)/2,backg.r,backg.g,backg.b,255);

  return ML_TILE(display_format(sur));
} 

value tile_blackit(value tile)
{
  SDL_Surface *sur=C_TILE(tile);
  int mmap_w=sur->w;
  int mmap_h=sur->h;
  SDL_Surface *sur_b=SDL_DisplayFormat(sur);
  int i,j;
  
  for(i=0;i<mmap_w;i++)
    {
      for(j=0;j<mmap_h;j++)
	{
	  pixelRGBA(sur_b, i,j,0,0,0,255); 
	}
    }
  return ML_TILE(sur_b);
}

value tile_shade(value tile)
{
  SDL_Surface *sur=C_TILE(tile);
  SDL_Surface *sur_b=SDL_CreateRGBSurface(SDL_HWSURFACE, sur->w, sur->h, 32, 0,0,0,0);
  SDL_Surface *sur2,*sur3;
  int r;
  sur2=SDL_ConvertSurface(sur,sur_b->format,SDL_HWSURFACE);
  sur3=SDL_ConvertSurface(sur,sur_b->format,SDL_HWSURFACE);
      
  SDL_imageFilterMMXon();
  r=SDL_imageFilterShiftRight(sur2->pixels, sur3->pixels, sur->w*sur->h*sur_b->format->BytesPerPixel,1);

  SDL_FreeSurface(sur2);
  SDL_FreeSurface(sur_b);
  return ML_TILE(display_format(sur3));
}
/* load a tile list from image cut in wxh + more space for load mirrored tile*/  
/* DIRTY */


value tiles_load_with_mirror_space(value filename,value w,value h)
{
  CAMLparam0();
  CAMLlocal1(a);
  SDL_Surface *sur,*sur2;
  int k=0;
  int i,j,x,y;
  int len=0;
  char *fname=String_val(filename);

  sur2=IMG_Load(fname);
  if (!sur2) {
    print_error("tile_load_with_mirror_space","Error loading file",IMG_GetError());
    exit(2);
  }
  sur=display_format(sur2);
  x=sur->w/Int_val(w);
  y=sur->h/Int_val(h);
  a=alloc((x+3)*y, 0);
  for(i=0;i<x+3;i++)
    {
      for(j=0;j<y;j++)
	{
	  SDL_Surface *tmpsur;
	  SDL_Rect tmprect;
	  tmprect.x=Int_val(w)*i;
	  tmprect.y=Int_val(h)*j;
	  tmprect.w=Int_val(w);
	  tmprect.h=Int_val(h);
	  
	  tmpsur=display_format(SDL_CreateRGBSurface(SDL_HWSURFACE, Int_val(w), Int_val(h), sur->format->BitsPerPixel, 0,0,0,0));


	  if(i<=x)
	    {
	      SDL_BlitSurface(sur, &tmprect, tmpsur, NULL);
	    }
	  Store_field(a, k, ML_TILE(tmpsur));
	  k++;
	}
    }
  CAMLreturn(a);
}     

/* load a tile list from image cut in wxh */  
value tiles_load(value filename,value w,value h)
{
  CAMLparam0();
  CAMLlocal1(a);
  SDL_Surface *sur,*sur2;
  int k=0;
  int i,j,x,y;
  int len=0;
  char *fname=String_val(filename);
  sur2=IMG_Load(fname);
  if (!sur2) {
    print_error("tiles_load","Error loading file",IMG_GetError());
    exit(2);
  }
  sur=display_format(sur2);
  x=sur->w/Int_val(w);
  y=sur->h/Int_val(h);
  a=alloc((x)*y, 0);
  for(i=0;i<x;i++)
    {
      for(j=0;j<y;j++)
	{
	  SDL_Surface *tmpsur;
	  SDL_Rect tmprect;
	  tmprect.x=Int_val(w)*i;
	  tmprect.y=Int_val(h)*j;
	  tmprect.w=Int_val(w);
	  tmprect.h=Int_val(h);

	  tmpsur=display_format(SDL_CreateRGBSurface(SDL_HWSURFACE, Int_val(w), Int_val(h), sur->format->BitsPerPixel, 0,0,0,0));

	  SDL_BlitSurface(sur, &tmprect, tmpsur, NULL);
	  Store_field(a, k, ML_TILE(tmpsur));
	  k++;
	}
    }

  CAMLreturn(a);
}     

/* set alpha color (pixel with alpha color isn't put on screen */ 
value tile_set_alpha(value tile,value r,value g, value b)
{
  SDL_Surface *sur=C_TILE(tile);
  SDL_SetColorKey(sur, SDL_SRCCOLORKEY | SDL_RLEACCEL, SDL_MapRGB(sur->format, Int_val(r), Int_val(g), Int_val(b)));
  return Val_unit;
}

/* put tile to screen at x,y */
value tile_put(value surface, value x, value y)
{
  SDL_Surface *sur=C_TILE(surface);
  if (sur!=NULL) {
  SDL_Rect rect;

  rect.x=Int_val(x);
  rect.y=Int_val(y);
  rect.w=sur->w;
  rect.h=sur->h;
  
  SDL_BlitSurface(sur, NULL,SDL_GetVideoSurface() , &rect);
  }
  return Val_unit;
}

/* put tile to another tile at x,y */
value tile_put_to(value surface,value dest, value x, value y)
{
  SDL_Surface *sur=C_TILE(surface);
  if (sur!=NULL){
    SDL_Rect rect;
    
    rect.x=Int_val(x);
    rect.y=Int_val(y);
    rect.w=sur->w;
    rect.h=sur->h;
    
    SDL_BlitSurface(sur, NULL,C_TILE(dest) , &rect);
  }
  else
    print_error("tile_put_to","Tile is NULL","");
  return Val_unit;
}

/* put part of a tile (x,y to w,h) to screen */
value tile_put_rect(value surface, value x, value y,value w, value h)
{
  SDL_Surface *sur=C_TILE(surface);
  SDL_Rect rect;
  SDL_Rect srect;
  int i,j;
  
  rect.x=Int_val(x);
  rect.y=Int_val(y);
  rect.w=Int_val(w);
  rect.h=Int_val(h);

  SDL_BlitSurface(sur, &rect,SDL_GetVideoSurface() , NULL);
  return Val_unit;
}

/* create a big tile from an array of tile */
value tiles_put(value a,value wc,value hc)
{
  SDL_Surface *sur;
  SDL_Surface *tmpsur1;
  int k=0;
  int i,j,x,y,h,w;

  tmpsur1=C_TILE(Field(a,0));
  w=tmpsur1->w*Int_val(wc);
  h=tmpsur1->h*Int_val(hc);

  sur=display_format(SDL_CreateRGBSurface(SDL_HWSURFACE, w, h, tmpsur1->format->BitsPerPixel, 0,0,0,0));

  for(i=0;i<Int_val(wc);i++)
    {
      for(j=0;j<Int_val(hc);j++)
	{
	  SDL_Surface *tmpsur;
	  SDL_Rect tmprect;
	  tmprect.x=tmpsur1->w*i;
	  tmprect.y=tmpsur1->h*j;
	  tmprect.w=tmpsur1->w;
	  tmprect.h=tmpsur1->h;
	  tmpsur=C_TILE(Field(a,k));
	  SDL_BlitSurface(tmpsur, NULL,sur,&tmprect);
	  k++;
	}
    }
  return ML_TILE(sur);
}     

/* create a minimap from 3 array : player array, ennemy array and fow array */
value tile_minimap(value obj_a,value en_a,value fow_a,value w, value h)
{
  int mmap_w=72;
  int mmap_h=72;
  SDL_Surface *sur=SDL_CreateRGBSurface(SDL_HWSURFACE,mmap_w , mmap_h, 32,0,0,0,0);
  int i,j;
  double fact_x=(double)mmap_w/(double)Int_val(w);
  double fact_y=(double)mmap_h/(double)Int_val(h);
  for(i=0;i<mmap_w;i++)
    {
      for(j=0;j<mmap_h;j++)
	{
	  int t=Int_val(Field(Field(fow_a,(int)((double)i/fact_x)),(int)((double)j/fact_y)));
	  int u=Int_val(Field(Field(obj_a,(int)((double)i/fact_x)),(int)((double)j/fact_y)));
	  int e=Int_val(Field(Field(en_a,(int)((double)i/fact_x)),(int)((double)j/fact_y)));
	  if(t==0) pixelRGBA(sur, i,j,0,0,0,255); 
	  if(t==1) pixelRGBA(sur, i,j,0,96,0,255); 
	  if(t==2)
	    {
	      if(u!=0)
		{
		  pixelRGBA(sur, i,j,255,255,255,255);
		}
	      else {
		if(e!=0)
		  {
		    pixelRGBA(sur, i,j,255,0,0,255);
		  }
		else pixelRGBA(sur, i,j,0,160,0,255);
	      }
	    }
	}
    }
  rectangleRGBA(sur, 0, 0,mmap_w-1, mmap_h-1,255,255,255,255);  
  return ML_TILE(display_format(sur));
}


/* create a levelbar  */
value tile_levelbar(value p, value pmax, value color, value rect)
{
  SDL_Surface *sur;
  
  SDL_Color c = { (unsigned char)Int_val( Field(color, 0)),
		  (unsigned char)Int_val( Field(color, 1)), 
		  (unsigned char)Int_val( Field(color, 2)), 
		      0 };

  int w=Int_val( Field(rect, 0));
  int h=Int_val( Field(rect, 1));
  sur=SDL_CreateRGBSurface(SDL_HWSURFACE, w , h, 32,0,0,0,0);
  Uint32 white=SDL_MapRGBA(sur->format, 255,255,255,255);  
  Uint32 black=SDL_MapRGBA(sur->format, 0,0,0,255);  
  int life=(int)(((double)Int_val(p)/(double)Int_val(pmax))*(double)w);
  SDL_FillRect(sur, NULL, white);
  SDL_SetColorKey(sur, SDL_SRCCOLORKEY | SDL_RLEACCEL, white); 
  
  boxRGBA(sur, 0, 0,life, h,c.r,c.g,c.b,255);    

  return ML_TILE(display_format(sur));    
}


/* create a lifebar  */
value tile_lifebar(value p, value pmax)
{
  SDL_Surface *sur=SDL_CreateRGBSurface(SDL_HWSURFACE, 32 , 5, 32,0,0,0,0);
  Uint32 white=SDL_MapRGBA(sur->format, 255,255,255,255);  
  Uint32 black=SDL_MapRGBA(sur->format, 0,0,0,255);  
  int life=(int)(((double)Int_val(p)/(double)Int_val(pmax))*(double)32);
  SDL_FillRect(sur, NULL, white);
  SDL_SetColorKey(sur, SDL_SRCCOLORKEY | SDL_RLEACCEL, white); 
  if (life>24 && life<=32) 
    boxRGBA(sur, 0, 0,life, 5,0,255,0,255);    
  if (life>16 && life<=24) 
    boxRGBA(sur, 0, 0,life, 5,255,255,0,255);    
  if (life>8 && life<=16) 
    boxRGBA(sur, 0, 0,life, 5,255,127,0,255);    
  if (life>=0 && life<=8) 
    boxRGBA(sur, 0, 0,life, 5,255,0,0,255);

  return ML_TILE(display_format(sur));    
}


/* create a rect of wxh (doesn't work on windows) */
value tile_rect(value w, value h, value fg)
{
  SDL_Surface *sur=SDL_CreateRGBSurface(SDL_HWSURFACE, Int_val(w) , Int_val(h), 32,0,0,0,0);
  SDL_Color forg = { (unsigned char)Int_val( Field(fg, 0)),
		  (unsigned char)Int_val( Field(fg, 1)), 
		  (unsigned char)Int_val( Field(fg, 2)), 
		      0 };
  Uint32 white=SDL_MapRGBA(sur->format, 255,255,255,255);  

  SDL_FillRect(sur, NULL, white);
  SDL_SetColorKey(sur, SDL_SRCCOLORKEY | SDL_RLEACCEL, white); 
  rectangleRGBA(sur, 0, 0,Int_val(w)-1, Int_val(h)-1,forg.r,forg.g,forg.b,255);

  return ML_TILE(display_format(sur)); 
}

value tile_box(value w,value h, value bg)
{
  SDL_Surface *sur=SDL_CreateRGBSurface(SDL_HWSURFACE, Int_val(w) , Int_val(h), 32,0,0,0,0);
  SDL_Color backg = { (unsigned char)Int_val( Field(bg, 0)),
		  (unsigned char)Int_val( Field(bg, 1)), 
		  (unsigned char)Int_val( Field(bg, 2)), 
		      0 };
  Uint32 b=SDL_MapRGBA(sur->format, backg.r,backg.g,backg.b,255);  

  boxRGBA(sur, 0, 0,Int_val(w)-1, Int_val(h)-1,backg.r,backg.g,backg.b,255);
  return ML_TILE(display_format(sur)); 
}





/* get tile part from other tile */
value tile_get(value surface,value x,value y,value w, value h)
{
  SDL_Surface *sur=C_TILE(surface);
  SDL_Surface *nsur=SDL_CreateRGBSurface(SDL_HWSURFACE, Int_val(w) , Int_val(h),sur->format->BitsPerPixel ,0,0,0,0);
  SDL_Rect rect;

  rect.x=Int_val(x);
  rect.y=Int_val(y);
  rect.w=Int_val(w);
  rect.h=Int_val(h);
  
  SDL_BlitSurface(sur,&rect,nsur,NULL);
  return ML_TILE(nsur);
}

/* create the horizontal mirror of a tile */
value tile_mirror (value surface)
{
  int i,j;
  SDL_Surface *sur=C_TILE(surface);
  SDL_Surface *fsurface=SDL_ConvertSurface(sur, sur->format, sur->flags);

  for(i=0;i<sur->h;i++)
    {
      for(j=0;j<sur->w;j++)
	{
	  Uint16 *p;
	  Uint16 *p2;
	  //Uint8 r,g,b;
	  
	  SDL_LockSurface(sur);	  
	  p=sur->pixels+i*sur->pitch+j*sur->format->BytesPerPixel;
	  SDL_UnlockSurface(sur);	  
	  
	  SDL_LockSurface(fsurface);
	  p2=fsurface->pixels+(i)*fsurface->pitch+(fsurface->w - j - 1)*fsurface->format->BytesPerPixel;
	  *p2=*p;
	  
	  SDL_UnlockSurface(fsurface);

	}
    } 
  

  return ML_TILE(fsurface);
}

/* set video surface clip */
value video_set_clip(value x, value y,value w, value h)
{
  SDL_Rect rect;
  rect.x=Int_val(x);
  rect.y=Int_val(y);
  rect.w=Int_val(w);
  rect.h=Int_val(h);
  
  SDL_SetClipRect(SDL_GetVideoSurface(), &rect);
  return Val_unit;
}


/* font */
/* load a font */
value font_empty(value n)
{
  TTF_Font *fnt=NULL;
  return ML_FONT(fnt);
}

value font_height(value f)
{
  TTF_Font *fnt=C_FONT(f);

  return Val_int((int)TTF_FontHeight(fnt));
}

value font_sizeof(value f,value s)
{
  TTF_Font *fnt=C_FONT(f);
  value a;
  int w=0,h=0;
  TTF_SizeText(fnt, String_val(s),&w,&h);
  a=alloc_small(2, 0);
  Field(a,0)=Val_int(w);
  Field(a,1)=Val_int(h);
  return a;
}


value font_load(value fontname, value ptsize)
{
  TTF_Font *font;
  font = TTF_OpenFont(&Byte(fontname,0), Int_val(ptsize));
  if(!font) {
    print_error("font_load","Error loading file",IMG_GetError()); 
    exit (2);
  }
  return ML_FONT(font);
}

/* free a font */
value font_free(value font)
{
  TTF_CloseFont(C_FONT(font)); 
  return Val_unit;
}

/* create tile with text from font */
value tile_text(value font, value text, value color)
{

  SDL_Surface *t;
  
  SDL_Color c = { (unsigned char)Int_val( Field(color, 0)),
		  (unsigned char)Int_val( Field(color, 1)), 
		  (unsigned char)Int_val( Field(color, 2)), 
		      0 };

  SDL_Color w={255,255,255,0};

    t = display_format(TTF_RenderText_Solid(C_FONT(font), &Byte(text,0), c));
  if(t==NULL)
    {
      printf("problem creating text\n");
    }
  return ML_TILE(t);
}



/* FX */
value fadein(value u)
{
  SDL_Surface *screen=SDL_GetVideoSurface();
  fade (screen,screen,1,0);
  return Val_unit;
}

value fadeout(value u)
{
  SDL_Surface *screen=SDL_GetVideoSurface();
  fade (screen,screen,1,1);
  return Val_unit;
}

/* other */
value map_load(value filename)
{
  CAMLparam0();
  CAMLlocal1(map);
  int c,x,y;
  int w,h;
  int k=0;
  SDL_Surface *m;
  m=IMG_Load(String_val(filename));
  w=m->w;
  h=m->h;  

  map=alloc(h*w, 0);
  for (y=0;y<h;y++)
    {
      for (x=0;x<w;x++)
	{
	  Uint8 *pixel;
	  pixel=m->pixels + y*m->pitch + x*m->format->BytesPerPixel;
	  Store_field(map, k, Val_int(*pixel));
	  k++;
	}
    }

  SDL_FreeSurface(m);
  CAMLreturn(map);
}
