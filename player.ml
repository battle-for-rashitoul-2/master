(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Bfrlib;;
open Version;;
open File;;
open Video;;
open Event;;
open Interface;;
open Object;;
open Iface_event;;

let iface=new interface "lecteur/lecteur.png" 278 150;;

let fnt=new font_object "medias/fonts/samos.ttf" 12;;

let player=iface_add_object iface (new iface_player fnt 278 150);;
player#move 20 20;
player#show();

let loop delay=
  clear_iface_ev_stack iface;
  iface#update();
  curs#put();
  flip();
  
 in
Callback.register "loop" (loop);
Callback.register "quit" (exit);
event_loop !ev_a 30
