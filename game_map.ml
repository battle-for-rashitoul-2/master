(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)

open Low;;
open Rect;;
open Config;;
open Video;;
open Object;;
open Music;;
open Interface;;
open File;;
open Network;;

open Game_main;;
open Game_interface;;
open Game_cursors;;
open Game_object;;
open Game_select;;
open Game_object_type;;
open Game_layer;;
open Game_tile_layer;;
open Game_object_layer;;
open Game_player_type;;
open Game_player_layer;;
open Game_ai;;
open Game_help;;
open Game_visual;;

(** Game map main class definition *)

(* FIXME : FILE TOO HEAVY *)

(* FIXME : these must be in game_map class *)
let motion_left=ref 0;;
let motion_right=ref 0;;
let motion_top=ref 0;;
let motion_bottom=ref 0;;

type game_m={
  w:int;
  h:int;
  resource_layer:game_object_lay;
  decor_layer:game_object_lay;
  player_layers:game_player_lay array 
};;

class game_map wi hi tw th vx vy players iam=
  object (self)
    initializer curs#set_player iam      
    val mutable rect=new rectangle 0 0 wi hi
    val mutable trect=new rectangle 0 0 tw th  
    val mutable vrect=new game_visual vx vy
    val mutable no_col_layer=new game_layer wi hi
    val mutable tile_layer=new game_tile_layer wi hi tw th   
    val mutable resource_layer=new game_object_layer wi hi 350
    val mutable decor_layer=new game_object_layer wi hi 350

    val mutable maptype=9
    val mutable num_players=(Array.length players)
    val mutable player_layers=     
      let a=Array.make ((Array.length players)+1) (new game_player_layer 0 (player_types#get_player_type "none") wi hi) in  
	for i=1 to (Array.length players) do
	  a.(i)<-(new game_player_layer i (player_types#get_player_type players.(i-1)) wi hi);
	  a.(i)#set_num i;
	  a.(i)#clear_objs()
	done;
	a
    val mutable iam_player=iam
    val mutable iam_string=players.(iam-1)			     
    val mutable iface=
      let ifa=(player_types#get_player_type players.(iam-1))#create_iface
      in
	
	(* create help *)
	if this_config.game_opt3=1 then create_help ifa players.(iam-1);

	(* create interface *)
	init_iface ifa players.(iam-1)
				      
    val mutable selection=new game_selection 50;	
    val mutable mul_selection=new game_multiple_selection;
    val mutable obj_types=new game_object_types;
    val mutable action_buttons=let a=Hashtbl.create 2 in Hashtbl.add a "none" 0;a
    val mutable face_objs=let a=Hashtbl.create 2 in Hashtbl.add a "none" (0,0);a
    val mutable icon_objs=let a=Hashtbl.create 2 in Hashtbl.add a "none" (0,0);a

    val mutable face_fnt=new font_object "medias/fonts/samos.ttf" 18

    val mutable move_requested=false
    val mutable attack_requested=false

    val mutable win_cond=(function()->(false))
    val mutable lose_cond=(function()->(false))

    val mutable ai=new game_ai

    val mutable spectator=false

    val mutable comb=new combo;
    val mutable game_type="none"


    method set_game_type t=game_type<-t

    method get_comb=comb

    method get_num_players=num_players

    method set_win_cond v=win_cond<-v
    method get_win_cond()=win_cond()

    method set_lose_cond v=lose_cond<-v
    method get_lose_cond()=lose_cond()

    method get_spectator=spectator
    method get_ai=ai

    

    method pay p dif=
      let d=ref dif in      
      let r=ref 0 in
      for k=1 to resource_layer#get_cur_obj do	  
	let ro=(resource_layer#get_object k) in	
	if !d>0 then (  
	  if self#get_camp p=ro#get_name then (
	    match ro#get_life with
	    | 1 -> (d:=!d-1;self#del_resource k)
	    | 2 -> (d:=!d-2;self#del_resource k)
	    | 3 -> (d:=!d-3;self#del_resource k)
	    | 4 -> (d:=!d-4;self#del_resource k)
	    | 5 -> (d:=!d-5;self#del_resource k)
	    | _ -> ();
	   )
	 )
      done;

      resource_layer#empty_del_stack();

    method get_camp p=
      match player_layers.(p)#get_name with
	| "rebel" -> "nature"
	| "neonaz" -> "pollution"
	| _ -> "none";

    method train_unit p b un=
      if (self#is_obj_type un)==true then (
	let co=self#get_unit p b in
	let ut=self#get_obj_t un in 
	let cost=ut#get_life_tot/100 in
	let res=player_layers.(p)#resource_get (self#get_camp p) in	
	let r=ref false in
	let x=co#get_rect#get_x and y=co#get_rect#get_y and ow=co#get_rect#get_w and oh=co#get_rect#get_h in
	if player_layers.(p)#get_train_now==false then
	  (
	   for h=(x - 1) to (x + ow) do
	     for j=(y - 1) to (y + oh) do
	       if self#is_unit h j==false then (
		 if self#out_of_map h j==false then (
		   if !r==false then (
		     if (res-cost)>0 then (
		       self#add_unit p h j (self#get_obj_type un);
		       player_layers.(p)#resource_set (self#get_camp p) (res-cost);
		       self#pay p cost;
		       r:=true;				      
		       player_layers.(p)#set_train_now true;
		      )
		    )		      
		  )
		)		
	     done;
	   done;
	  )


       )
	  
	  
    method plant_around_obj co res rx ry=
      let c=ref 0 in
      for i=(co#get_rect#get_x - rx) to (co#get_rect#get_x + co#get_rect#get_w+ rx - 1) do
	for j=(co#get_rect#get_y - ry) to (co#get_rect#get_y + co#get_rect#get_h+ ry - 1) do
	  if self#out_of_map i j==false then (
	    if (self#is_unit i j)==false && (self#get_resource_num i j)=0 then (
		let r=(randomize 4) in     
		if r=1 then 
		  (
		   self#add_resource i j (self#get_obj_type res);
		   c:= !c+1;
		  )
	     )
	   )
	done;
      done;
      !c

    method plant_around p b res rx ry=
      let co=self#get_unit p b in
      self#plant_around_obj co res rx ry


    method out_of_map x y=
      if x>0 && x<rect#get_w 
	  && y>0 && y<rect#get_h then 
	false
      else
	true 

    method can_construct_here_array p x y=
      let r=ref true in
      let o=self#get_construct p (player_layers.(p)#get_construct_requested) in
      let cant_array=Array.make_matrix (o#get_rect#get_w+1) (o#get_rect#get_h+1) true in
      for i=x to x+o#get_rect#get_w do
	for j=y to y+o#get_rect#get_h do
	  if self#out_of_map i j==false then (
	    if (self#is_unit i j)==true then (
	      cant_array.(i-x).(j-y)<-false;
	    );
	    if self#get_fow_position p i j = 0 then (
	      cant_array.(i-x).(j-y)<-false;
	    )
	  )
	done;
      done;	  	  
      cant_array

    method can_construct_here p x y=
      let r=ref true in
      let o=self#get_construct p (player_layers.(p)#get_construct_requested) in
      for i=x to x+o#get_rect#get_w do
	for j=y to y+o#get_rect#get_h do
	  if self#out_of_map i j==false then (
	    if (self#is_unit i j)==true then
 	      r:=false;
	    if self#get_fow_position p i j = 0 then
	      r:=false
	   )
	  else
	    r:=false;
	done;
      done;	  	  
      !r

    method is_construct p c=
      let r=ref false in
      for i=0 to (player_layers.(p)#get_c_obj_cons)-1 do
	if (fst (player_layers.(p)#get_obj_constructed i))=c then
	  (
	   r:=true;
	  );
      done;
      !r
    method get_construct p c=
      let r=ref none_obj in
      for i=0 to (player_layers.(p)#get_c_obj_cons)-1 do
	if (fst (player_layers.(p)#get_obj_constructed i))=c then
	  (
	   r:=(snd (player_layers.(p)#get_obj_constructed i));
	  );
      done;
      !r

    method construct p c obj=
      if (self#is_construct p c)==false then (
	player_layers.(p)#add_obj_constructed c obj
       );

    method del_construct p c=      
      for i=0 to (player_layers.(p)#get_c_obj_cons)-1 do
	if (fst (player_layers.(p)#get_obj_constructed i))=c then
	  (
	   player_layers.(p)#del_obj_constructed i;
	  );
      done;


    method tile_init t=
      maptype<-t;
      for i=0 to rect#get_w-1 do
	for j=0 to rect#get_h-1 do
	  let mt=randomize 2 in
	    tile_layer#set_position i j (t+mt); 
	done;
      done;
      (* random resources position *)
    method freestyle_resource p1 p2=

      let tot_case=rect#get_w*rect#get_h in
      let pour1=int_of_float(((float_of_int p1)/.100.0)*.(float_of_int tot_case))
      and pour2=int_of_float(((float_of_int p2)/.100.0)*.(float_of_int tot_case)) in
      for i=0 to rect#get_w-1 do
	for j=0 to rect#get_h-1 do
	  
	  let r1=randomize pour1 in
	  let r2=randomize pour2 in
	  if r1=pour1-1 then (
	    if self#is_unit i j==false then
		self#add_resource i j (self#get_obj_type "nature");
	   )
	  else
	    (
	     if r2=pour2-1 then (
	       if self#is_unit i j==false then
		 self#add_resource i j (self#get_obj_type "pollution");
	      )
	    )
	done;
      done;


    method network_init p=
	let x=ref 0 and y=ref 0 in
	  (* must be val proportional to map size *)
	let free_place=ref false in
	  while !free_place<>true do
	    let r=randomize 4 in	  
	      (
		match r with
		  |0 -> x:=10;y:=10 
		  |1 -> x:=(rect#get_w - 10);y:=10
		  |2 -> x:=(rect#get_w - 10);y:=(rect#get_h - 10)
		  |3 -> x:=10;y:=(rect#get_h - 10)
		  |_->()
	      );
	      if (self#is_unit !x !y)==false then free_place:=true;
	  done;
  

	  (
	    match player_layers.(p)#get_name with		
	      | "rebel" -> self#add_unit p !x !y (self#get_obj_type "zork")
	      | "neonaz" -> self#add_unit p !x !y (self#get_obj_type "brad_lab")
	      | _ -> ()
	  );


	    vrect#set_position ( abs((!x)*32-vrect#get_w/2 + 10)) ( abs((!y)*32-vrect#get_h/2 + 10)); 
	    if vrect#get_x>(rect#get_w*32 - vrect#get_w - 1) then 
	      vrect#set_position (rect#get_w*32 - vrect#get_w - 1) (vrect#get_y);
	    if vrect#get_y>(rect#get_h*32 - vrect#get_h - 1) then 
	      vrect#set_position (vrect#get_x) (rect#get_h*32 - vrect#get_h - 1);


    method freestyle_init()=
      for i=1 to num_players do
	let x=ref 0 and y=ref 0 in
	  (* must be val proportional to map size *)

	let free_place=ref false in
	  while !free_place<>true do
	    let r=randomize 4 in	  
	      (
		match r with
		  |0 -> x:=10;y:=10 
		  |1 -> x:=(rect#get_w - 10);y:=10
		  |2 -> x:=(rect#get_w - 10);y:=(rect#get_h - 10)
		  |3 -> x:=10;y:=(rect#get_h - 10)
		  |_->()
	      );
	      if (self#is_unit !x !y)==false then free_place:=true;
	  done;
  

	  (
	    match player_layers.(i)#get_name with		
	      | "rebel" -> self#add_unit i !x !y (self#get_obj_type "zork")
	      | "neonaz" -> self#add_unit i !x !y (self#get_obj_type "brad_lab")
	      | _ -> ()
	  );


	  if i=iam_player then (
	    vrect#set_position ( abs((!x)*32-vrect#get_w/2 + 10)) ( abs((!y)*32-vrect#get_h/2 + 10)); 
	    if vrect#get_x>(rect#get_w*32 - vrect#get_w - 1) then 
	      vrect#set_position (rect#get_w*32 - vrect#get_w - 1) (vrect#get_y);
	    if vrect#get_y>(rect#get_h*32 - vrect#get_h - 1) then 
	      vrect#set_position (vrect#get_x) (rect#get_h*32 - vrect#get_h - 1);
	  )

      done;

      


    method init_freestyle_OLD perc1 perc2 t=
      maptype<-t;
      let r1=randomize 4 in
      let r2=randomize 4 in
      let init_p p x y=
	let pu=if p=1 then "zork" else "brad_lab" in      
	self#add_unit p x y (self#get_obj_type pu);
	(* AUTO POSITION *)
	if p=iam_player then
	  vrect#set_position ( abs((x)*32-vrect#get_w/2 + 10)) ( abs((y)*32-vrect#get_h/2 + 10)); 
	if vrect#get_x>(rect#get_w*32 - vrect#get_w - 1) then 
	  vrect#set_position (rect#get_w*32 - vrect#get_w - 1) (vrect#get_y);
	if vrect#get_y>(rect#get_h*32 - vrect#get_h - 1) then 
	  vrect#set_position (vrect#get_x) (rect#get_h*32 - vrect#get_h - 1);
	
      in
      let init_pos p r=
	match r with
	|0 -> init_p p 10 10 
	|1 -> init_p p (rect#get_w - 10) 10
	|2 -> init_p p (rect#get_w - 10) (rect#get_h - 10)
	|3 -> init_p p 10 (rect#get_h - 10)
	|_->() in
	
	init_pos 1 r1;
	if r2<>r1 then
	  init_pos 2 r2
	else
	  (
	    if r1>0 then init_pos 2 (r2 - 1);
	    if r1<4 then init_pos 2 (r2 + 1);
	  );
	
	self#freestyle_resource perc1 perc2 

   

    method load_map w h filename=
      let a=map_load filename in
      print_newline();
      let k=ref 0 in
      print_string "LOADING MAP";print_newline();
      print_string "w :";print_int w;print_newline();
      print_string "h :";print_int h;print_newline();
      for y=0 to h*4 - 1 do
	for x=0 to w - 1 do
	  if y<h && x<w then (	    
	    tile_layer#set_position (x) (y) (
	    match (a.(!k)) with
	    | 72 -> 9
	    | 128 -> 6
	    | 89 -> 0
	    | _ -> 0;
	   );	  
	   );

	  if y>=h && y<2*h && x<w then (
	    match (a.(!k)) with
	    | 114 ->  self#add_resource (x) (y-h) (self#get_obj_type "nature1")
	    | 113 -> self#add_resource (x) (y-h) (self#get_obj_type "nature1") 
	    | _ -> ();
	   );


	  if y>=3*h && y<4*h && x<w then (
	    match (a.(!k)) with
	    | 54 -> self#add_unit 1 (x) (y-(3*h)) (self#get_obj_type "zork")
	    | 149 -> self#add_unit 2 (x) (y-(3*h)) (self#get_obj_type "gymnaz")
	    | 148 -> self#add_unit 2 (x) (y-(3*h)) (self#get_obj_type "caserne")
	    | 147 -> self#add_unit 2 (x) (y-(3*h)) (self#get_obj_type "labo")

	    | 216 -> self#add_unit 2 (x) (y-(3*h))  (self#get_obj_type "patrouilleur")
	    | _ -> ()
	   );		  

	  k:= !k+1;
	done;
      done;


      print_string "MAP LOADED";print_newline();
      print_newline();

    method is_move_requested=move_requested
    method set_move_requested v=move_requested<-v

      (* actions buttons funcs *)
    method add_action_button nm file= 
      iface#add_object (new iface_button file 48 48);
      let n=iface#get_cur_obj - 1 in
      Hashtbl.add action_buttons nm n
	
    method get_action_button nm=
      iface#get_object_num (Hashtbl.find action_buttons nm)

    method create_capacity_array can_move can_attack can_build_u can_build_b=
      let a=Hashtbl.create 2 in Hashtbl.add a "none" false;
      Hashtbl.add a "move" can_move;
      Hashtbl.add a "attack" can_attack;
      Hashtbl.add a "build_u" can_build_u;
      Hashtbl.add a "build_b" can_build_b;
      a

    method comp_capacity_array a1 a2=

      let get_v a v=(Hashtbl.find a v) in 
      let na=Hashtbl.create 2 in Hashtbl.add na "none" false;
      Hashtbl.add na "move" ((get_v a1 "move")&&(get_v a2 "move"));
      Hashtbl.add na "attack" ((get_v a1 "attack")&&(get_v a2 "attack"));
      Hashtbl.add na "build_u" ((get_v a1 "build_u")&&(get_v a2 "build_u"));
      Hashtbl.add na "build_b" ((get_v a1 "build_b")&&(get_v a2 "build_b"));
      na

    method create_capacity_array_from_obj t=
      let obj=obj_types#get_object t in
      let a=Hashtbl.create 2 in Hashtbl.add a "none" false;
      Hashtbl.add a "move" (if obj#get_velocity>0 then true else false);
      Hashtbl.add a "attack" (if obj#get_attack1>0 then true else false);
      Hashtbl.add a "build_u" false;
      Hashtbl.add a "build_b" false;
      a


    method create_action_buttons ()=

      (* add buttons *)
      (
	match iam_string with
	  | "rebel" -> 
	      self#add_action_button "move" "medias/interface/buttons/rebel_move_button.png";
	      self#add_action_button "attack" "medias/interface/buttons/rebel_attack_button.png";
	      print_string "add rebel buttons";print_newline();
	  | "neonaz" -> 
	      self#add_action_button "move" "medias/interface/buttons/naz_move_button.png";
	      self#add_action_button "attack" "medias/interface/buttons/naz_attack_button.png";
	      print_string "add neonaz buttons";print_newline();
	  | _ -> ();
      );   
    let mbut=self#get_action_button "move" and
      abut=self#get_action_button "attack" in
      mbut#set_click (function()->(move_requested<-true));
      abut#set_click (function()->(attack_requested<-true));	      
      
      (* move buttons *)
      (
	match iam_string with
	  | "rebel" -> 
	      mbut#move (f_size_w 566) (f_size_h 477);
	      abut#move (f_size_w 614) (f_size_h 477);
	      print_string "move rebel buttons";print_newline();
	  | "neonaz" -> 
	      mbut#move (f_size_w 635) (f_size_h 440);
	      abut#move (f_size_w 683) (f_size_h 440);	    
	      print_string "move neonaz buttons";print_newline();
	  | _ -> ();
      )	    

    method add_obj_type nm rep icon obj=
      print_string ("ADDING OBJECT TYPE "^nm);print_newline();
      (* buttons *)
      obj_types#add_object_type nm obj;

      if (Hashtbl.mem face_objs (nm))==false then (
	if rep<>"none" then (
	  iface#add_object_n (nm^":face") (new iface_face_object rep 85 85 face_fnt);
	  let n=iface#get_cur_obj - 1 in
	  Hashtbl.add face_objs nm (0,n);
	 );
	);

      if (iface#is_object (nm^":icon"))==false then (
	if icon<>"none" then (
         iface#add_object_n (nm^":icon") (new iface_button_construct icon 48 48 );
	  let obj=(iface#get_object_char (nm^":icon")) in
	  obj#set_mouseover (function()->(
	    if (iface#is_object (nm^":help")) then (
	      let hobj=(iface#get_object_char (nm^":help")) in
	      hobj#show();
	     )
	   ));
	  obj#set_mouseout (function()->(
	    if (iface#is_object (nm^":help")) then (
	      let hobj=(iface#get_object_char (nm^":help")) in
	      hobj#hide();
	     )
	   ));
	 )
       );

    method get_obj_type nm=obj_types#get_object_type nm
    method get_obj_t nm=obj_types#get_object nm
    method is_obj_type nm=obj_types#is_object_type nm

    method get_iface=iface

    method get_selection=selection
    method get_mul_selection=mul_selection

    method get_tile_layer=tile_layer
    method get_player_layer player=player_layers.(player)

    method get_iam=iam_player
    method get_iam_string=iam_string

    method get_rect=rect
    method get_vrect=vrect


    (* FOW operations *)
    method get_fow_position p x y=
      (player_layers.(p)#get_fow_position x y);	    


    (* unit see around him *) 

    method update_sight player num=
      let play=(player_layers.(player)) in
      let obj=play#get_obj num in
      let x=obj#get_rect#get_x and y=obj#get_rect#get_y in
	for i=(x - obj#get_sight - 1) 
	to (x + obj#get_sight + obj#get_rect#get_w) do
	  for j=(y - obj#get_sight - 1) 
	  to (y + obj#get_sight + obj#get_rect#get_h) do
	    if self#out_of_map i j==false then (	    
	      if (self#get_fow_position player i j )<> 2 then ( 
		play#set_fow_position i j 2;
		
		(* if resource update pos arround it *)
		if(self#get_resource_num i j <> 0) then (
		  let curobj=self#get_resource_by_position i j in
		    for k=curobj#get_rect#get_x to (curobj#get_rect#get_x + curobj#get_rect#get_w - 1) do
		      for l=curobj#get_rect#get_y to (curobj#get_rect#get_y + curobj#get_rect#get_h - 1) do
			if (self#out_of_map k l)==false then
			  play#set_fow_position k l 2;		      
		      done;
		    done;
		);
		
		(* if decor update pos arround it *)
		if(self#get_decor_num i j <> 0) then (
		  let curobj=self#get_decor_by_position i j in
		    for k=curobj#get_rect#get_x to (curobj#get_rect#get_x + curobj#get_rect#get_w - 1) do
		      for l=curobj#get_rect#get_y to (curobj#get_rect#get_y + curobj#get_rect#get_h - 1) do
			if (self#out_of_map k l)==false then
			play#set_fow_position k l 2;
		      done;
		    done;
		);
	      );
	      
	      (* if unit *)
	      if (self#is_unit i j)==true then (
		let p=self#get_unit_player i j in
		let curobj=self#get_unit_by_position p i j in
		  (* FIXME : players *)
		  (* An ennemy!*)
		  if (player_layers.(p)#get_name<>player_layers.(player)#get_name) then (		
		    (* mini-AI : auto-attacking *)	 	    		
		    if obj#get_state="idle" then (
		      self#unit_move_for_attack player (self#get_unit_num player x y)  i j; 
		    );
		    
		    (* update pos arround unit *)
		    for k=curobj#get_rect#get_x to (curobj#get_rect#get_x + curobj#get_rect#get_w - 1) do
		      for l=curobj#get_rect#get_y to (curobj#get_rect#get_y + curobj#get_rect#get_h - 1) do
			if (self#out_of_map k l)==false then
			play#set_fow_position k l 2;
		      done;
		    done;
		  )		  
	      );
	      
	    (* FIXME : problem here *)
	    (* if moving for attack, attack when attacked unit viewed *)
	    if obj#get_state="move_for_attack" then 
	      if (self#is_unit i j)=true then (
		let p=self#get_unit_player i j in
		if (player_layers.(p)#get_name<>player_layers.(player)#get_name) && (obj#get_aunit=0 || obj#get_aunit=(self#get_unit_num p i j)) then (
		  if obj#get_attack2 > 0 then (
		  if abs(x - i)<=obj#get_attack2_dist && abs(y - j)<=obj#get_attack2_dist then (
		     obj#act_stop();
		     obj#set_state "idle";
		     self#unit_attack player x y p i j;
		    )
		   )
		  else (
		    if ((x - i)==1 || (x - i)==0 || (x - i)== -1) 
			&& ((y - j)==1 || (y - j)==0 || (y - j)== -1) then (
		      obj#act_stop();
		      obj#set_state "idle";
		      self#unit_attack player x y p i j;
		     )
		   )
		 )
	       )

	   );
	done;
      done;


    method foreach_player f=
      Array.iteri f player_layers

    method foreach_other_player f=
	Array.iteri(fun i v->(
		      if i<>iam_player then
			f i
		    ))
	  player_layers

    method foreach_ennemy_player f p ennemy_string=
      Array.iteri (fun i v->(
	if i<>p then
	  if v#get_name=ennemy_string then
	    f i))
	player_layers;


    (* traverse full layer objects *)
    method private foreach_object layer d=
      layer#foreach_object d; 


    method private foreach_map_entry_all d=      
      for i=0 to rect#get_w-1 do
	for j=0 to rect#get_h-1 do
	  d i j;
	done;
      done;
      
    method private foreach_map_entry_player player=
      for i=0 to rect#get_w-1 do
	for j=0 to rect#get_h-1 do
	  for p=1 to num_players do	  
	    player i j p;
	  done;  
	done;
      done;


	    
    method update_server()=

      ai#check_rules(); 


      (* clean all layers entries *)

      (* resources & decors *)
      self#foreach_map_entry_all
	(fun i j->(
	  resource_layer#set_position i j 0; 
	  decor_layer#set_position i j 0; 
	 ));

      (* players units *)
      self#foreach_map_entry_player 
	(fun i j p->(
	   player_layers.(p)#clean_position i j; 
	 ));
      
      (* decors update *)
      self#foreach_object decor_layer 
	(fun k dob->(
	   decor_layer#update_obj k; 	  
	 ));

      (* resources update *)
      self#foreach_object resource_layer 
	(fun k rob->(
	   resource_layer#update_obj k; 
	 ));


      (* players units update *)
      for p=1 to num_players do
	self#foreach_object player_layers.(p)#get_obj_layer
	  (fun k v->(
	     player_layers.(p)#update_obj k; 
	   ))
      done;     
      

      (* resource grow *)
      self#foreach_object resource_layer 
	(fun k oc->(
	   if oc#get_life <6 then (
	     if oc#get_c_cons_s>=oc#get_cons_speed then (
	       if oc#get_life = 5 then (
		 let c=
		   self#plant_around_obj oc oc#get_name 1 1;
		 in ()
	       );
	       oc#set_life (oc#get_life + 1);
	       oc#set_c_cons_s 0;	      
	     )
	     else
	       oc#set_c_cons_s (oc#get_c_cons_s + 1);	    
	   )
	   else oc#set_life 3;
	 ));
      

      (* players resources reinit*)
      self#foreach_player (fun i v->(
			let res=
			  match v#get_name with
			    | "rebel" -> "nature"
			    | "neonaz" -> "pollution"
			    | _ -> "none" in		
			  v#resource_set res 0;
		      ));




      (* players resources count *)
      self#foreach_object resource_layer 
	(fun k ro->(
	   match ro#get_name with 
	     |"nature" -> 
		for i=1 to num_players do
		  if player_layers.(i)#get_name="rebel" then
		    (player_layers.(i)#resource_set "nature" (player_layers.(i)#resource_get "nature" + ro#get_life))
		done
	     |"pollution" -> 
		for i=1 to num_players do
		  if player_layers.(i)#get_name="neonaz" then
		    (player_layers.(i)#resource_set "pollution" (player_layers.(i)#resource_get "pollution" + ro#get_life))
		done
	     | _ -> ();
	 ));	  

      
      

    method update_client p=
      let p_lay=player_layers.(p) in
	
	p_lay#set_train_now false;
	
	(* players units *)
	self#foreach_map_entry_all
	  (fun i j->(	  
	     p_lay#clean_position i j; 
	   ));

	p_lay#foreach_object 
	  (fun k v->(
	     p_lay#update_obj k; 
	   ));
	
	(* update fow *)

	self#foreach_map_entry_all
	  (fun i j->(	  
	     if self#get_fow_position p i j=2 then	(
	       (* FIXME : fix this for multiplayer *)
	       if p=iam_player then tile_layer#set_shaded i j false; 
	       
	       p_lay#set_fow_position i j 1; 
	     );	   
	 )); 

	(* update sight for all units - set fow to 2 *)
	p_lay#foreach_object 
	  (fun k v -> (
	     self#update_sight p k;    	  
	   ));
      
	
	(* units polit test *)
	p_lay#foreach_object
	  (fun k v-> (
	     self#unit_be_polit p k ;
	   ));
	
	(* construction grow *)
	p_lay#foreach_object_constructed
	  (fun k oc ->(
	     if oc#get_c_cons_s>=oc#get_cons_speed then
	       (
		 if oc#get_life<=oc#get_life_tot then (
		   p_lay#get_when_constructing();
		   oc#set_life (oc#get_life + 10)
		 )
		 else 
		   (
		     p_lay#get_when_constructed();
		 );
		 oc#set_c_cons_s 0;	      
	       )
	     else
	       oc#set_c_cons_s (oc#get_c_cons_s + 1);
	   ));
	
	
	(* construction build *)
	if p_lay#get_construct_now==true then
	  (
	    let o=self#get_construct p (p_lay#get_construct_requested) in
	      if o#get_name<>"none" then ( 
		if self#can_construct_here p o#get_rect#get_x o#get_rect#get_y==true then ( 
		  if o#get_rect#get_x<>0 && o#get_rect#get_y<>1 then (  
		    self#add_unit p (o#get_rect#get_x) (o#get_rect#get_y) o; 
		    self#del_construct p p_lay#get_construct_requested; 
		    
		    p_lay#set_construct_requested 0;
		    p_lay#set_construct_now false;
		  )
		)
	      )
	  );
	
	p_lay#foreach_object
	  (fun k obj -> (	    
	     (* interface earthquake *)
	     if obj#get_spe_mode<45 && obj#get_spe_mode>30 then
	       (
		 let rx=(randomize 16) - 8 and ry=(randomize 16) - 8 in
		   iface#move_all rx ry;
	       );
	     
	     (* end of spe anim : kill unit *)
	     if obj#get_spe_mode=45 then (
	       if p=iam then (
		 selection#del_all_selected();
	       );
	       self#del_unit p k;
	     );
	     
	     obj#act vrect#get_x vrect#get_y;
	     obj#anim();
	     
	     if obj#get_spe_mode=0 then 
	       (	       
		 (* dead anim finished : kill unit*)
		 if obj#will_i_dead==true then  (
		   self#del_unit p k;
		 )
		 else (	      
		   (* dead anim not finished *)
		   if obj#get_life < 0 then (		    
		     obj#set_state "dead";
		     (* COMBO *)
		     if obj#get_spe >= 2 then (
		       obj#set_spe_mode 1;
		     );		 
		   );
		   if obj#get_state="dead" && obj#get_current_state#get_current=obj#get_current_state#get_num_frames - 1 then (
		     obj#i_will_dead;
		   );	    	      		  
		 );
		 obj#set_spe 0; 
	       )
	   ));



    method update_iface()=
      (* update iam_player resourcebar *)
      let obj=(iface#get_object_char "resourcebar") in
      obj#set_data_text "resource";
	(      
	match iam_string with
	  | "rebel" -> obj#set_data (player_layers.(1)#resource_get "nature");
	  | "neonaz" -> obj#set_data (player_layers.(2)#resource_get "pollution");
	  | _ -> ();      
	);
      obj#set_data1 (1000);

      (* update iam_player boucheriebar *)
      comb#update();

      let obj=(iface#get_object_char "resourcebar") in
      obj#set_data_text "boucherie";
      let old_b=obj#get_data in
      obj#set_data (player_layers.(iam_player)#resource_get "boucherie");
      if old_b<obj#get_data then (
	let r=(obj#get_data - old_b) in
	print_string "BOUCHERIE : ";print_int r;print_newline();
	comb#action r;
       );
      obj#set_data1 (100);

      
      (* FIXME : to many things here *)
      (* selected unit operations *)      
      (* clear face entries *)
      let f k v=
	let get_ext s=
	  if (String.contains s ':')==true then (
	    let c=String.index s ':' in
	    let ns=String.sub s (c) ((String.length s)- c) in
	    ns 
	   )
	  else
	    "none"
	in
	if (get_ext k)=":icon" then
	  (	
	   let o=iface#get_object_char k in
	   o#hide();
	  )

      in Hashtbl.iter f (iface#get_object_hash);

      let pobj=iface#get_object_char "player" in
      pobj#hide();

      let cb=iface#get_object_char "constructbar" in
      cb#hide();
      let cb=iface#get_object_char "construct_label" in
      cb#hide();
	let cancel=iface#get_object_char "cancelbutton" in
	cancel#hide();      

      (* init action from selection *)
      if selection#get_nb_selected=0 then
	(
	 let cons=Hashtbl.find ((player_layers.(iam_player))#get_tech_tree) "none" in
	 let cancel=(iface#get_object_char "cancelbutton") in
	 if (iface#is_object ((snd cons.(0))^":icon")) then (
	   let o=iface#get_object_char ((snd cons.(0))^":icon") in
	   if (self#is_construct iam_player (-1))==true then
	     (
	      let cb=iface#get_object_char "constructbar" in
	      let cl=iface#get_object_char "construct_label" in
	      let co=self#get_construct iam_player (-1) in
	      if co#get_life>=co#get_life_tot then (
		cb#set_data (co#get_life);
		cb#set_data1 (co#get_life_tot);
		cb#show();
		o#set_release (function()->(		  
		  player_layers.(iam_player)#request_construct (-1);
		 ));

		cancel#show();
		(		  
		 match iam_string with
		 | "rebel" -> o#move (f_size_w 500) (f_size_h 510)
		 | "neonaz" -> o#move (f_size_w 530) (f_size_h 480)
		 | _ -> ();
		);
		o#set_data 2;
		o#show();

	       )
	      else (
		cb#set_data (co#get_life);
		cb#set_data1 (co#get_life_tot);
		cb#show();
		cl#show();
		o#set_release (function()->());
		cancel#set_release (function()->(		  
		  player_layers.(iam_player)#set_construct_requested 0;
		  self#del_construct iam_player (-1);
		 ));
		cancel#show();
		
		(
		 match iam_string with
		 | "rebel" -> o#move (f_size_w 500) (f_size_h 510)
		 | "neonaz" -> o#move (f_size_w 530) (f_size_h 480)
		 | _ -> ();
		);
		o#set_data 1;
		o#show();
	       )
	     )else(
	     o#set_data 0;

	     (
	      match iam_string with
	      | "rebel" -> o#move (f_size_w 566) (f_size_h 477)
	      | "neonaz" -> o#move (f_size_w 635) (f_size_h 440)
	      | _ -> ();
	     );
	     o#show();

	     cancel#set_release (function()->(		  
	       player_layers.(iam_player)#set_construct_requested 0;
	      ));
	     cancel#show();

	     o#set_release (function()->(	     
	       (
		match iam_string with
		| "rebel" -> self#construct iam_player (-1) (self#get_obj_type "taverne")
		| "neonaz" -> self#construct iam_player (-1) (self#get_obj_type "gymnaz")
		| _ -> ();
	       );
	      ))
	    );


	  )   
	);


      if selection#get_nb_selected=1 then
	(
	 let sel=(selection#get_selected 0) in
	 let u=self#get_unit iam_player sel in
	 let un=u#get_name in	 
	 if un="festival" || un="caserne" then (
	   let pobj=(iface#get_object_char "player") in
	   pobj#show();
	  );
	 
	   
	   
	 if (Hashtbl.mem ((player_layers.(iam_player))#get_tech_tree) un)==true then (
	   let cons=Hashtbl.find ((player_layers.(iam_player))#get_tech_tree) un in	   
	   let cancel=(iface#get_object_char "cancelbutton") in
	   let nb=ref 0 in
	   let nu=ref 0 in
	   for i=0 to (Array.length cons)-1 do
	     (* build building *)
	     if (fst cons.(i))=0 then (
	       if (iface#is_object ((snd cons.(i))^":icon")) then (
		 let o=iface#get_object_char ((snd cons.(i))^":icon") in		 
		 if (self#is_construct iam_player sel)==true then
		   (
		    let cb=iface#get_object_char "constructbar" in
		    let cl=iface#get_object_char "construct_label" in
		    let co=self#get_construct iam_player sel in

		    let pobj=(iface#get_object_char "player") in
		    pobj#hide();
		    
		    if co#get_name=(snd cons.(i)) then (		     
		      if co#get_life>=co#get_life_tot then (
			cb#set_data (co#get_life);
			cb#set_data1 (co#get_life_tot);
			cb#show();
			o#set_release (function()->(
			  (* here function to build on map *)
			  player_layers.(iam_player)#request_construct sel;
			 )); 
			(
			 match iam_string with
			 | "rebel" -> o#move (f_size_w 500) (f_size_h 510)
			 | "neonaz" -> o#move (f_size_w 530) (f_size_h 480)
			 | _ -> ();
			);
			o#set_data 2;
			o#show();
		       ) else (
			cb#set_data (co#get_life);
			cb#set_data1 (co#get_life_tot);
			cb#show();
			cl#show();
			o#set_release (function()->());
			(
			 match iam_string with
			 | "rebel" -> o#move (f_size_w 500) (f_size_h 510)
			 | "neonaz" -> o#move (f_size_w 530) (f_size_h 480)
			 | _ -> ();
			);
			o#set_data 1;
			o#show();
			cancel#set_release (function()->(		  
			  player_layers.(iam_player)#set_construct_requested 0;
			  self#del_construct iam_player sel;
			 ));
			cancel#show();

		       )
		     )
		   )else(
		   o#set_data 0;
		   (
		    match iam_string with
		    | "rebel" -> o#move (f_size_w (566) + f_size_w ((i - !nu)*48)) (f_size_h 477)
		    | "neonaz" -> o#move (f_size_w (635) + f_size_w ((i - !nu)*48)) (f_size_h 440)
		    | _ -> ();
		   );
		   o#show();

		   cancel#set_release (function()->(		  
		     player_layers.(iam_player)#set_construct_requested 0;
		     self#del_construct iam_player sel;
		    ));
		   cancel#show();

		   o#set_release (function()->(
		     self#construct iam_player sel (self#get_obj_type (snd cons.(i)));		 
		    ));
		  );
		 nb:= !nb+1;
		);
	      );   

	     (* grow resource around building *)
	     if (fst cons.(i))=1 then 
	       (
		let c=ref 0 in
		let o=iface#get_object_char ((snd cons.(i))^":icon") in		 
		let co=self#get_unit iam_player sel in
		o#set_data 0;

		(
		 match iam_string with
		 | "rebel" -> o#move (f_size_w (566)+ f_size_w (i*48)) (f_size_h 477)
		 | "neonaz" -> o#move (f_size_w (635)+ f_size_w (i*48)) (f_size_h 440)
		 | _ -> ();
		);

		o#show();
		
		
		o#set_release (function()->(	     
		  let c=ref 0 in
		(
		 match iam_string with
		 | "rebel" -> c:=self#plant_around iam_player sel "nature" 1 1
		 | "neonaz" -> c:=self#plant_around iam_player sel "pollution" 1 1;
		 | _ -> ();
		);
		  self#pay iam_player ((!c)/2);
		 ))
	       );
	     
	     (* train unit *)
	     if (fst cons.(i))=2 then 
	       (
		
		if (iface#is_object ((snd cons.(i))^":icon"))==true then
		  (
		   let o=iface#get_object_char ((snd cons.(i))^":icon") in		 
		   let co=self#get_unit iam_player sel in
		   o#set_data 0;
		   (
		    match iam_string with
		    | "rebel" -> o#move (f_size_w 566+ f_size_w ((i - !nb)*48)) (f_size_h (477+48))
		    | "neonaz" -> o#move (f_size_w 635+ f_size_w ((i - !nb)*48)) (f_size_h (440+48));
		    | _ -> ();
		   );
			o#show();
		   
		   
		   o#set_release (function()->(	     
		     self#train_unit iam_player sel (snd cons.(i));
		    ));
		   nu:= !nu+1;
		  )
	       )
	   done;
	  )
	);

      (* clear actions *)
      let f k v=
	if (Hashtbl.mem face_objs k)==true then (
	  Hashtbl.replace face_objs k (0,(snd v));
	  let o=Hashtbl.find face_objs k in
	  let rep_obj=(iface#get_object_num (snd o)) in
	  rep_obj#hide();
	 );
	(iface#get_object_char "iface_pv")#hide();
	(iface#get_object_char "iface_ac")#hide();
	(iface#get_object_char "iface_dc")#hide();
	(iface#get_object_char "iface_ae")#hide();
	(iface#get_object_char "iface_de")#hide();

	(iface#get_object_char "iface_pv_val")#hide();
	(iface#get_object_char "iface_ac_val")#hide();
	(iface#get_object_char "iface_dc_val")#hide();
	(iface#get_object_char "iface_ae_val")#hide();
	(iface#get_object_char "iface_de_val")#hide();

	let but=self#get_action_button "move" in
	but#hide();
	let but=self#get_action_button "attack" in
	but#hide();





      in Hashtbl.iter f face_objs;

                              
      (* init face entries from selection *)
      for i=0 to selection#get_nb_selected do
	if (selection#get_selected i)<>0 then
	  (
	   let obj=self#get_unit (iam_player) (selection#get_selected i) in
	   if (Hashtbl.mem face_objs obj#get_name)==true then (
	     let o=Hashtbl.find face_objs obj#get_name in
	     Hashtbl.replace face_objs obj#get_name ((fst o)+1,(snd o));
	   )
	  )
      done;

      (* set face objects *)
      let ccap=ref (self#create_capacity_array true true true true) in
      let n=ref 0 in
      let un=ref "none" in

      let f k v=
	if k<>"none" then (
	  if (fst v)>0 then (
	    let rep_obj=(iface#get_object_num (snd v)) in
	    rep_obj#set_data (fst v);
	    if iface#get_moving==false then
	      (
	      match iam_string with
		| "rebel" -> rep_obj#move (f_size_w (268+(!n*32))) (f_size_h 452)
		| "neonaz" -> rep_obj#move (f_size_w (341+(!n*32))) (f_size_h 451)
		| _ -> ();
	      );
	    rep_obj#show();
	    ccap:=self#comp_capacity_array (self#create_capacity_array_from_obj k) (!ccap); 
	    un:=k;	    

	    n:=!n+1;
	   );

	 )
      in Hashtbl.iter f face_objs;

      (* infos part *)
      if !n>0 && !n<2 then (
	let obj_t=obj_types#get_object (!un) in
	(iface#get_object_char "iface_pv")#show();
	(iface#get_object_char "iface_pv_val")#set_data_text (string_of_int((obj_t#get_life_tot - 100)/100));
	(iface#get_object_char "iface_pv_val")#show();

	(iface#get_object_char "iface_ac")#show();
	(iface#get_object_char "iface_ac_val")#set_data_text (string_of_int((obj_t#get_attack1)/10));
	(iface#get_object_char "iface_ac_val")#show();


	(iface#get_object_char "iface_dc")#show();
	(iface#get_object_char "iface_dc_val")#set_data_text (string_of_int((100 - obj_t#get_defend1)/10));
	(iface#get_object_char "iface_dc_val")#show();

	(iface#get_object_char "iface_ae")#show();
	(iface#get_object_char "iface_ae_val")#set_data_text (string_of_int((obj_t#get_attack2)/10));
	(iface#get_object_char "iface_ae_val")#show();

	(iface#get_object_char "iface_de")#show();
	(iface#get_object_char "iface_de_val")#set_data_text (string_of_int((100 - obj_t#get_defend2)/10));
	(iface#get_object_char "iface_de_val")#show();

       );

      (* button part *)
      if !n=0 then (
      ccap:=(self#create_capacity_array false false false false);
       );
      let get_v a v=(Hashtbl.find a v) in 
      if (get_v (!ccap) "move")==true then (
	let but=self#get_action_button "move" in
	but#show();
       );
      if (get_v (!ccap) "attack")==true then (
	let but=self#get_action_button "attack" in
	but#show();
       );



    (* sync the whole map *)
    method sync_map_from_client ic=
      let a=Marshal.from_channel ic in
      player_layers.(a.num)#from_value_s a obj_types

    method sync_map_to_client oc =
	Marshal.to_channel oc (self#to_value) [];

    method sync_map_from_server ic=
	let a=Marshal.from_channel ic in
	  self#from_value a;
    method sync_map_to_server oc=
	Marshal.to_channel oc 
	  (player_layers.(iam_player)#to_value
	     (vrect#get_x/32) 
	     (vrect#get_y/32) 
	     (vrect#get_w/32 - vrect#get_x/32) 
	     (vrect#get_h/32 - vrect#get_y/32))
	  [];
	
    (* update map (without screen operation) *)
    method update_freestyle()=
	self#update_server();
      for i=1 to num_players do
	self#update_client i;
      done;
	self#update_iface();            
      
    method sync_network chans=
	let ic,oc=chans in
	self#sync_map_to_server oc; 
	flush oc; 
	self#sync_map_from_server ic;

    method update_main()=
 	self#update_client iam_player;
	self#update_iface();            



   (* update screen itself *)
    method update_graphic()=

      let vw= !scr_w/32 and vh=(f_size_h 540)/32 in
      let motion_range=16 in
      if !motion_left=1 && vrect#get_x>0 then 
	(
	  vrect#scroll (-motion_range) 0;
	);
      if !motion_right=1 && vrect#get_x<((rect#get_w - (vw + 1))* !tile_w) then 
	(
	  vrect#scroll motion_range 0;
	);
      if !motion_top=1 && vrect#get_y>0 then 
	(
	  vrect#scroll 0 (-motion_range);
	);
      if !motion_bottom=1 && vrect#get_y<((rect#get_h - (vh + 2 ))* !tile_h) then 
	(
	  vrect#scroll 0 motion_range;
	);

      iface#rewind_all();


      (* update objs on vrect screen *)

      let vx=(f_size_w 0) and vy=(f_size_h 0) in
      let vw= !scr_w and vh=(f_size_h 540) in

      let vrect_x=if (vrect#get_x)/32 >= 0 then (vrect#get_x)/32 else 0
          and 
          vrect_y=if (vrect#get_y)/32 >=0 then (vrect#get_y)/32 else 0 
          and
          vrect_dx=if (vw + vrect#get_x)/32 < rect#get_w then (vw + vrect#get_x)/32 else rect#get_w-1
          and 
          vrect_dy=if (vh + vrect#get_y)/32 < rect#get_h then (vh + vrect#get_y)/32 else rect#get_h-1
          in

      video_set_clip vx vy vw vh; 

      (* decors init put *)
      self#foreach_object decor_layer 
	(fun k dob->(
	   dob#init_put();
	 ));

      (* resources init put *)
      self#foreach_object resource_layer 
	(fun k rob->(
	   rob#init_put(); 
	 ));

      (* players init put *)
      for p=1 to num_players do
	player_layers.(p)#foreach_object 
	  (fun k o->(
	     o#init_put();
	   ));
      done;


      (* update tile around buildings *)
      for p=1 to num_players do      
	self#foreach_object  player_layers.(p)#get_obj_layer	  
	  (fun k obj->(
	     if obj#get_rect#get_w>1 && obj#get_rect#get_h>1 then (
	       for i=obj#get_rect#get_x-1 to obj#get_rect#get_x+obj#get_rect#get_w+1 do
		 for j=obj#get_rect#get_y-1 to obj#get_rect#get_y+obj#get_rect#get_h+1 do
		   
		   if self#out_of_map i j==false then
		     (
		       match player_layers.(p)#get_name with
			 | "rebel" ->
			     (tile_layer#set_position i j 6)
			 | "neonaz" ->
			     (tile_layer#set_position i j 12)
			 | _ -> ();			  
		     )	     
		 done;
	       done;
	     ))
	    )
      done;
      
      (* update tile around resources *)
      self#foreach_object resource_layer 
	(fun k obj->(
	   let x=obj#get_rect#get_x and y=obj#get_rect#get_y in
	     for i=x-1 to x+1 do
	       for j=y to y+2 do
		 if self#out_of_map i j==false then (
		   match obj#get_name with 
		     | "nature" -> (tile_layer#set_position i j 6);
		     | "pollution" ->(tile_layer#set_position i j 12);
		     | _ -> ();
		 )
	       done;
	     done;
	     
	 ));
      


      (* update tiles *)
      self#foreach_map_entry_all
	(fun i j->(
	   if self#get_fow_position iam_player i j=1 then		
	     tile_layer#set_shaded i j true;
	   tile_layer#refresh i j;	  
	 ));

      if vrect#has_change==true then (
	for i=vrect_x to vrect_dx do
	  for j=vrect_y to vrect_dy do
	    tile_layer#refresh_force i j;	  
	    player_layers.(iam_player)#refresh_force i j;
	  done;
	done;
      );

      (* set borders *)
      tile_layer#update maptype (vrect#get_x/32) (vrect#get_y/32) (vw/32) (vh/32);
      tile_layer#update_fow player_layers.(iam_player)#get_fow (vrect#get_x/32) (vrect#get_y/32) (vw/32) (vh/32);



      (* put tile_layer ( to btile )*)
      (* FIXME : must be in player_layer *)
      for i=(vrect_x) to (vrect_dx) do
	for j=(vrect_y) to (vrect_dy) do	  	     
	  (
	    let  fow_nu=player_layers.(iam_player)#get_fow_position_nu i j in
	      (* position need update ?*)	     
	      if fow_nu==true then 
		(	       
		  tile_layer#reput player_layers.(iam_player)#get_fow i j vrect_x vrect_y;
(*		  tile_layer#reput_fow player_layers.(iam_player)#get_fow i j vrect_x vrect_y;
*)	  
		);
	      player_layers.(iam_player)#set_fow_position_nu i j false; 	       	
	  )
	done;
      done;

(*
      for j=(vrect_y) to (vrect_dy) do
	for i=(vrect_x) to (vrect_dx) do	  	     
	  if (resource_layer#is_object j i)==true then 
	    resource_layer#reput tile_layer#get_btile j i vrect_x vrect_y;
	  for k=1 to num_players do	  	    
	    if (player_layers.(k)#is_object j i)==true then
	      player_layers.(k)#get_obj_layer#reput tile_layer#get_btile j i vrect_x vrect_y;
	  done;	    
	done;
      done;
*)
      (* put tile layer (to screen) *)
      tile_layer#put ((vrect_x*32)-vrect#get_x) ((vrect_y*32)-vrect#get_y) (vrect#get_x + vw) (vrect#get_y + vh); 


      
      (* put decors *)
      for i=(vrect_y) to (vrect_dy) do
	for j=(vrect_x) to (vrect_dx) do	  
	  if (decor_layer#is_object j i)==true then (
	    let deco=(decor_layer#get_object_by_position j i) in
	    if self#get_fow_position iam_player j i=2 then
	      deco#put vrect#get_x vrect#get_y trect#get_w trect#get_h;	       
	   )
	done;
      done;


      (* put unit shadow*)      
      if this_config.video_opt1=1 then (
	for i=(vrect_y) to (vrect_dy) do
	  for j=(vrect_x) to (vrect_dx) do
	    for k=1 to num_players do
	      if (player_layers.(k)#is_object j i)==true then (
		let obj=(player_layers.(k)#get_obj_by_position j i) in
		if self#get_fow_position iam_player j i=2==true then
		  obj#put_shadow vrect#get_x vrect#get_y trect#get_w trect#get_h;
	       )
	    done;
	  done;
	done;
       );

      (* put unit (no spe) && resources *)
      for i=(vrect_y) to (vrect_dy) do
	for j=(vrect_x) to (vrect_dx) do

	  (* resource *)
	  if (resource_layer#is_object j i)==true then (
	    let reso=(resource_layer#get_object_by_position j i) in
	      if self#get_fow_position iam_player j i=2==true then
		reso#put vrect#get_x vrect#get_y trect#get_w trect#get_h;	       	      
	   );

	  (* unit *)

	  for k=1 to num_players do
	    if (player_layers.(k)#is_object j i)==true then (			      
	      let obj=(player_layers.(k)#get_obj_by_position j i) in
		if obj#get_spe_mode=0 then (
		  if self#get_fow_position iam_player j i=2==true  then
		    (
		      obj#put vrect#get_x vrect#get_y trect#get_w trect#get_h;	       
		    ) 
		) 
	    ) 
	  done;	  
	done;
      done;
      

   (* put unit projectile*)
      for i=(vrect_y) to (vrect_dy) do
        for j=(vrect_x) to (vrect_dx) do
            for k=1 to num_players do
	      if (player_layers.(k)#is_object j i)==true then (	      
		if self#get_fow_position iam_player j i=2==true then (
		  let obj=(player_layers.(k)#get_obj_by_position j i) in
                  obj#put_projectile vrect#get_x vrect#get_y trect#get_w trect#get_h;
		 )
               )
            done;
        done;
      done;

      (* put unit repr && lifebar *)
      for i=(vrect_y) to (vrect_dy) do
	for j=(vrect_x) to (vrect_dx) do
	    for k=1 to num_players do
	      if (player_layers.(k)#is_object j i)==true then (	      
		if self#get_fow_position iam_player j i=2==true then	       
		  (
		   let obj=(player_layers.(k)#get_obj_by_position j i) in
		   if this_config.game_opt1=1 then (
		     let x=obj#get_rect#get_x and y=obj#get_rect#get_y in
		     let px=obj#get_prect#get_x and py=obj#get_prect#get_y in
		     let w=obj#get_rect#get_w*32 and h=obj#get_rect#get_h*32 in
		     let gr=obj#get_graphic in
		     player_layers.(k)#put_repr (x*32 - vrect#get_x + px +w) (y*32 - vrect#get_y + py -(gr#get_rect#get_h - h));
		    );
		   if this_config.game_opt2=1 then (
		     obj#put_lifebar vrect#get_x vrect#get_y trect#get_w trect#get_h;	       
		    )
		  )
	      )
	  done;
	done;
      done;


      (* init face entries from selection *)                                                    
      for i=0 to selection#get_nb_selected do
	if (selection#get_selected i)<>0 then
	  (
	   self#put_unit_select (iam_player) (selection#get_selected i);
	  )
      done;

      mul_selection#create();

      (* construct put *)
      if player_layers.(iam_player)#get_construct_requested<>0 then (
	let o=self#get_construct iam_player player_layers.(iam_player)#get_construct_requested in
	  o#put_shaded vrect#get_x vrect#get_y trect#get_w trect#get_h;
	  if self#can_construct_here iam_player o#get_rect#get_x o#get_rect#get_y==false then ( 

	  );
	  let cant_array=self#can_construct_here_array iam_player o#get_rect#get_x o#get_rect#get_y in
	    for i=0 to o#get_rect#get_w do
	      for j=0 to o#get_rect#get_h do
		if cant_array.(i).(j)==false then (
		  forbid#move ((o#get_rect#get_x+i)*32 - vrect#get_x) ((o#get_rect#get_y+j)*32 - vrect#get_y);
		  forbid#put();
(*		  let ti=tile_rect_alpha 32 32 (255,0,0) 127 in
		    tile_put ti (32*(o#get_rect#get_x+i) - vrect#get_x) (32*(o#get_rect#get_y+j) - vrect#get_y);
		    tile_free ti;
*)
		)
	      done;
	    done;
	  o#init_put(); 
      );

      (* put unit (spe)*)
      for i=(vrect_y) to (vrect_dy) do
	for j=(vrect_x) to (vrect_dx) do
	  for k=1 to num_players do
	    if (player_layers.(k)#is_object j i)==true then
	      (
		let obj=(player_layers.(k)#get_obj_by_position j i) in
		  if obj#get_spe_mode>0 then (
		    if self#get_fow_position iam_player j i=2 || spectator==true then	       
		      obj#put vrect#get_x vrect#get_y trect#get_w trect#get_h;	       
		  );
	      )
	  done;
	done;
      done;


      video_set_clip 0 0 (f_size_w 800) (f_size_h 600); 

      (* update iface *)
      iface#update();

      (* COMBO, SERIAL KILLER, ...*)
      comb#put();

      (* minimap *)
	   (* FIXME : this doesn't work for more than 2 player *) 
      	   let en_p=if iam_player=1 then 2 else 1 in

	   let fow=(player_layers.(iam_player))#get_fow in
	   let minimap=tile_minimap (player_layers.(iam_player))#get_obj_layer#get_lay (player_layers.(en_p))#get_obj_layer#get_lay fow rect#get_w rect#get_h in
	   tile_put minimap (f_size_w 80) (f_size_h 485);
	   tile_free minimap;
	   
	   let nx=int_of_float(((float_of_int(vrect#get_x)/.32.0)*.(72.0/.float_of_int(rect#get_w))))
	   and ny=int_of_float(((float_of_int(vrect#get_y)/.32.0)*.(72.0/.float_of_int(rect#get_w))))
	   and nw=int_of_float(((float_of_int !scr_w)/.32.0)/.(float_of_int(rect#get_w))*.72.0)
	   and nh=int_of_float(((float_of_int !scr_h)/.32.0)/.(float_of_int(rect#get_h))*.72.0)
	   in
	   let minimap_select=tile_rect nw nh (0,0,255) in 
	   tile_put minimap_select ((f_size_w 80)+nx) ((f_size_h 485)+ny);
	   tile_free minimap_select;


	   vrect#reinit();
      

(* UNIT action methods *)
(* FIXME : must rewrite these methods *)
(*

* unit_get_next_path cx cy dx dy 
 get the next best position to go : 
 cx,cy = current position
 dx,dy = destination position
 return (rect: next position , int : direction)

* unit_be_polit player num
 if someone want to go at the same position that player num unit, let it go
 player = unit player
 num = unit number
 return (unit)

* unit_attack player x y aplayer ax ay
 player unit at x,y attack aplayer unit at ax ay
 player= attacking unit player
 x,y= attacking unit position
 aplayer=attacked unit player
 ax,ay= attacked unit position

* unit_move_for_attack player num dx dy
 player unit num go to attack position dx,dy
 player : attacking unit player
 num : attacking unit position
 dx,dy : destination position

* unit_move player num dx dy
 player unit num move to dx,dy

*)
    method unit_get_next_path cx cy dx dy =
      let direction=ref 0 in
      let best_rect=(new rectangle 0 0 0 0) in 
      let best=ref 0 in
      let heuristic x1 y1 x2 y2=
	((x2 - x1)*(x2 - x1)+(y2 - y1)*(y2 - y1)+(
	 if x1>0 && y1>0 && x1<rect#get_w && y1<rect#get_h then ( 
	   let r=ref false in 
	   for k=1 to num_players do 
	       r:=(!r || (player_layers.(k)#is_object x1 y1)) 
	   done;

	   if (!r=true) then (
	     rect#get_w+rect#get_h
	    ) 
	   else 0
	  ) 
	 else 0
	       
	)) in 

      
      let a=Array.make 9 0 in
      a.(0)<-heuristic (cx) (cy - 1) dx dy;
      a.(1)<-heuristic (cx + 1) (cy - 1) dx dy;
      a.(2)<-heuristic (cx + 1) (cy) dx dy;
      a.(3)<-heuristic (cx + 1) (cy + 1) dx dy;
      a.(4)<-heuristic (cx) (cy + 1) dx dy;
      a.(5)<-heuristic (cx - 1) (cy + 1) dx dy;
      a.(6)<-heuristic (cx - 1) (cy) dx dy;
      a.(7)<-heuristic (cx - 1) (cy - 1) dx dy;

      for i=0 to 7 do
	if a.(i) < a.(!best) then best:=i;
      done;
      
      if !best=0 then best_rect#set_position (cx) (cy - 1);
      if !best=1 then best_rect#set_position (cx + 1) (cy - 1);
      if !best=2 then best_rect#set_position (cx + 1) (cy);
      if !best=3 then best_rect#set_position (cx + 1) (cy + 1);
      if !best=4 then best_rect#set_position (cx) (cy + 1);
      if !best=5 then best_rect#set_position (cx - 1) (cy + 1);
      if !best=6 then best_rect#set_position (cx - 1) (cy);
      if !best=7 then best_rect#set_position (cx - 1) (cy - 1);

      (best_rect,!best)
	

    (* polit function *)
    method unit_be_polit player num=
      let unit=self#get_unit player num in
      let nx=unit#get_drect#get_x and ny=unit#get_drect#get_y in
      let x=unit#get_rect#get_x and y=unit#get_rect#get_y in
      for i=(nx-1) to (nx+1) do	    
	for j=(ny-1) to (ny+1) do	    
	  if i>0 && i<rect#get_w && j>0 && j<rect#get_h then (
	    if i<>x || j<>y then (	      
	      if self#get_unit_num player i j<>0 then (		  
		let obj=self#get_unit_by_position player i j in
		if (obj#get_drect#get_x=nx && obj#get_drect#get_y=ny) 
	      ||   (obj#get_rect#get_x=nx && obj#get_rect#get_y=ny) 
		then (  
		  unit#get_drect#set_position unit#get_rect#get_x unit#get_rect#get_y; 
		  unit#get_rect#set_position unit#get_srect#get_x unit#get_srect#get_y; 
		  unit#get_prect#set_position 0 0; 
		 ))
	     ))
	done;
      done;



   (* NOTES unit_attack
     - boucherie increase must not be here
     - kill unit must not be here -> unit_kill method can be usefull
     - special effect must not be here : needing to implement a event handling method
   *)
    method unit_attack player x y aplayer ax ay=
      let ennemy=if player=1 then 2 else 1 in
      let unit=(player_layers.(player)#get_obj_by_position x y) in      
      let aunit=(player_layers.(aplayer)#get_obj_by_position ax ay) in

      unit#set_state "attack";

      unit#act_start();

      unit#get_prect#set_position 0 0;	   
      unit#get_drect#set_position ax ay; 

      (* attacked unit respond to attack *)
      if (aunit#get_state<>"attack1") 
	  && (aunit#get_state<>"attack2") 
	  &&  (aunit#get_state<>"move_for_attack")
      then (
	if (aunit#get_attack1>0 && aunit#get_attack2>=0) then
	  (
	  if aunit#get_state <> "dead" then
	    (
	     aunit#get_drect#set_position 0 0;
	     self#unit_move_for_attack aplayer (self#get_unit_num aplayer ax ay)  x y; 
	    )
	 )
       );

      unit#set_act (function()->(
	(* FIXME : dirty *)

	if self#is_unit ax ay==false then
	  (
	   unit#set_state "idle";
	  );

	if (player_layers.(aplayer)#is_object ax ay)==true then 
	  (	  
	      let res1=self#unit_get_next_path unit#get_rect#get_x unit#get_rect#get_y ax ay in
	  
	      (* attaque corps a corps *)
		if (x-ax)<2 && (x-ax)>(-2) && (y-ay)<2 && (y-ay)>(-2) then (
		if unit#get_attack1=0 then (
		 )else(
		  aunit#set_bloody;
		  unit#set_state "attack1";
		  
		  (* ennemy unit dead *)
		  if aunit#get_life < 0 then (
		    if aunit#get_state <> "dead" then (
		    aunit#set_state "dead";
		    unit#set_killed (unit#get_killed + 1);
		    (* boucherie augmente *)
		    player_layers.(player)#resource_set "boucherie" (player_layers.(player)#resource_get "boucherie" + unit#get_killed);

(*		      
		      if unit#get_killed>=2 && unit#get_name="guerrier_bud" then (
			explo_gb#move (32*aunit#get_rect#get_x - vrect#get_x) (32*aunit#get_rect#get_y - vrect#get_y);
			explo_gb#put();
		       );

		      if unit#get_killed>=2 && unit#get_name="lance_pepino" then (
			explo_lp#move (32*aunit#get_rect#get_x - vrect#get_x) (32*aunit#get_rect#get_y - vrect#get_y);
			explo_lp#put();
		       )
*)
		     );
		    unit#set_state "idle";		      

		   )
		  else
		    aunit#set_life (aunit#get_life - (unit#get_attack1*aunit#get_defend1)/100 - 1);
		  
		 )		
	       ) else (
		if unit#get_attack2=0 then (
		  (* pas d'attaque 2, doit se raprocher *)
		  if unit#get_attack1<>0 then (
		    unit#set_state "move_for_attack";
		    unit#get_drect#set_position ((fst res1)#get_x) ((fst res1)#get_y); 
		    unit#turn (snd res1);
		   )
		 ) else (
		  (* attaque arme a feu OU explosif *)            
		  aunit#set_bloody;
		  unit#set_state "attack2";
		  
		  if aunit#get_life < 0 then (
		    if aunit#get_state <> "dead" then (
		    aunit#set_state "dead";		    
		    unit#set_killed (unit#get_killed + 1);
		    (* boucherie augmente *)
		    player_layers.(player)#resource_set "boucherie" (player_layers.(player)#resource_get "boucherie" + unit#get_killed);

		     );
		    unit#set_state "idle";
		   )
		  else (
		    aunit#set_life (aunit#get_life - (unit#get_attack2*aunit#get_defend2)/100 - 1);
		    
		    (* special effect handling - HAHA COMBO!*)
		    if unit#get_name = "bazooka" then (
		      if aunit#get_rect#get_w=1 && aunit#get_rect#get_h=1 then
		      (aunit#set_spe (aunit#get_spe + 1));
		     );
		   )
		 )
	       );
	      
	     )
	else
	  (
	   unit#set_state "idle";
	  )
       ));


    method unit_move_for_attack player num dx dy=
      let u=(player_layers.(player)#get_obj num) in
      let res1=self#unit_get_next_path u#get_rect#get_x u#get_rect#get_y dx dy in
      u#set_state "move_for_attack";
      u#get_prect#set_position 0 0;	   
      u#act_start();
      u#get_drect#set_position ((fst res1)#get_x) ((fst res1)#get_y); 
      u#turn (snd res1); 
    
      u#set_act (function()->(
	let res=self#unit_get_next_path u#get_rect#get_x u#get_rect#get_y dx dy in
	let nx=(fst res)#get_x and ny=(fst res)#get_y in

	if ((dx=u#get_rect#get_x) && (dy=u#get_rect#get_y)) then (
	  u#act_stop();
	  u#set_state "idle";
	 )
	else (
	  u#get_drect#set_position nx ny; 
	  u#turn (snd res);

	 );
       ));

    method unit_move player num dx dy=
      let u=(player_layers.(player)#get_obj num) in
      let res1=self#unit_get_next_path u#get_rect#get_x u#get_rect#get_y dx dy in
	let nx=(fst res1)#get_x and ny=(fst res1)#get_y in
      if dx<>u#get_rect#get_x || dy<>u#get_rect#get_y  then (
	u#set_state "move";
	u#get_prect#set_position 0 0;	   
	u#act_start();
	u#get_drect#set_position nx ny; 
	u#turn (snd res1); 
       );	

      u#set_act (function()->(
	let res=self#unit_get_next_path u#get_rect#get_x u#get_rect#get_y dx dy in
	let nx=(fst res)#get_x and ny=(fst res)#get_y in


	  if (dx=u#get_rect#get_x && dy=u#get_rect#get_y)  
	  then (
	    u#act_stop();
	    u#set_state "idle";
	   )
	  else (
	    u#get_drect#set_position nx ny;
            u#turn (snd res);
	   );
	    
	  if (dx=u#get_drect#get_x && dy=u#get_drect#get_y)  
	  then (
	    if self#is_unit dx dy then
	      (
	       u#act_stop();
	       u#set_state "idle";
	      )
	   );

       ));

    method unit_kill player num=
     let u=(player_layers.(player)#get_obj num) in      
     u#set_state "dead";

    method unit_hit player num aplayer anum a=
     let unit=(player_layers.(player)#get_obj num) and
         aunit=(player_layers.(aplayer)#get_obj anum) in                
     if a=1 then
     aunit#set_life (aunit#get_life - (unit#get_attack1*aunit#get_defend1)/100 - 1);
     if a=2 then
     aunit#set_life (aunit#get_life - (unit#get_attack2*aunit#get_defend2)/100 - 1); 

    (* resources *)
    method add_resource x y obj=
      obj#move x y;
      resource_layer#add_object obj
    method del_resource num=resource_layer#add_del_stack num
    method get_resource num=resource_layer#get_object num
    method get_resource_by_position x y=resource_layer#get_object_by_position x y
    method get_resource_num x y=resource_layer#get_position x y

    (* decors *)
    method add_decor x y obj=
      obj#move x y;
      decor_layer#add_object obj
    method del_decor num=decor_layer#add_del_stack num
    method get_decor num=decor_layer#get_object num
    method get_decor_by_position x y=decor_layer#get_object_by_position x y
    method get_decor_num x y=decor_layer#get_position x y
	
    (* units *)
    method add_unit player x y obj=
      obj#move x y;
      player_layers.(player)#add_obj obj;
      
    method del_unit player num=
      let o=player_layers.(player)#get_obj num in
	if o#get_rect#get_w=1 && o#get_rect#get_h=1 then (
	  self#add_decor o#get_rect#get_x o#get_rect#get_y (o); 
	  (*      o#get_graphic#move  (32*o#get_rect#get_x - vrect#get_x + o#get_prect#get_x) (32*o#get_rect#get_y - vrect#get_y + o#get_prect#get_y);
		  o#get_graphic#put_to tile_layer#get_btile;
	  *)
	)else(
	  if self#out_of_map (o#get_rect#get_x + 6) (o#get_rect#get_y + 4)==false then
	    self#add_decor o#get_rect#get_x o#get_rect#get_y (new game_object_ruin "ruine" player); 
	);
	player_layers.(player)#add_dead num; 
	

    method del_units player=
      player_layers.(player)#empty_dead_stack();
      
    method move_unit player num x y=player_layers.(player)#move_obj num x y
    method get_unit player num=player_layers.(player)#get_obj num
    method get_unit_by_position player x y=player_layers.(player)#get_obj_by_position x y
    method put_unit_select player num=(player_layers.(player)#get_obj num)#put_select vrect#get_x vrect#get_y tw th

    method get_unit_num player x y=player_layers.(player)#get_obj_num x y

    method is_unit x y=
      let r=ref false in	
      for k=1 to num_players do 
(*	if self#out_of_map x y==false then *)
	  r:=(!r || (player_layers.(k)#is_object x y)) 
(*	else r:=false; *)
      done;
      !r

    method get_unit_player x y=
	let r=ref 0 in	
	for k=1 to num_players do 
	  if (player_layers.(k)#is_object x y) then
	    r:= k;(*player_layers.(k)#get_obj_layer#get_position x y;*)
	done;
	!r

    method to_value=
      let t={
	w=rect#get_w;
	h=rect#get_h;
	resource_layer=resource_layer#to_value;
	decor_layer=decor_layer#to_value;
        player_layers=
		       (let a=Array.make (num_players+1) (player_layers.(0)#to_value 0 0 0 0) in		      
			  for i=1 to num_players do
			    a.(i)<-(player_layers.(i)#to_value  0 0 0 0)
			      
			  done;
			  a)
      } in
	t

    method from_value a=      
      rect#set_size a.w a.h;
      resource_layer#from_value a.resource_layer obj_types;
      decor_layer#from_value a.decor_layer obj_types;

      for i=1 to num_players do
	player_layers.(i)#from_value (a.player_layers).(i) obj_types
      done;

  end;;

