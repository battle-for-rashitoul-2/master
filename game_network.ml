(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)

open Low;;
open Network;;
open Unix;;

open Game_map;;

(*
class game_network=
object(self)
  method get_data ic=
    let a=Marshal.from_channel ic in
      a;
      
  method set_data oc data=
    Marshal.to_channel oc data [];
end;;
*)
class game_server_connection gm (sd,sa)=
object(self)
  val mutable ic=Unix.in_channel_of_descr sd
  val mutable oc=Unix.out_channel_of_descr sd

		   
  method start()=Thread.create (function()->self#run()) ();

  method run()=
    try while true do       

      gm#sync_map_from_client ic; 	      	
      gm#sync_map_to_client oc;	       
      flush oc;

    done 
    with _ -> ();

end;;


class game_server gm=
object(self)
  val port=5000
  val mutable sock=Unix.socket PF_INET Unix.SOCK_STREAM 0 
  val mutable gm=gm
  val mutable m=Mutex.create();
		   
  initializer
  let my_address=get_my_addr() in
    Unix.bind sock (Unix.ADDR_INET(my_address,port)) ;
    Unix.listen sock 3;

  method run_game()=
    while true do
      let toc1=ref 0 and
	toc2=ref 0 in
	
	toc1:=time_get();

	Mutex.lock m;	
	gm#update_server();
	Mutex.unlock m;
	(*print_string "SERVER : update";print_newline();*)
	
	
	toc2:=time_get();
	time_sleep(60 -(!toc2- !toc1));
    done
    

  method run()=
    print_string "SERVER: started";print_newline();
    Thread.create(function()->self#run_game()) ();

    while true do
      let (sd,sa)=Unix.accept sock in
      let conn=new game_server_connection gm (sd,sa) in
	print_string "SERVER: new connection";print_newline(); 
	conn#start();	
    done;

(*    print_string "SERVER : started";print_newline();
    server_start self#serv_fun (get_my_addr()) 5000;   
*)
(*      match Unix.fork() with
	|0 -> 
	   if Unix.fork() <> 0 then exit 0;
	     print_string "SERVER : started";print_newline();
	     server_start self#serv_fun (get_my_addr()) 5000;   
	|id ->
	 while true do
	     let toc1=ref 0 and
	       toc2=ref 0 in

	       toc1:=time_get();

	       gm#update_server();
	       print_string "SERVER : update";print_newline();
	       

	     toc2:=time_get();
	     time_sleep(60 -(!toc2- !toc1));
	   done
*)
end;;


class game_network_client gm=
object (self) 
  method client_fun ic oc=
    gm#sync_map_from_server ic;
    gm#update_client();
    gm#sync_map_to_server oc; 
    flush oc; 		
       
  method init()=
    client_start self#client_fun (get_my_addr()) 5000;
      
end;;

