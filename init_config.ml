open Config;;

let configfile=
if Sys.os_type="Unix" then (Sys.getenv("HOME")^"/.bfr.conf")
else "bfr.conf";;

let this_config=load_config configfile;;

let save_config()=save_file configfile this_config;;

at_exit (save_config);;


let set_lang l=this_config.lang<-l;;

let args=[
("-lang",Arg.String (set_lang),"language")] in
let usage= "usage : init_config -lang lang" in
Arg.parse args (fun s -> ()) usage;;
