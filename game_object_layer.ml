(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open Rect;;
open Video;;
open Object;;
open Game_object;;
open Game_object_type;;
open Game_layer;;

(** Game object layer class definition *)

type game_object_lay={
  objs:(int * game_obj) array;
  is_objs:(int * bool) array; 
(*  lay:int array array; *)
};;

class game_del_stack=
object
  val mutable del_stack=Stack.create();
  method add_del_stack k=Stack.push k del_stack; 
  method empty_del_stack (del_func:int->unit)=
    while Stack.length del_stack <> 0 do
      let a=(Stack.top del_stack) in
      del_func a;
      Stack.pop del_stack;
    done;
end;;

class game_object_layer wi hi max=
  object (self)
    inherit game_layer wi hi as super
    val mutable del_stack=new game_del_stack
    val mutable objs=Array.make max none_obj
    val mutable is_objs=Array.make max false
    val mutable cur_obj=max-1

    method get_cur_obj=cur_obj  
(*    method get_objects=objs  *)
	
    method foreach_object d=
      let f i v=if is_objs.(i)==true then d i v in
      Array.iteri f objs;


    method foreach_map_object d=
      self#foreach_map_entry (fun i j v->(if is_objs.(v)==true then d i j objs.(v)))

    method clear()=
      for i=0 to wi-1 do
	for j=0 to hi-1 do
	  lay.(i).(j)<-0
	done;
      done;

      for k=1 to cur_obj do
	objs.(k)<-none_obj
      done;
      
    method print_para_o w num=
      print_string ("GAME OBJECT LAYER PARACHUTE : "^w^" "^string_of_int(num)^" OUT OF ARRAY");
      print_newline();

    method out_of_a num=
      if num>=0 && num<=cur_obj then false else true

    method get_object_by_position x y=            
      let n=self#get_position x y in
      self#get_object n

    method get_object num=      
      objs.(num)

    method is_object x y=
      if self#get_position x y<>0 then true else false

    method is_object_num num=
      is_objs.(num);

    method is_object_num_with_check num=
      if self#out_of_a num==false then
	self#is_object_num num
      else false

    method set_object num obj=    
      objs.(num)<-obj

    method add_object obj=
      if  obj#get_name<>"none" then (
	let k=ref 1 in
	
	while self#is_object_num_with_check (!k)==true do k:=!k+1 done;
        if self#out_of_a (!k)==false then (
	  objs.(!k)<-obj;
	  is_objs.(!k)<-true;
	  self#set_position (obj#get_rect#get_x) (obj#get_rect#get_y) !k;
         )
       )

    method add_del_stack k=del_stack#add_del_stack k
    method empty_del_stack()=del_stack#empty_del_stack self#del_object;

    method del_object num=
      self#set_object num none_obj;
      is_objs.(num)<-false;

    method update_obj num=
      let obj=self#get_object num in
      if self#is_object_num num==true then (
	for i=0 to (obj#get_rect#get_w)-1 do
	  for j=0 to (obj#get_rect#get_h)-1 do
	    self#set_position (obj#get_rect#get_x + i) (obj#get_rect#get_y + j)  num;	    
	  done;
	done;
       )
	  
    method move num x y=
      let obj=objs.(num) in
      self#set_position (x) (y) num;
      self#set_position (obj#get_rect#get_x) (obj#get_rect#get_y) 0;
      obj#move x y;

    method move_from cx cy x y=
      let num=self#get_position cx cy in
      self#move num x y;


    method reput btile x y vx vy=
      (self#get_object_by_position x y)#put_to btile (vx*32) (vy*32) 32 32;



    method reduce_objs()=
      let c=ref 1 in
	self#foreach_object (fun i v->(
			       if v#get_name<>"none" then
				 c:=!c+1
			     ));
	let a=Array.make (!c+1) (0,none_obj) in
	  c:=1;
	  self#foreach_object (fun i v->(
				 if v#get_name<>"none" then (
				   a.(!c)<-(i,v);
				   c:=!c+1
				 )));
	  a

    method to_value=
      let na=self#reduce_objs() in
      let nr={x=0;y=0;w=0;h=0} in
      let a=Array.make (Array.length na+1) 
	      (0,
	       {name="none";
		rect=nr;
		drect=nr;
		srect=nr;
		prect=nr; 
		state="idle"; 
		life=0;
		direction=0; 
		acting=false; 
	       }) and
	b=Array.make (Array.length na+1) (0,false) in

	for i=1 to (Array.length na-1) do	
	  let (nan,nao)=na.(i) in
	    a.(i)<-(nan,nao#to_value);
	    b.(i)<-(nan,true);
	done;	  
	
	{
	  objs=a;
	  is_objs=b;
	}

    method from_value a (obj_types: game_object_types)=
      let na=self#reduce_objs() in
	for i=1 to (Array.length a.objs - 1) do	
	  let (nan,nao)=a.objs.(i) in
	  let (inan,inao)=a.is_objs.(i) in
	    
	    if objs.(nan)#get_name="none" 
	    then self#set_object nan (obj_types#get_object_type (nao.name));
	    
	    objs.(nan)#from_value nao; 
	    is_objs.(inan)<-inao;
	    self#update_obj nan;
	done;
	
  end;;
