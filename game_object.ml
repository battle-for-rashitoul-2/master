(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open Rect;;
open Video;;
open Object;;

(** Game objects class definitions *)

type anim={mutable current_frame:int;frames:int array;mutable current_refresh:int;refresh:int};;
type action={mutable is:bool;start:unit;stop:unit;act:unit};;

type trect={x:int;y:int;w:int;h:int};;
let rect_to_trect r=
  {
    x=r#get_x;
    y=r#get_y;
    w=r#get_w;
    h=r#get_h
  }

let trect_to_rect tr=
 new rectangle tr.x tr.y tr.w tr.h

type game_obj={
    name:string;
    rect:trect;
    drect:trect;
    srect:trect;
    prect:trect; 
    state:string; 
    life:int;
    direction:int;
    acting:bool;
};;

class game_object_anim frames refresh=
object
    val mutable current_frame=0
    val mutable current_refresh=0
    val mutable frames=frames
    val mutable refresh=refresh
    method get_num_frames=Array.length frames
    method set_frames f=frames<-f
    method set_refresh r=refresh<-r
    method get_frames=frames
    method get_refresh=refresh
    method get_frame=frames.(current_frame)
    method get_current=current_frame
    method get_current_refresh=current_refresh
    method set_current c=current_frame<-c
    method set_current_refresh c=current_refresh<-c

    method anim()=
      if current_refresh = refresh then (
	if current_frame < (Array.length frames)-1 then ( 	  
	  current_frame<-current_frame+1
	 )
	else
	  (
	   current_frame<-0;
	  );
	current_refresh<-0
       )
      else
	current_refresh<-current_refresh+1

    method print_frame=print_int frames.(current_frame)
  end;;

class game_object_action acti=
  object (self)
    val mutable acting=false
    val mutable action=acti
    method set_action a=action<-a 
    method start()=acting<-true
    method stop()=acting<-false
    method act()=if acting==true then action()
    method get_acting=acting
    method set_acting a=acting<-a
  end;;


class game_object_state n f r acti=
  object
    inherit game_object_anim f r
    inherit game_object_action acti
    val mutable name=n
    method get_name=name
    method set_name nm=name<-nm
    method print_name=print_string name
  end;;

let anim_new f r={current_frame=0;frames=f;current_refresh=0;refresh=r};;


let cadre=new graphic_object 7 7  "medias/tiles/cadre_unit.png" false false;;

let tile_rect_true w h=
  let t=tile_rect w h (255,255,255) in
  tile_put_to (cadre#get_tile 0) t 0 0;
  tile_put_to (cadre#get_tile 1) t (w - 7) 0;
  tile_put_to (cadre#get_tile 2) t (w - 7) (h - 7);
  tile_put_to (cadre#get_tile 3) t 0 (h - 7);
  tile_set_alpha t 255 255 255;
  t;;



(** Combo class : handler of arcade COMBO/SERIAL KILLER/MEGAKILL/... *)
class combo=
  object
    val mutable snds=new sound_object [|"none";"medias/misc/combo.wav";"medias/misc/killer.wav";"medias/misc/megakill.wav";"medias/misc/barbarian.wav";"medias/misc/butchery.wav"|];
    val mutable fnt=new font_object "medias/fonts/samos.ttf" 64;
    val mutable cur_c="none"
    val mutable refresh=0
    val mutable nx=0
    val mutable ny=0
    
    (* for stats *)
    val mutable combo=0;
    val mutable serial_killer=0;
    val mutable megakill=0;
    val mutable barbarian=0;
    val mutable butchery=0;

    method get_combo=combo
    method get_serial_killer=serial_killer
    method get_megakill=megakill
    method get_barbarian=barbarian
    method get_butchery=butchery
	
    method action n=
      sound_chan_stop (-1);
      snds#play_simple n;
      cur_c<-match n with
      | 1 ->(combo<-combo + 1;"combo")
      | 2 ->(serial_killer<-serial_killer + 1;"serial killer")
      | 3 ->(megakill<-megakill + 1;"megakill")
      | 4 ->(barbarian<-barbarian + 1;"barbarian")
      | 5 ->(butchery<-butchery + 1;"butchery")
      | _ ->"none";

    method update()=
      if refresh >= 80 then (
	nx<-(randomize 15);
	ny<-(randomize 15);
	cur_c<-"none"; 
	refresh<-0;
       )
      else
	refresh<-refresh+1;
      
    method put()=
      if cur_c<>"none" then (
	let tmp1=fnt#create_text cur_c (0,0,0) in
	let tmp=fnt#create_text cur_c (255,0,0) in
	tile_put tmp1 ((!scr_w - tile_get_w tmp1)/2 -5 + nx) ((!scr_h - tile_get_h tmp1)/2 -5 + ny);
	tile_put tmp ((!scr_w - tile_get_w tmp1)/2) ((!scr_h - tile_get_h tmp1)/2);
	tile_free tmp;	  
	tile_free tmp1;	  
       )

  end;;




class game_object nm gwi ghi tilesfile mirror is_shaded wi hi=
object (self)
    val mutable name=nm
    val mutable aunit=0

    val mutable rect=new rectangle 0 0 wi hi
    val mutable drect=new rectangle 0 0 wi hi
    val mutable srect=new rectangle 0 0 wi hi
    val mutable prect=new rectangle 0 0 gwi ghi

    val mutable direction=0

    val mutable graphic=new graphic_object gwi ghi tilesfile mirror is_shaded

    val mutable sight=2
    val mutable sample_state=new game_object_state "sample" [|0|] 12 (function()->()) 
    val mutable current_state=new game_object_state "sample" [|0|] 12 (function()->()) 
    val mutable need_put=true
    val mutable on_select=(function()->())
    val mutable on_deselect=(function()->())

    val mutable act_freq_cur=0;
    val mutable act_freq=4;

    val mutable will_dead=false
    val mutable spe_var=0
    val mutable spe_mode=0;
    val mutable blocking=true;

    val mutable build_by=0
	
    method get_killed=0
    method set_killed (v:int)=()

    method set_build_by v=build_by<-v
    method get_build_by=build_by
    method set_blocking b=blocking<-b
    method get_blocking=blocking
    method get_can_mulsel=false

    method get_graphic=graphic
    method get_spe_mode=spe_mode
    method set_spe_mode v=spe_mode<-v
    method get_spe=spe_var
    method set_spe v=spe_var<-v

    method i_will_dead=will_dead<-true;
    method will_i_dead=will_dead;

    method set_bloody=()

    method get_aunit=aunit
    method set_aunit a=aunit<-a

    method get_rect=rect
    method get_prect=prect
    method get_drect=drect
    method get_srect=drect
    method get_sight=sight
    method get_name=name

    method set_name n=name<-n

    method set_sight s=sight<-s	

    method print_name=print_string name

    method play_snd (t:string) (vx:int) (vy:int)=()
    
    method resize w h=rect#set_size w h
    method move x y=rect#set_position x y
    method turn dir=direction<-dir;

    method set_anim f r=current_state#set_frames f;current_state#set_refresh r
    method set_act a=current_state#set_action a

    method dump()=      
      if name<>"none" then (
	print_string "UNIT DUMP : ";
	print_string "name : "; print_string name;print_string " - ";
	print_string "x : "; print_int (rect#get_x); print_string " - ";
	print_string "y : "; print_int (rect#get_y); print_string " - ";
	print_string "dx : "; print_int (drect#get_x); print_string " - ";
	print_string "dy : "; print_int (drect#get_y); print_string " - ";
	print_string "state : "; print_string (current_state#get_name);
	print_newline();
       );

    method get_current_state=current_state
    method get_state=current_state#get_name
    method set_state name=                  
      (match name with 
      | "sample" -> current_state<-sample_state	    
      | _ -> ());
      current_state#set_name name
	    
    method anim()=current_state#anim();
    method act (vx:int) (vy:int)=
      if act_freq_cur==act_freq then
	(
	 current_state#act();
	 act_freq_cur<-0;
	)else(
	 act_freq_cur<- act_freq_cur + 1;
       )	  
    method act_start()=current_state#start()
    method act_stop()=current_state#stop()

    method init_put()=need_put<-true;

    method put vx vy tw th=
      if need_put==true then (
	let cur=graphic#get_cur_tile in
	graphic#set_cur_tile (((graphic#get_tiles_size)/8)*direction + current_state#get_frame);
	graphic#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y);
	graphic#put();
	need_put<-false;
       )
    method put_to btile vx vy tw th=
      if need_put==true then (
	let cur=graphic#get_cur_tile in
	graphic#set_cur_tile (((graphic#get_tiles_size)/8)*direction + current_state#get_frame);
	graphic#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y);
	graphic#put_to btile;
	need_put<-false;
       )

    method set_on_select a=on_select<-a
    method on_select_action()=on_select();

    method set_on_deselect a=on_deselect<-a;
    method on_deselect_action()=on_deselect();

    (* virtual methods *)
    method put_lifebar (vx:int) (vy:int) (tw:int) (th:int)=()
    method put_shadow (vx:int) (vy:int) (tw:int) (th:int)=()
    method put_projectile (vx:int) (vy:int) (tw:int) (th:int)=()
    method put_shaded (vx:int) (vy:int) (tw:int) (th:int)=()
    method put_select (vx:int) (vy:int) (tw:int) (th:int)=()
    method put_repr (vx:int) (vy:int) (tw:int) (th:int) (r:graphic_object)=()

    method get_life=0
    method get_life_tot=0
    method get_attack1=0
    method get_attack2=0
    method get_defend1=0
    method get_defend2=0
    method get_attack2_dist=0
    method get_velocity=0
 
    method get_cons_speed=0
    method get_c_cons_s=0

    method set_life (l:int)=()
    method set_c_cons_s (v:int)=()

    method to_value=
(*      print_string ("NETWORK(to_value):"^name^" "^current_state#get_name^" at "^(string_of_int rect#get_x)^" "^(string_of_int rect#get_y));print_newline();  
*)
     {name=name; 
       rect=rect_to_trect rect;
       drect=rect_to_trect drect;
       srect=rect_to_trect srect;
       prect=rect_to_trect prect;   
       state=(self#get_current_state#get_name);
       life=(self#get_life);
       direction=direction;
       acting=current_state#get_acting;
     } 
	

    method from_value a=
 (*     print_string ("NETWORK(from_value):"^a.name^" "^a.state^" at "^(string_of_int a.rect.x)^" "^(string_of_int a.rect.y));print_newline(); 
 *)
      rect<-trect_to_rect a.rect;
      drect<-trect_to_rect a.drect;
      srect<-trect_to_rect a.srect;
      prect<-trect_to_rect a.prect; 
      self#set_state a.state;
      self#set_life a.life;      
      direction<-a.direction;
      current_state#set_acting a.acting;


  end;;



class game_object_ruin nm c=
object
    inherit game_object nm 192 128 "medias/misc/ruine.png" false false 6 4
    method put vx vy tw th=
      if need_put==true then (
	let cur=graphic#get_cur_tile in
	if c=1 then
	    graphic#set_cur_tile 0
	else
	    graphic#set_cur_tile 1;
	graphic#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y);
	graphic#put();
	need_put<-false;

       )
end;;


type state={frames:int array;refresh:int;action:unit->unit;sound:string array};;

class game_explosion nm gwi ghi tilesfile exp_snd exp_state=
object (self)
  inherit game_object nm gwi ghi tilesfile false false 1 1 as super
  val mutable explode=false
  val mutable snd=new sound_object exp_snd;
  val mutable exp_state=new game_object_state "explode" exp_state.frames exp_state.refresh exp_state.action 
    method play_snd (t:string) vx vy=
      snd#move (rect#get_x/32) (rect#get_y/32);
      snd#play_random vx vy;

    method put vx vy tw th=
      if explode==true then (
	if graphic#get_cur_tile<graphic#get_tiles_size-1 then (
	  snd#move (tw*rect#get_x - vx + prect#get_x)
		   (th*rect#get_y - vy + prect#get_y);
	  snd#play_random vx vy;
	  exp_state#anim();
	  graphic#set_cur_tile(exp_state#get_current);
	  graphic#move (tw*rect#get_x - vx + prect#get_x) 
	    (th*rect#get_y - vy + prect#get_y);
	  graphic#put();
	   )
	  else
	    (
	     explode<-false;
	    )
	 );       

end;;



(* we can set an pixel x y pos with projectile + z *)
class game_projectile nm gwi ghi tilesfile is_curve st=
  object
    inherit game_object nm gwi ghi tilesfile true false 1 1 as super
    val mutable z=0
    val mutable curve_state=new game_object_state "curve" st.frames st.refresh st.action
    val mutable rect_state=new game_object_state "rect" st.frames st.refresh st.action
    val mutable prev=0
    val mutable zp=0
    val mutable snd=new sound_object st.sound

    method anim()=curve_state#anim();
    method act (vx:int) (vy:int)=curve_state#act();
    method play_snd (t:string) vx vy=
      snd#move (rect#get_x/32) (rect#get_y/32);
      snd#play_random vx vy;
	  
    method put_shadow vx vy tw th=
      zp<-(
	match curve_state#get_frame with
	| 0 -> 24
	| 1 -> 16
	| 2 -> 8
	| _ -> 0;
      );
(*      prev<-curve_state#get_frame; *)
      let cur=graphic#get_cur_tile in
      let n=tw*th in
      let shadow=tile_ellipse (graphic#get_rect#get_w - zp) ((4*graphic#get_rect#get_h )/10 -zp/3)  (0,0,0) in
      tile_set_alpha shadow 255 255 255;
      tile_put shadow (rect#get_x - vx + prect#get_x) (rect#get_y - vy + prect#get_y + 64);
      tile_free shadow;

    method put vx vy tw th=
      zp<-(
	match curve_state#get_frame with
	| 0 -> 24
	| 1 -> 16
	| 2 -> 8
	| _ -> 0;
      );
      prev<-curve_state#get_frame;
      let cur=graphic#get_cur_tile in

      graphic#set_cur_tile (((graphic#get_tiles_size)/8)*direction + curve_state#get_frame);
      graphic#move (rect#get_x - vx + prect#get_x) (rect#get_y - vy + prect#get_y - zp + 10);
      graphic#put();
      need_put<-false;
  end;;


class game_resource nm sx sy gwi ghi tilesfile wi hi lev r static init_state= 
  object
    inherit
      game_object nm gwi.(r) ghi.(r) (snd tilesfile.(r)) false true wi hi as super
    
    initializer 
      rect#set_position sx sy;
      super#set_blocking (fst tilesfile.(r))

    val mutable ttiles=tilesfile							      
    val mutable level=lev

    val mutable cons_speed=300;
    val mutable c_cons_s=0

    method get_life_tot=lev
    method get_life=level
    method set_life l=level<-l
   
    method get_cons_speed=cons_speed
    method get_c_cons_s=c_cons_s
    method set_c_cons_s c=c_cons_s<-c

    method put vx vy tw th=
      if static==true then
	level<-3;

      if need_put==true then (
	let cur=graphic#get_cur_tile in
	let le=(if level<4 then level else 3) in
	if le > 0 then (
	  graphic#set_cur_tile (le-1);
	  graphic#move (tw*rect#get_x - vx + prect#get_x - (graphic#get_rect#get_w - 32)) (th*rect#get_y - vy + prect#get_y - (graphic#get_rect#get_h - 32) + 16);
	  graphic#put();	  
	 ) 
	else
	  (
	  init_state#set_cur_tile (0);
	  init_state#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y + 16);
	  init_state#put();	  

	  );
	need_put<-false;
       )

    method put_to btile vx vy tw th=
      if static==true then
	level<-3;

      if need_put==true then (
	let cur=graphic#get_cur_tile in
	let le=(if level<4 then level else 3) in
	if le > 0 then (
	  graphic#set_cur_tile (le-1);
	  graphic#move (tw*rect#get_x - vx + prect#get_x - (graphic#get_rect#get_w - 32)) (th*rect#get_y - vy + prect#get_y - (graphic#get_rect#get_h - 32) + 16);
	  graphic#put_to btile;	  
	 ) 
	else
	  (
	  init_state#set_cur_tile (0);
	  init_state#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y + 16);
	  init_state#put_to btile;	  

	  );
	need_put<-false;
       )

    method put_shaded vx vy tw th=
      if static==true then
	level<-3;
      if need_put==true then (
	let cur=graphic#get_cur_tile in
	let le=(if level<4 then level else 3) in
	if le>0 then (
	  graphic#set_cur_tile (le-1);
	  graphic#move (tw*rect#get_x - vx + prect#get_x - graphic#get_rect#get_w/2) (th*rect#get_y - vy + prect#get_y - (graphic#get_rect#get_h) + 64);
	  graphic#put_shaded();	

	 )
	else (
	  init_state#set_cur_tile (0);
	  init_state#move (tw*rect#get_x - vx + prect#get_x - graphic#get_rect#get_w/2) (th*rect#get_y - vy + prect#get_y - (graphic#get_rect#get_h) + 64);
	  init_state#put_shaded();	
	 );
	need_put<-false;
       )



  end;;

class game_resource_random nm sx sy gwi ghi tilesfile wi hi lev init_state=
  object
    inherit
    let r=(randomize (Array.length tilesfile)) in     
    game_resource nm sx sy gwi ghi tilesfile wi hi lev r false init_state
      
  end;;


class game_unit nm sx sy gwi ghi tilesfile mirror is_shaded wi hi sel_sounds idle_state sight_d=
  object
    inherit game_object nm gwi ghi tilesfile mirror is_shaded wi hi as super
    initializer rect#set_position sx sy;sight<-sight_d
    val mutable select_tile=
      new graphic_dyn_object ("select:"^nm) 0 (
      function k->(
	tile_rect_true gwi ghi)
     );

    val mutable idle_state=new game_object_state "idle" idle_state.frames idle_state.refresh idle_state.action 
    val mutable selection_sounds=new sound_object sel_sounds;

(* these must be set in creation *)
    method put vx vy tw th=
      if need_put==true then (
	let cur=graphic#get_cur_tile in
	graphic#set_cur_tile (((graphic#get_tiles_size)/8)*direction + current_state#get_frame);
	graphic#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y);
	graphic#put();
	need_put<-false;

       )

    method set_state name=
      (match name with
      | "idle" -> current_state<-idle_state	    
      | _ -> ());
      super#set_state name;
      

    method put_select vx vy tw th=
      if spe_mode=0 then (
	select_tile#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y); 
	select_tile#put();
       )

    method on_select_action()=
      super#on_select_action();
      (*selection_sounds#play_random()*)
  end;;




class game_unit_building nm sx sy gwi ghi tilesfile wi hi sel_sounds idle_state  destruct lif def1 def2 sight_d=
  object (self)
    inherit game_unit nm sx sy gwi ghi tilesfile false true wi hi sel_sounds idle_state sight_d as super
    initializer current_state<-idle_state;    
    val mutable construct=0
    val mutable dead_state=new game_object_state "dead" [|0;0;0|] 8 (function()->()) 

    val mutable defend1=100 - (def1*10) 
    val mutable defend2=100 - (def2*10) 

    val mutable life=((lif*100)+100)*4
    val mutable life_tot=((lif*100)+100)*4
    val mutable cons_speed=1;
    val mutable c_cons_s=0

    val mutable explosion=new graphic_object 288 280 "medias/misc/explosion.png" false false;
    val mutable explode=false
    val mutable exp_anim=new game_object_anim [|0;1;2;3;4;5;6;7;8;9;10;11|] 5;
    val mutable exp_sound=new sound_object [|"medias/misc/explosion.wav"|];

    method get_cons_speed=cons_speed 
    method get_c_cons_s=c_cons_s 
    method set_c_cons_s c=c_cons_s<-c

    method set_state name=
      (match name with
      | "idle" -> current_state<-idle_state	    
      | "attack" -> current_state<-idle_state	    
      | "attack1" -> current_state<-idle_state	    
      | "attack2" -> current_state<-idle_state	    
      | "move_for_attack" -> current_state<-idle_state	    
      | "dead" -> current_state<-dead_state	    
      | _ -> ());
      super#set_state name;

    method get_attack1=0
    method get_attack2=0

    method get_defend1=defend1
    method get_defend2=defend2

    method get_life=life
    method set_life l=life<-l
    method get_life_tot=life_tot

    method act (vx:int) (vy:int)=
      if life<=0 then explode<-true;
      if act_freq_cur=act_freq then
	(
	 current_state#act();
	 act_freq_cur<-0;
	)else(
	act_freq_cur<- act_freq_cur + 1;
       )	  
	  
    method put vx vy tw th=
      if need_put==true then (
      let dest=
	let r=ref 0 in
	if self#get_life<(3*self#get_life_tot)/4 then
	  r:=1;
	if self#get_life<(1*self#get_life_tot)/2 then
	  r:=2;
	if self#get_life<(1*self#get_life_tot)/4 then
	  r:=3;
	!r
      in	      
	let cur=graphic#get_cur_tile in
	graphic#set_cur_tile (current_state#get_current+destruct.(dest));
	graphic#move (tw*rect#get_x - vx + prect#get_x - sx*32) 
	  (th*rect#get_y - vy + prect#get_y - sy*32);
	graphic#put();	

	if explode==true then (
	  if explosion#get_cur_tile<explosion#get_tiles_size-1 then (
	  exp_sound#move (tw*rect#get_x - vx + prect#get_x - sx*32) 
	  (th*rect#get_y - vy + prect#get_y - sy*32);
	  exp_sound#play_random vx vy;
	  exp_anim#anim();
	  explosion#set_cur_tile(exp_anim#get_current);
	  explosion#move (tw*rect#get_x - vx + prect#get_x - sx*32) 
	    (th*rect#get_y - vy + prect#get_y - sy*32);
	  explosion#put();
	   )
	  else
	    (
	     explode<-false;
	    )
	 );       
	need_put<-false;
       );

    method put_to btile vx vy tw th=
      if need_put==true then (
      let dest=
	let r=ref 0 in
	if self#get_life<(3*self#get_life_tot)/4 then
	  r:=1;
	if self#get_life<(1*self#get_life_tot)/2 then
	  r:=2;
	if self#get_life<(1*self#get_life_tot)/4 then
	  r:=3;
	!r
      in	      
	let cur=graphic#get_cur_tile in
	graphic#set_cur_tile (current_state#get_current+destruct.(dest));
	graphic#move (tw*rect#get_x - vx + prect#get_x - sx*32) 
	  (th*rect#get_y - vy + prect#get_y - sy*32);
	graphic#put_to btile;	

	if explode==true then (
	  if explosion#get_cur_tile<explosion#get_tiles_size-1 then (
	  exp_sound#move (tw*rect#get_x - vx + prect#get_x - sx*32) 
	  (th*rect#get_y - vy + prect#get_y - sy*32);
	  exp_sound#play_random vx vy;
	  exp_anim#anim();
	  explosion#set_cur_tile(exp_anim#get_current);
	  explosion#move (tw*rect#get_x - vx + prect#get_x - sx*32) 
	    (th*rect#get_y - vy + prect#get_y - sy*32);
	  explosion#put();
	   )
	  else
	    (
	     explode<-false;
	    )
	 );       
	need_put<-false;
       );

	  
    method put_lifebar vx vy tw th=
      let lifebar=tile_lifebar (self#get_life) life_tot in
      tile_put lifebar (tw*rect#get_x - vx + prect#get_x - sx*32)
	(th*rect#get_y - vy + prect#get_y - sy*32);
      tile_free lifebar;

    method put_shaded vx vy tw th=
      if need_put==true then (
	let cur=graphic#get_cur_tile in
	graphic#set_cur_tile (current_state#get_frame);
	graphic#move (tw*rect#get_x - vx + prect#get_x - sx*32) 
	  (th*rect#get_y - vy + prect#get_y - sy*32);
	graphic#put_shaded();
	need_put<-false;
       )

    method put_select vx vy tw th=
      select_tile#move (tw*rect#get_x - vx + prect#get_x - sx*32) 
	(th*rect#get_y - vy + prect#get_y - sy*32);
      select_tile#put();

(*
    method put_shadow vx vy tw th=
      if name<>"festival" then (
	 let shadow=tile_ellipse (graphic#get_rect#get_w) (4*(graphic#get_rect#get_h)/10) (0,0,0) in
	 tile_set_alpha shadow 255 255 255;
	 tile_put shadow (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_x + graphic#get_rect#get_h/2 - 16);
	 tile_free shadow;
      )
*)
  end;;




class game_unit_char1 nm mirror is_shaded sx sy gwi ghi tilesfile wi hi sel_sounds idle_state move_state attack_state_i dead_state vel att1 def1 att2 def2 lif sight_d att2_d projectile=
  object (self)
    inherit game_unit nm sx sy gwi ghi tilesfile mirror is_shaded wi hi sel_sounds idle_state sight_d as super
    initializer super#set_state "idle"
    val mutable killed=0

    val mutable panik=0
    val mutable panik_max=20
    val mutable prev_state="idle"

    val mutable attack1=att1*10
    val mutable attack2=att2*10
    val mutable defend1=100 - (def1*10) 
    val mutable defend2=100 - (def2*10) 

    val mutable life=(lif*100)+100
    val mutable life_tot=(lif*100)+100

    val mutable attack2_dist=att2_d

    val mutable attack2_freq=4
(* (Array.length attack_state.frames)*attack_state.refresh *)
    val mutable attack1_freq=(Array.length attack_state_i.frames)*attack_state_i.refresh

    val mutable velocity=vel;

    val mutable move_sounds=new sound_object move_state.sound
    val mutable move_state=new game_object_state "move" move_state.frames move_state.refresh move_state.action 
    val mutable attack_sounds=new sound_object attack_state_i.sound
    val mutable attack_state=new game_object_state "attack" attack_state_i.frames attack_state_i.refresh attack_state_i.action 
    val mutable dead_state=
      new game_object_state "dead" dead_state.frames dead_state.refresh dead_state.action 
    
    val mutable proj=projectile

    val mutable blood=new graphic_object 41 41 "medias/misc/blood.png" false false
    val mutable bloody=false	
    val mutable cur_blood=0
    val mutable spe_tiles=
      let f=("medias/units/rebels/"^nm^"/pics/"^nm^"-spe.png") in
      if  (Sys.file_exists f) then
	new graphic_object 92 101 f false false
      else
	new graphic_object 92 101 "medias/units/rebels/lance_pepino/pics/lance_pepino-spe.png" false false
    val mutable spe_obj=
      new graphic_dyn_object (nm^"/spe1") 42 (function k->(tile_empty()));

    val mutable  change_move=false

    initializer
      spe_obj<-new graphic_dyn_object (nm^"/spe1") 45 
	(function k->(
	  if k < 31 then (
	    graphic#set_cur_tile (((graphic#get_tiles_size)/8)*(k/4) + dead_state#get_frame);
	    let ctile=graphic#get_tile (graphic#get_cur_tile) in
	    let ctile2=tile_rotozoom ctile (float_of_int(k*12)) (float_of_int(k)/.10.0 +. 1.0) in	    
	    ctile2
	   )							       
	  else
	    (
	     let t=k - 31 in
	     let ct=spe_tiles#get_tile (t/5 + 1) in
	     let ct2=tile_rotozoom ct (0.0) (3.0) in
	     ct2
	    )
	 )
	)

    method get_killed=killed
    method set_killed (v:int)=killed<-v


    method set_bloody=bloody<-true

    method get_can_mulsel=true

    method get_attack1=attack1
    method get_attack2=attack2
    method get_defend1=defend1
    method get_defend2=defend2
    method get_life=life
    method set_life l=life<-l
    method get_life_tot=life_tot
    method get_attack2_dist=attack2_dist
    method get_velocity=velocity
    method set_state nm=

      let print_state n=print_string "STATE(";print_int rect#get_x;print_string " - ";print_int rect#get_y;print_string ") : ";print_string n;print_newline() in

      let old=name in
      (match nm with	
      | "move" -> (current_state<-move_state)
      | "move_for_attack" -> (current_state<-move_state)
      | "attack" -> (current_state<-attack_state)
      | "attack1" -> (current_state<-attack_state)
      | "attack2" -> (current_state<-attack_state)
      | "dead" -> (current_state<-dead_state)
      | _ -> ());
      super#set_state nm; 
      current_state#set_name nm


    method act vx vy=
(*
      if current_state#get_name=prev_state then
	panik<-panik+1
      else (
	panik<-0;prev_state<-current_state#get_name;
       );

      if panik=panik_max then
	(
	 self#set_state "idle";
	 panik<-0
	);
*)
      (* FIXME : must use match here *)

      (* move state *)      
      if current_state#get_name="move" 
    || current_state#get_name="move_for_attack" 
      then
	(
	 if direction=0 then (prect#set_position (prect#get_x) (prect#get_y - velocity));
	 if direction=1 then (prect#set_position (prect#get_x + velocity) (prect#get_y - velocity));
	 if direction=2 then (prect#set_position (prect#get_x + velocity) (prect#get_y)); 
	 if direction=3 then (prect#set_position (prect#get_x + velocity) (prect#get_y + velocity)); 	 if direction=4 then (prect#set_position (prect#get_x) (prect#get_y + velocity)); 
	 
	 if direction=5 then (prect#set_position (prect#get_x - velocity) (prect#get_y + velocity)); 
	 if direction=6 then (prect#set_position (prect#get_x - velocity) (prect#get_y)); 
	 if direction=7 then (prect#set_position (prect#get_x - velocity) (prect#get_y - velocity)); 	 
	 
	 if (prect#get_x/ !tile_w=(drect#get_x - rect#get_x) 
	       && prect#get_y/ !tile_h=(drect#get_y - rect#get_y)) then 
	   (  
	      srect#set_position rect#get_x rect#get_y;
	      rect#set_position drect#get_x drect#get_y;	      
	      prect#set_position 0 0;	   
	      current_state#act();
	     );

	);

      (* attack2 state *)
	if current_state#get_name="attack2" then (
	  if (proj#get_rect#get_x == drect#get_x*32) then
	    (
	     (* here explosion sound *)
	     proj#move (rect#get_x*32) (rect#get_y*32);
	    );

	  if proj#get_rect#get_x == rect#get_x*32 && act_freq_cur==0 then
	    (
	     proj#play_snd "" vx vy;
	    );
	  
	  if act_freq_cur==attack2_freq then
	    (
	     current_state#act();
	     act_freq_cur<-0;
	     let pow d=d*d in
	     proj#get_drect#set_position drect#get_x drect#get_y;
	     proj#move (proj#get_rect#get_x + (drect#get_x*32 - rect#get_x*32)/8) (proj#get_rect#get_y + (drect#get_y*32 - rect#get_y*32)/8) ;
	     
	     proj#anim();
	     proj#turn direction;
	    )else( 	       
	    act_freq_cur<- act_freq_cur + 1;
	   )	  
	 );

      (* attack1 state *)
      if current_state#get_name="attack1" then (
	if act_freq_cur>=attack1_freq then (
	  current_state#act();
	  act_freq_cur<-0;
	 )else(
	  act_freq_cur<- act_freq_cur + 1;
	 );
       );
	(* others state *)
	if current_state#get_name<>"attack2" 
	    && current_state#get_name<>"attack1" 
	    && current_state#get_name<>"move_for_attack" 
	    && current_state#get_name<>"move" 
	then 
	(
	 proj#move (rect#get_x*32) (rect#get_y*32);
	 super#act vx vy; 
	)
         
    method on_select_action()=super#on_select_action();super#turn 4
    method put vx vy tw th=
      if spe_mode=0 then (
      if need_put==true then (
		
	let cur=graphic#get_cur_tile in
	graphic#set_cur_tile (((graphic#get_tiles_size)/8)*direction + current_state#get_frame);
	graphic#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y);
	graphic#put();

	if bloody==true then
	  (
	   if cur_blood==2 then
	     (
	      cur_blood<-0;
	      bloody<-false;
	     )
	   else (
	     blood#set_cur_tile cur_blood;
	     blood#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y);
	     blood#put();
	     cur_blood<-cur_blood + 1;
	    )
	  );
	
	need_put<-false;      
      )
      )
      else
	(
	 (* spe put *)
	 
	 if spe_mode>=42 then
	   (
	    spe_mode<-45;
	   )
	 else
	   (
	    spe_obj#set_cur_tile spe_mode; 
	    if spe_mode<31 then
	      spe_obj#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y)
	    else
	      spe_obj#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y + (spe_mode-31)*15);
	    spe_obj#put();

	    spe_mode<-spe_mode+1;	    
	   );
	)

    method put_to btile vx vy tw th=
      if spe_mode=0 then (
      if need_put==true then (
		
	let cur=graphic#get_cur_tile in
	graphic#set_cur_tile (((graphic#get_tiles_size)/8)*direction + current_state#get_frame);
	graphic#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y);
	graphic#put_to btile;

	if bloody==true then
	  (
	   if cur_blood==2 then
	     (
	      cur_blood<-0;
	      bloody<-false;
	     )
	   else (
	     blood#set_cur_tile cur_blood;
	     blood#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y);
	     blood#put();
	     cur_blood<-cur_blood + 1;
	    )
	  );
	
	need_put<-false;      
      )
      )
      else
	(
	 (* spe put *)
	 
	 if spe_mode>=42 then
	   (
	    spe_mode<-45;
	   )
	 else
	   (
	    spe_obj#set_cur_tile spe_mode; 
	    if spe_mode<31 then
	      spe_obj#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y)
	    else
	      spe_obj#move (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y + (spe_mode-31)*15);
	    spe_obj#put();

	    spe_mode<-spe_mode+1;	    
	   );
	)


    method put_lifebar vx vy tw th=
	let lifebar=tile_lifebar (self#get_life) life_tot in
	tile_put lifebar (tw*rect#get_x - vx + prect#get_x) (th*rect#get_y - vy + prect#get_y - (graphic#get_rect#get_h - th*rect#get_h));
	tile_free lifebar;

    method put_shadow vx vy tw th=
      if spe_mode=0 then
	(
	 let shadow=tile_ellipse (graphic#get_rect#get_w) ((4*graphic#get_rect#get_h)/10) (0,0,0) in
	 tile_set_alpha shadow 255 255 255;
	 tile_put shadow (tw*rect#get_x - vx + prect#get_x +graphic#get_rect#get_w/8) (th*rect#get_y - vy + prect#get_y + (4*graphic#get_rect#get_h)/5 );
	 tile_free shadow;
	 if current_state#get_name="attack2" then (
	   if proj#get_name<>"none" then
	     proj#put_shadow vx vy 0 0;
	  );
	)else(	
	(* spe shadow *)
	 let shadow=tile_ellipse (graphic#get_rect#get_w/(((spe_mode/10)+1))) ((graphic#get_rect#get_h)/(((spe_mode)/10)+1)) (0,0,0) in
	 tile_set_alpha shadow 255 255 255;
	 tile_put shadow (tw*rect#get_x - vx + prect#get_x +graphic#get_rect#get_w/8) (th*rect#get_y - vy + prect#get_y + (4*graphic#get_rect#get_h)/5 );
	 tile_free shadow;

       )

      
    method put_projectile vx vy tw th=
      if current_state#get_name="attack2" then (
	if proj#get_name<>"none" then
	proj#put vx vy 0 0;
       );

    method put_repr vx vy tw th repr=
      repr#move (tw*rect#get_x - vx + prect#get_x - sx*32) 
	(th*rect#get_y - vy + prect#get_y - sy*32);
      repr#put();	


    method play_snd t vx vy=           
      match t with

      | "select" -> (
	  selection_sounds#move rect#get_x rect#get_y;
	  selection_sounds#play_random vx vy;
	 )
      | "move" -> (
	  move_sounds#move rect#get_x rect#get_y;
	  move_sounds#play_random vx vy;
	 )     
      | "attack" -> (
	  attack_sounds#move rect#get_x rect#get_y;
	  attack_sounds#play_random vx vy;
	 )     
      | "proj" -> (	  
	  proj#play_snd "" vx vy;
	 )     
      | _ -> ();
      
      super#play_snd t vx vy; 


  end;;

class game_unit_char nm sx sy gwi ghi tilesfile wi hi sel_sounds idle_state move_state attack_state_i dead_state vel att1 def1 att2 def2 lif sight_d att2_d projectile=
  object
    inherit game_unit_char1 nm true false sx sy gwi ghi tilesfile wi hi sel_sounds idle_state move_state attack_state_i dead_state vel att1 def1 att2 def2 lif sight_d att2_d projectile


  end;;

let none_state={frames=[|0|];refresh=8;action=(function()->());sound=[|"none"|]}
let none_obj=(new game_object "none" (!tile_w) (!tile_h) "none" false false 1 1);;
let forbid=new graphic_object 20 20 "medias/misc/forbide.png" false false;;



class game_unit_tower nm sx sy gwi ghi tilesfile wi hi sel_sounds idle_state attack_state_i  destruct att1 def1 att2 def2 lif sight_d att2_d projectile=
object (self)
  inherit game_unit_building nm sx sy gwi ghi tilesfile wi hi sel_sounds idle_state  destruct lif def1 def2 sight_d as super
  val mutable attack1=att1*10
  val mutable attack2=att2*10
  val mutable attack2_dist=att2_d
  val mutable proj=projectile

  val mutable attack_state=new game_object_state "attack" attack_state_i.frames attack_state_i.refresh attack_state_i.action 
  val mutable attack2_freq=4
  val mutable attack1_freq=4
(*(Array.length attack_state_i.frames)*attack_state_i.refresh*)


  method set_state nm=
      (match nm with
      | "attack" -> current_state<-attack_state	    
      | "attack1" -> current_state<-attack_state	    
      | "attack2" -> current_state<-attack_state	    
      | _ -> ());
    if nm<>"attack" 
	&& nm<>"attack1" 
	&& nm<>"attack2" then 
      super#set_state nm;
    current_state#set_name nm 


  method act vx vy=
    if current_state#get_name="attack2" ||  current_state#get_name="attack1" then (
      proj#move (rect#get_x*32) (rect#get_y*32);
      if act_freq_cur=0 then
	proj#play_snd "" vx vy;
     );
    super#act vx vy;

  method play_snd t vx vy=           
    match t with
    | "proj" -> (	  
	proj#play_snd "" vx vy;
       )     
    | _ -> ();
	super#play_snd t vx vy; 

  method get_attack2_dist=attack2_dist
  method get_attack1=attack1
  method get_attack2=attack2


end;;  
