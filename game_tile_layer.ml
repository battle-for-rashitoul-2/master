(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open Rect;;
open Video;;
open Object;;
open Game_layer;;



(** Game tile layer class definition *)

type game_tile_lay={
  tiles:int array array;
(*  nu:bool array array;
  new_l:int array array
*)
  };;

class game_tile_layer wi hi tw th=
  object (self)
    inherit game_layer wi hi
	(* val mutable lay : int array array *) 
    val mutable terrains_tiles=
      new graphic_object (!tile_w) (!tile_h) "medias/tiles/terrains.png" false true
    val mutable borders_tiles=
      new graphic_object (!tile_w) (!tile_h) "medias/tiles/bordures.png" false true 
    val mutable fow_tiles=
      new graphic_object (!tile_w) (!tile_h) "medias/tiles/fow.png" false false
	
    val mutable btile=
      tile_box (!scr_w) (!scr_h) (0,0,0) 
(*      tiles_put (Array.make (wi*hi) tb) wi hi *)

    val mutable border_lay=Array.make_matrix wi hi 0
    val mutable nu_lay=Array.make_matrix wi hi false
    val mutable nu_shaded_lay=Array.make_matrix wi hi false
    val mutable new_lay=Array.make_matrix wi hi 0

    val mutable fow_border_lay=Array.make_matrix wi hi (-1)

    method free_btile()=tile_free btile
    method get_btile=btile
	      

    method set_border_position x y v=
      if border_lay.(x).(y)<>v then
	nu_lay.(x).(y)<-true;

      border_lay.(x).(y)<-v;

    method set_fow_border_position x y v=      
      if fow_border_lay.(x).(y)<>v then
	nu_lay.(x).(y)<-true;
      fow_border_lay.(x).(y)<-v;


    method refresh x y=
      if new_lay.(x).(y)<>lay.(x).(y) then
	(
	 lay.(x).(y)<-new_lay.(x).(y);
	 nu_lay.(x).(y)<-true;
	)

    method refresh_force x y=
      	 nu_lay.(x).(y)<-true;

    method set_shaded x y (v:bool)=
      if nu_shaded_lay.(x).(y)<>v then
	nu_lay.(x).(y)<-true;
      nu_shaded_lay.(x).(y)<-v;
      
    method set_position x y v=
      let mt=(lay.(x).(y) mod 3) in
      new_lay.(x).(y)<-(v+mt)
    
    method reput (fow_lay:int array array) x y vx vy=
      if nu_lay.(x).(y)==true then (
	if nu_shaded_lay.(x).(y)==false then (
	  self#put_to x y vx vy false;      
	 ) else (
	  self#put_to x y vx vy true;      
	 );
	self#put_fow_to fow_lay x y vx vy; 
      )

    method reput_fow (fow_lay:int array array) x y vx vy=
      if nu_lay.(x).(y)==true then (
	self#put_fow_to fow_lay x y vx vy;
      );

    method put_fow_to (fow_lay:int array array) x y vx vy=      
      let b=fow_border_lay.(x).(y) 
      and n=fow_lay.(x).(y) in
	if b<>(-1) then (
	  fow_tiles#move ((x-vx)* !tile_w) ((y-vy)* !tile_h);
	  fow_tiles#set_cur_tile b;
	  fow_tiles#put_to btile;  
	  nu_lay.(x).(y)<-true;
	);
	if n=0 then (
	  fow_tiles#move ((x-vx)* !tile_w) ((y-vy)* !tile_h);
	  fow_tiles#set_cur_tile 0;
	  fow_tiles#put_to btile;  
	  nu_lay.(x).(y)<-true;
	);
	  
    method put_to x y vx vy shaded =
      let blay=border_lay.(x).(y) in
      terrains_tiles#move ((x-vx)* !tile_w) ((y-vy)* !tile_h);      
      terrains_tiles#set_cur_tile (lay.(x).(y));
      if shaded==true then (
	terrains_tiles#put_shaded_to btile;
	if blay>0 then (
	  borders_tiles#move ((x-vx)* !tile_w) ((y-vy)* !tile_h);      
	  borders_tiles#set_cur_tile (blay - 1);
	  borders_tiles#put_shaded_to btile
	 );
	nu_lay.(x).(y)<-false;
       )
      else (
	
	terrains_tiles#put_to btile;
	if blay>0 then (
	  borders_tiles#move ((x-vx)* !tile_w) ((y-vy)* !tile_h);      
	  borders_tiles#set_cur_tile (blay - 1);
	  borders_tiles#put_to btile;
	 );
	nu_lay.(x).(y)<-false;
       );

    method set_border px py f t=
      let get_pos x y=self#get_position x y in

      if get_pos px py=f || get_pos px py=(f+1) || get_pos px py=(f+2)   
then (
	let a=Array.make_matrix 3 3 (-1) in
	for v=(-1) to 1 do
	  for w=(-1) to 1 do
	    if (v+px)>0 && (w+py)>0 && (v+px)<rect#get_w && (w+py)<rect#get_h then (	       
		a.(w+1).(v+1)<-(		
		  let r=ref (get_pos (v+px) (w+py)) in
		  if (!r=6 || !r=7 || !r=8) && (f<>6 && f<>7 && f<>8) then r:=-1;
		  if (!r=0 || !r=1 || !r=2) && (t<>0 && t<>1 && t<>2) then r:=-2;
		  if !r=f || !r=(f+1) || !r=(f+2) then r:=6;		  
		  if !r=t || !r=(t+1) || !r=(t+2) then r:=0;
		  !r
		 )
	       )		     
	    done;
	  done;
	  match a with
	    | [|
		[|v;0;w|];
		[|6;6;6|];
		[|x;y;z|];
	      |] -> (self#set_border_position px py (3+(t/3*20)))
	    | [|
		[|0;0;0|];
		[|0;6;z|];
		[|0;x;y|];
	      |] -> (self#set_border_position px py (11+(t/3*20)))
	    | [|
		[|0;0;0|];
		[|x;6;0|];
		[|y;z;0|];
	      |] -> (self#set_border_position px py (12+(t/3*20)))
	    | [|
		[|0;0;v|];
		[|0;6;z|];
		[|w;x;y|];
	      |] -> (self#set_border_position px py (5+(t/3*20)))
	    | [|
		[|v;0;0|];
		[|x;6;0|];
		[|y;z;w|];
	      |] -> (self#set_border_position px py (7+(t/3*20)))
	    | [|
		[|v;6;x|];
		[|0;6;y|];
		[|w;6;z|];
	      |] -> (self#set_border_position px py (1+(t/3*20)))
	    | [|
		[|x;y;z|];
		[|6;6;6|];
		[|v;0;w|];
	      |] -> (self#set_border_position px py (4+(t/3*20)))
	    | [|
		[|0;x;y|];
		[|0;6;z|];
		[|0;0;0|];
	      |] -> (self#set_border_position px py (10+(t/3*20)))
	    | [|
		[|y;z;0|];
		[|x;6;0|];
		[|0;0;0|];
	      |] -> (self#set_border_position px py (9+(t/3*20)))
	    | [|
		[|v;x;y|];
		[|0;6;z|];
		[|0;0;w|];
	      |] -> (self#set_border_position px py (8+(t/3*20)))
	    | [|
		[|y;z;v|];
		[|x;6;0|];
		[|w;0;0|];
	      |] -> (self#set_border_position px py (6+(t/3*20)))
	    | [|
		[|x;6;v|];
		[|y;6;0|];
		[|z;6;w|];
	      |] -> (self#set_border_position px py (2+(t/3*20)))
	    | _-> (self#set_border_position px py (-1)) ;
      ) 


    method  set_fow_border (fow_layer:int array array) px py f=
      let get_pos x y=
	fow_layer.(x).(y) 
      in
	if get_pos (px) (py)=f then (
	  let a=Array.make_matrix 3 3 (-1) in
	    for v=(-1) to 1 do
	      for w=(-1) to 1 do
		if (v+px)>0 && (w+py)>0 && (v+px)<rect#get_w && (w+py)<rect#get_h then (	       
		  let n=if (get_pos (v+px) (w+py))=1 then 2 else (get_pos (v+px) (w+py)) in
		    a.(w+1).(v+1)<-n;
		)		     
	      done;
	    done;
	    match a with
	      | [|
		  [|v;0;w|];
		  [|2;2;2|];
		  [|x;y;z|];
		|] -> (self#set_fow_border_position px py 3)
	      | [|
		  [|v;2;w|];
		  [|0;2;y|];
		  [|x;2;z|];
		|] -> (self#set_fow_border_position px py 2)
	      | [|
		  [|v;y;w|];
		  [|2;2;2|];
		  [|x;0;z|];
		|] -> (self#set_fow_border_position px py 4)
	      | [|
		  [|v;2;w|];
		  [|y;2;0|];
		  [|x;2;z|];
		|] -> (self#set_fow_border_position px py 1)
		  
	      | [|
		  [|0;0;0|];
		  [|0;2;z|];
		  [|0;y;v|];
		|] -> (self#set_fow_border_position px py 5)

	      | [|
		  [|0;0;x|];
		  [|0;2;z|];
		  [|w;y;v|];
		|] -> (self#set_fow_border_position px py 5)
	      | [|
		  [|0;0;0|];
		  [|z;2;0|];
		  [|v;y;0|];
		|] -> (self#set_fow_border_position px py 6)
	      | [|
		  [|w;0;0|];
		  [|z;2;0|];
		  [|v;y;x|];
		|] -> (self#set_fow_border_position px py 6)
		  
	      | [|
		  [|x;z;0|];
		  [|y;2;0|];
		  [|0;0;0|];
		|] -> (self#set_fow_border_position px py 7)

	      | [|
		  [|x;z;v|];
		  [|y;2;0|];
		  [|w;0;0|];
		|] -> (self#set_fow_border_position px py 7)
		  
	      | [|
		  [|0;z;x|];
		  [|0;2;y|];
		  [|0;0;0|];
		|] -> (self#set_fow_border_position px py 8)

	      | [|
		  [|v;z;x|];
		  [|0;2;y|];
		  [|0;0;w|];
		|] -> (self#set_fow_border_position px py 8)
		  
	      | _-> () ;
	) 
	  

    method update_all()=
      for i=0 to rect#get_w-1 do
	for j=0 to rect#get_h-1 do
	  self#set_border i j 6 9;
	  self#set_border i j 12 9;
	done;
      done;


    method update t vx vy vw vh=
      for i=vx to vx+vw do
	for j=vy to vy+vh do
	  if self#out_of_lay i j==false then (
	    self#set_border i j 6 t; 
	    self#set_border i j 12 t;	    
	 )
	done;
      done;

    method update_fow (fow_layer:int array array) vx vy vw vh=
      for i=vx to vx+vw do
	for j=vy to vy+vh do
	  if self#out_of_lay i j==false then (	    
	    fow_border_lay.(i).(j)<-(-1);
	 )
	done;
      done;
      for i=vx to vx+vw do
	for j=vy to vy+vh do
	  if self#out_of_lay i j==false then (	    
	    self#set_fow_border fow_layer i j 2; 
	    self#set_fow_border fow_layer i j 1; 
	 )
	done;
      done;

    method put (x:int) (y:int) (w:int) (h:int)=
      (* tile_put_rect btile x y w h *)
      tile_put btile x y;
    method to_value=
      let a={
	tiles=lay;
(*	nu=nu_lay;
	new_l=new_lay *)
	    } in
      a
    method from_value a=
      lay<-a.tiles;
(*      nu_lay<-a.nu;
      new_lay<-a.new_l *)
      
  end;;
