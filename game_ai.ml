(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open Object;;
open Game_object;;
open Game_player_layer;;

(** Game AI class definitions *)

class ai_actions=
object (self)
  method speak (w:string)=(*print_string ("AI SAYING : "^w);print_newline();*)();

  method train (player:game_player_layer) (b:string) (w:string) (t_func:int->int->string->unit)=
    self#speak("TRAIN UNIT");
    let f=ref 0 in
    for k=1 to player#get_cur_obj do
      if (player#get_obj k)#get_name=b then
	f:=k;
    done;	
    t_func player#get_num (!f) w;

  method plant (player:game_player_layer) (b:string) (w:string) (p_func:int->int->string->int->int->int) (pay_f:int->int->unit)=
    let f=ref 0 in
    for k=1 to player#get_cur_obj do
      if (player#get_obj k)#get_name=b then
	(
	 self#speak("PLANT"); 
	 let c=p_func player#get_num (k) w 1 1 in 
	 pay_f player#get_num (c/2);

	)
    done;	



  method attack (player:game_player_layer) (aplayer:game_player_layer) (w:string) (n:int) (a_func:int->int->int->int->unit)=
    self#speak("ATTACKING");
    (* x & y is an ennemy position to determine *)
    let x=ref 0 and y=ref 0 in
    let apo=aplayer in
    for i=1 to apo#get_cur_obj do
      if (apo#get_obj i)#get_name<>"none" then 
	( 
	  x:=(apo#get_obj i)#get_rect#get_x;
	  y:=(apo#get_obj i)#get_rect#get_y;
	 )

    done;
    let po=player in
    let nb=ref 0 in
    for i=1 to po#get_cur_obj do
      if (po#get_obj i)#get_name=w 
	  && (po#get_obj i)#get_state<>"move_for_attack" 
	  && (po#get_obj i)#get_state<>"attack"
	  && (po#get_obj i)#get_state<>"attack1"
	  && (po#get_obj i)#get_state<>"attack2"
	  && (po#get_obj i)#get_state<>"dead"
      then (
	
	if !nb<n then (
	  if !x<>0 || !y<>0 then (
(*	    print_string "AI:ATTACKING";print_newline(); *)
	   a_func (player#get_num) i !x !y;
	   nb:=!nb+1;
	   )
	 )
       )
    done;

  method move_one (player:game_player_layer) (x:int) (y:int) (dx:int) (dy:int)  (m_func:int->int->int->int->unit)=
    let on=player#get_obj_num x y in
    m_func (player#get_num) on dx dy;
    
  method move (player:game_player_layer)  (dx:int) (dy:int) (w:string) (n:int) (m_func:int->int->int->int->unit)=
    self#speak("MOVE");
    let po=player in
    let nb=ref 0 in
    for i=1 to po#get_cur_obj do
      if (po#get_obj i)#get_name=w 
	  && (po#get_obj i)#get_state<>"move" 
	  && (po#get_obj i)#get_state<>"move_for_attack" 
	  && (po#get_obj i)#get_state<>"attack"
	  && (po#get_obj i)#get_state<>"attack1"
	  && (po#get_obj i)#get_state<>"attack2"
	  && (po#get_obj i)#get_state<>"dead" 
      then (
	if !nb<n then (
	  m_func (player#get_num) i dx dy;
	  nb:=!nb+1;
	 )
       )
    done;

  method get_unit (player:game_player_layer) (ft:string)=
    let f=ref 0 in
    for k=1 to player#get_cur_obj do
      if (player#get_obj k)#get_name=ft then
	f:=k;
    done;
    !f

  method build_first (player:game_player_layer) (c_func:int->int->game_object->unit) (gc_func:int->int->game_object) (can_cons_func:int->int->int->bool) (got:string->game_object) (m_func:int->int->int->int->unit)=
    if player#get_construct_requested=0 && player#get_construct_now==false then (
    self#speak("BUILD FIRST");
    let get_b p=
      match player#get_name with
	| "rebel" -> "taverne"
	| "neonaz" -> "gymnaz"
	| _ -> "none" in
      c_func player#get_num (-1) (got (get_b player#get_num));		 
      player#set_construct_requested (-1);	      
      let co=gc_func player#get_num (-1) in

    let r=ref false in
      let r=ref false in
      for i=0 to player#get_w-1 do
	for j=0 to player#get_h-1 do
	  if (player#get_fow_position i j)=2 then (
	    if can_cons_func player#get_num i j==true then (
	      r:=true;
	      player#set_when_constructed (function()->(
		co#move i j; 
 		player#set_construct_now true;	      
	       ));
	      
	     )
	   )
	done;
      done;
      let s=
      match player#get_name with
	| "rebel" -> "zork"
	| "neonaz" -> "brad_lab"
	| _ -> "none" in
      
      let un=self#get_unit player s in
      let u=player#get_obj un in
      let i=u#get_rect#get_x and j=u#get_rect#get_y in
      if !r==false then (
	if i>0
	    && j>0
	    && i<player#get_w-1
	    && j<player#get_h-1 then  (
	  let rx=randomize 3 in     
	  let ry=randomize 3 in     
	  self#move player (i+(rx-1)) (j+(ry-1)) s 1 m_func;
	 )	    
       )
      else (
	u#act_stop();
	u#set_state "idle";
       )
)
  method build (player:game_player_layer) (c_func:int->int->game_object->unit) (gc_func:int->int->game_object) (can_cons_func:int->int->int->bool) (got:string->game_object) (m_func:int->int->int->int->unit) (ft:string) (n:string)=
    

    let f=ref 0 in
    for k=1 to player#get_cur_obj do
      if (player#get_obj k)#get_name=ft then
	f:=k;
    done;	


   if player#get_construct_requested=0 && player#get_construct_now==false then (       
     c_func player#get_num (!f) (got n);		 
     self#speak("BUILD");
    );
    player#set_construct_requested (!f);	      
    let co=gc_func player#get_num (!f) in
      let r=ref false in
      for i=0 to player#get_w-1 do
	for j=0 to player#get_h-1 do
	  if (player#get_fow_position i j)=2 then (
	    if can_cons_func player#get_num i j==true then (
	      r:=true;
	      player#set_when_constructed (function()->(
		  co#move i j; 
 		  player#set_construct_now true;	      		 
	       ))	      
	     ))
	done;
      done;

      let s=
      match player#get_name with
	| "rebel" -> "zork"
	| "neonaz" -> "brad_lab"
	| _ -> "none" in
      if !r==false then (
      for k=1 to player#get_cur_obj do
	if (player#get_obj k)#get_name=s then (
	  let u=(player#get_obj k) in
	  let i=u#get_rect#get_x and j=u#get_rect#get_y in
	if i>0
	    && j>0
	    && i<player#get_w-1
	    && j<player#get_h-1 then  (
	  let rx=(randomize 3) in     
	  let ry=(randomize 3) in     
	  if (player#is_object (i+(rx-1)) (j+(ry-1)))==false then 
	    m_func (player#get_num) k (i+(rx-1)) (j+(ry-1)); 
	 )
	 )
      done;
       );       

end;;



class ai_events=
object

  method is_unit_at (player:game_player_layer) (x:int) (y:int)=
    player#is_object x y;
  method have_less_units (player:game_player_layer) (n:int)=
      let u=ref 0 in
      let po=player in
      for i=1 to po#get_cur_obj do
	u:=!u+1;
      done;
      if !u<=n then true else false

  method have_less_units_of_type (player:game_player_layer) (n:int) (t:string)=
      let u=ref 0 in
      let po=player in
      for i=1 to po#get_cur_obj do
	if (po#get_obj i)#get_name=t then
	  u:=!u+1;
      done;
      if !u<=n then true else false

  method have_more_units_of_type (player:game_player_layer) (n:int) (t:string)=
      let u=ref 0 in
      let po=player in
      for i=1 to po#get_cur_obj do
	if (po#get_obj i)#get_name=t then
	  u:=!u+1;
      done;
      if !u>=n then true else false

  method have_less_resource (player:game_player_layer) (n:int) (t:string)=
    if player#resource_get t < n then true else false
  method have_more_resource (player:game_player_layer) (n:int) (t:string)=
    if player#resource_get t > n then true else false

end;;

class ai_rules=
object
   val mutable rules=Array.make 100 (
     function()->())
  val mutable c_rule=1    
  val mutable up_tot=20
  val mutable up=0
  method add_rule (e:unit->bool) (a:unit->unit)=
    rules.(c_rule)<-(function()->(if e() then a()));
    c_rule<-c_rule + 1;

  method check_rules()=
    if up=up_tot then ( 
      for i=1 to c_rule do
	let exe=rules.(i) in
	exe();      
      done;
      up<-0
     )
    else
      up<-up+1;

end;;

class game_ai=
object
  inherit ai_actions
  inherit ai_events
  inherit ai_rules
end;;




