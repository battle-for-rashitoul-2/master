#
# sample Makefile for Objective Caml
# Copyright (C) 2001 Jean-Christophe FILLIATRE
# Modified for BFR by Julien BOULNOIS 2003
#
# This library is free software; you can redistribute it and/or
# modify it under the terms of the GNU Library General Public
# License version 2, as published by the Free Software Foundation.
# 
# This library is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# 
# See the GNU Library General Public License version 2 for more details
# (enclosed in the file LGPL).

# where to install the binaries
prefix=/usr/local
exec_prefix=${prefix}
BINDIR=${exec_prefix}/bin

# where to install the man page
MANDIR=${prefix}/man

# other variables set by ./configure
OCAMLC   = ocamlc
OCAMLOPT = ocamlopt
OCAMLDEP = ocamldep
OCAMLLEX = ocamllex
OCAMLYACC= ocamlyacc
OCAMLLIB = /usr/local/lib/ocaml
OCAMLBEST= opt
OCAMLVERSION = 3.08.1
OCAMLWEB = true
OCAMLWIN32 = no
EXE = 
CC = gcc

INCLUDES = 
BFLAGS =  -g $(INCLUDES) 
OFLAGS = $(INCLUDES) -I bfrlib -I locales
CFLAGS = -Ibfrlib/SDL_gfx  -I/usr/include/SDL -D_REENTRANT

# main target
#############

NAME = bfr$(EXE)

all: $(OCAMLBEST)

# native-code compilation
######################################
BFRLIB = bfrlib/common.c bfrlib/video.c bfrlib/fading.c bfrlib/audio.c bfrlib/event.c bfrlib/time.c bfrlib/movie.c 
BFRLIB_OBJS=$(BFRLIB:.c=.o)

BFRLIB_ML=bfrlib/rect.ml bfrlib/file.ml bfrlib/config.ml bfrlib/video.ml bfrlib/vfs.ml bfrlib/object.ml bfrlib/music.ml bfrlib/event.ml bfrlib/cursors.ml bfrlib/interface.ml bfrlib/iface_event.ml bfrlib/stage.ml bfrlib/network.ml
BFRLIB_CMX=$(BFRLIB_ML:.ml=.cmx)

LOCALES_ML = locales/locale.ml locales/fr.ml
LOCALES_CMX=$(LOCALES_ML:.ml=.cmx)

ML = game_version.ml game_main.ml game_cursors.ml game_interface.ml game_visual.ml game_select.ml game_object.ml game_object_type.ml game_layer.ml game_tile_layer.ml game_object_layer.ml game_player_type.ml game_player_layer.ml game_ai.ml game_help.ml game_map.ml game_network.ml game_init.ml game_event.ml game_stage.ml bfr.ml
CMX = $(ML:.ml=.cmx)

PLAYER=version.ml rect.ml file.ml config.ml video.ml player_main.ml object.ml music.ml event.ml interface.ml iface_event.ml player.ml
PLAYER_CMX=$(PLAYER:.ml=.cmx)

GENERATED = game_version.ml

opt: $(NAME)

%.o: %.c
	$(CC) -c -I$(OCAMLLIB) $(CFLAGS) $< -o $@

locales: $(LOCALES_CMX)

bfrlib.cmxa: $(BFRLIB_OBJS) 
	ar rc bfrlib/libbfrlib_stubs.a $^
	ranlib bfrlib/libbfrlib_stubs.a
	$(OCAMLOPT) -a -cc $(CC) -ccopt -Lbfrlib -cclib -lbfrlib_stubs -cclib " -L/usr/lib -lSDL -lpthread -lSDL_image -lSDL_mixer -lSDL_ttf -lsmpeg" $(OFLAGS) bfrlib/SDL_gfx/.libs/libSDL_gfx.a -I bfrlib -o bfrlib/$@ bfrlib/low.ml $(BFRLIB_ML)


# linux only?
game_network.cmx: game_network.ml
	$(OCAMLOPT) -thread -c $(OFLAGS) $<

$(NAME).static: bfrlib.cmxa $(LOCALES_CMX) $(CMX)
	$(OCAMLOPT) -thread -ccopt -static -cclib "-L/usr/X11R6/lib -L/usr/lib -L/usr/local/lib -lstdc++ -lpng12 -ltiff -lfreetype -lttf -lz -ljpeg -lvorbisfile -lvorbis -logg -lX11 -lXext -lXv -lasound" -cclib " -L/usr/lib -lSDL -lpthread -lSDL_image -lSDL_mixer -lSDL_ttf -lsmpeg" -ccopt -Lbfrlib -I bfrlib $(OFLAGS) unix.cmxa str.cmxa threads.cmxa $^ -o $@

$(NAME): bfrlib.cmxa $(LOCALES_CMX) $(CMX) 
	$(OCAMLOPT) -thread -ccopt -Lbfrlib $(OFLAGS) unix.cmxa str.cmxa threads.cmxa $^ -o $@

init_config: bfrlib.cmxa
	$(OCAMLOPT) -ccopt -Lbfrlib -I bfrlib bfrlib.cmxa unix.cmxa str.cmxa $^ -o $@

player: bfrlib.cmxa  $(PLAYER_CMX)
	$(OCAMLOPT) -ccopt "-Lbfrlib" -I bfrlib $(OFLAGS) -o $@ unix.cmxa str.cmxa $^

VERSION=DEMOpre3

game_version.ml: Makefile
	echo "let version = \""$(VERSION)"\"" > game_version.ml
	echo "let date = \""`date`"\"" >> game_version.ml

# generic rules
###############

.SUFFIXES: .mli .ml .cmi .cmo .cmx .mll .mly .tex .dvi .ps .html

.mli.cmi:
	$(OCAMLC) -c $(BFLAGS) $<

.ml.cmo:
	$(OCAMLC) -c $(BFLAGS) $<

.ml.o:
	$(OCAMLOPT) -c $(OFLAGS) $<

.ml.cmx:
	$(OCAMLOPT) -c $(OFLAGS) $<

.mll.ml:
	$(OCAMLLEX) $<

.mly.ml:
	$(OCAMLYACC) -v $<

.mly.mli:
	$(OCAMLYACC) -v $<

.tex.dvi:
	latex $< && latex $<

.dvi.ps:
	dvips $< -o $@ 

.tex.html:
	hevea $<

# Emacs tags
############

tags:
	find . -name "*.ml*" | sort -r | xargs \
	etags "--regex=/let[ \t]+\([^ \t]+\)/\1/" \
	      "--regex=/let[ \t]+rec[ \t]+\([^ \t]+\)/\1/" \
	      "--regex=/and[ \t]+\([^ \t]+\)/\1/" \
	      "--regex=/type[ \t]+\([^ \t]+\)/\1/" \
              "--regex=/exception[ \t]+\([^ \t]+\)/\1/" \
	      "--regex=/val[ \t]+\([^ \t]+\)/\1/" \
	      "--regex=/module[ \t]+\([^ \t]+\)/\1/"

# Makefile is rebuilt whenever Makefile.in or configure.in is modified
######################################################################

Makefile: Makefile.in config.status
	./config.status

config.status: configure
	./config.status --recheck

configure: configure.in
	autoconf 

# clean
#######

clean::
	rm -f *.cm[iox] *.o *~
	rm -f $(GENERATED) parser.output
	rm -f $(NAME).byte $(NAME).opt
	rm -f *.aux *.log $(NAME).tex $(NAME).dvi $(NAME).ps
	rm -f bfrlib/*.cm[iox] bfrlib/*.o bfrlib/*~	
	rm -fr bfrlib/*.cmxa bfrlib/*.a
	rm -fr locales/*.cm[iox] locales/*~ locales/*.o

dist-clean distclean:: clean
	rm -f Makefile config.cache config.log config.status

# depend
########

.depend depend:: $(GENERATED)
	rm -f .depend
	$(OCAMLDEP) $(INCLUDES) *.ml *.mli > .depend

include .depend
