(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open Rect;;
open Video;;
open Object;;
open Game_object;;
open Game_object_type;;
open Game_layer;;
open Game_object_layer;;
open Game_player_type;;

(** Game player layer class definition *)


type game_player_lay={
  mutable num:int;
  mutable name:string;
  mutable obj_lay:game_object_lay;
  mutable fow_lay:int array array;
  mutable vx:int;
  mutable vy:int;
  mutable dvx:int;
  mutable dvy:int;

};;

(* tech_tree : "building" [|(0,"can_build");(1,"can_plant");(2,"can_train")|] *)

(* player_num, player_name, w_map_size, h_map_size *)
class game_player_layer n (pt:#game_player_type) wi hi=
  object (self) 
    val mutable num=n
    val mutable obj_layer=new game_object_layer wi hi 250
    val mutable fow_layer=Array.make_matrix wi hi 0
    val mutable fow_layer_nu=Array.make_matrix wi hi false
    val mutable obj_constructed=new game_object_layer wi hi 50

    val mutable player_type=pt			     

    val mutable resources=let a=Hashtbl.create 2 in
      Hashtbl.add a "none" 0;
      for i=0 to (Array.length pt#get_resources-1) do
	Hashtbl.add a pt#get_resources.(i) 0
      done;
      a

      
    val mutable when_constructed=(function()->())
    val mutable when_constructing=(function()->())

    val mutable construct_requested=0	
    val mutable construct_now=false	

    val mutable train_now=false

    method get_name=player_type#get_name
    method set_name n=player_type#set_name n
	
    method set_train_now v=train_now <- v
    method get_train_now=train_now

    method set_when_constructed f = when_constructed<-f
    method get_when_constructed = when_constructed

    method set_when_constructing f = when_constructing<-f
    method get_when_constructing = when_constructing

    method request_construct n=construct_requested<-n;

    method get_construct_now=construct_now
    method set_construct_now c=construct_now<-c

    method get_construct_requested=construct_requested
    method set_construct_requested c=construct_requested<-c

    method foreach_map_object d=
      obj_layer#foreach_map_object d

    method foreach_object d=
      obj_layer#foreach_object d

    method foreach_object_constructed d=
      obj_constructed#foreach_object d


    method add_obj_constructed (from:int) obj=
      obj#set_life 0;
      obj#set_build_by from;
      obj_constructed#add_object obj;

    method del_obj_constructed num=
      obj_constructed#del_object num;

    method get_obj_cons_layer=obj_constructed

    method get_obj_constructed i=
      let o=obj_constructed#get_object i in
      (o#get_build_by,o);

    method get_c_obj_cons=
      obj_constructed#get_cur_obj

(*    method get_player_type=player_type *)

    method get_tech_tree=player_type#get_tech_tree

    method get_repr=player_type#get_repr
    method put_repr x y=player_type#get_repr#move x y;player_type#get_repr#put();

    method resource_add nm=Hashtbl.add resources nm 0
    method resource_set nm v=(Hashtbl.replace resources nm v)
    method resource_get nm=(Hashtbl.find resources nm)

    method set_num n=num<-n
    method get_num=num
    method print_num=print_int num

    method count_obj=
      let r=ref 0 in
      for i=1 to self#get_cur_obj do
	if (self#get_obj i)#get_name<>"none" then
	  r:= !r+1
      done;
      !r

    method clear_objs()=obj_layer#clear()

    method get_obj_layer=obj_layer 

    method add_obj obj=obj_layer#add_object obj
    method move_obj n x y=obj_layer#move n x y

    method get_obj n=obj_layer#get_object n
    method get_obj_by_position x y=obj_layer#get_object_by_position x y
    method get_obj_num x y=obj_layer#get_position x y

    method clean_position x y=obj_layer#set_position x y 0
    method update_obj k=obj_layer#update_obj k

    method get_cur_obj=obj_layer#get_cur_obj
    method is_object x y=obj_layer#is_object x y

    (* UNSAFE : only used in minimap *)
    method get_fow=
      fow_layer

    method add_dead k=
      obj_layer#del_object k

    method empty_dead_stack()=()
(*      obj_layer#empty_del_stack(); *)
(*      obj_constructed#empty_del_stack()  *)


    method get_fow_position x y=
      if obj_layer#out_of_lay x y==false then 
	(
	  fow_layer.(x).(y)
	)
      else
	(
	 obj_layer#print_para  "player_layer:get_fow_position" x y;
	  0
	)

    method get_fow_position_nu x y=
      if obj_layer#out_of_lay x y==false then 
	(
	  fow_layer_nu.(x).(y)
	)
      else
	(
	 obj_layer#print_para  "player_layer:get_fow_position" x y;
	  false
	)


    method refresh_force x y=
      self#set_fow_position_nu x y true;

    method set_fow_position x y v=
      self#set_fow_position_v x y v;
      self#set_fow_position_nu x y true;

    method set_fow_position_v x y v=
      if obj_layer#out_of_lay x y==false then (
	fow_layer.(x).(y)<-v;
      )
      else
	(
	 obj_layer#print_para  "player_layer:set_fow_position" x y;
	)   

    method set_fow_position_nu x y nu=
      if obj_layer#out_of_lay x y==false then(
	fow_layer_nu.(x).(y)<-nu;
      ) 
     else
	(
	 obj_layer#print_para  "player_layer:set_fow_position_nu" x y;
	)   

    method get_w=wi
    method get_h=hi



    method to_value vx vy dvx dvy=
     	{
	  num=num;
	  name=self#get_name;
	  obj_lay=obj_layer#to_value;
	  fow_lay=fow_layer;
	  vx=vx;
	  vy=vy;
	  dvx=dvx;
	  dvy=dvy;
	}

    method from_value_s a obj_types=
      obj_layer#from_value (a.obj_lay) obj_types; 
      fow_layer<-a.fow_lay;

    method from_value a obj_types=
      obj_layer#from_value (a.obj_lay) obj_types; 
      fow_layer<-a.fow_lay;

  end;;
