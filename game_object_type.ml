(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Object;;
open Game_object;;

(** Game object type class definition *)

(* object_types are dynamic for create unit of type *)
(* objects are static for get caracteristic *) 

class game_object_types=
  object
    val mutable object_types=let a=Hashtbl.create 2 in Hashtbl.add a "none" (function()->(new game_object "none" 32 32 "none" false false 1 1));a
    val mutable objects=let a=Hashtbl.create 2 in Hashtbl.add a "none" (new game_object "none" 32 32 "none" false false 1 1);a
    method add_object_type nm obj=
      if(Hashtbl.mem objects nm)==false then
	Hashtbl.add objects nm (obj());	  
      Hashtbl.add object_types nm obj
    method get_object_type nm=(Hashtbl.find object_types nm)()
    method get_object nm=(Hashtbl.find objects nm)
    method is_object_type nm=(Hashtbl.mem objects nm)
end;;

