(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open Video;;
open Event;;
open Object;;
open Game_cursors;;
open Game_object;;
open Game_map;;


(** Game event manager *)

let clear_ev_stack gm=
  gm#get_mul_selection#clear();
  while Stack.length !ev_a <> 0 do
    let a=(Stack.top !ev_a) in

    curs#move a.ex a.ey;
    
    if a.etype="mouse" && a.eval= "motion" then (
      gm#get_iface#mouseover a.ex a.ey;
     );

     if a.etype="mouse" && a.eval= "released" then (
      gm#get_iface#release a.ex a.ey;
     );
    
    if a.ex > (f_size_w 8) 
	&& a.ex < (f_size_w 782) 
	&& a.ey > (f_size_h 20) 
	&& a.ey < (f_size_h 424) then 
      (

       (* we have ennemy under the cursor *)
	(*if a.etype="mouse" && a.eval= "motion" then 
	 (
	  let dx=(a.ex/ !tile_w + gm#get_vrect#get_x/ !tile_w) and
	      dy=(a.ey/ !tile_h + gm#get_vrect#get_y/ !tile_h) in       

	    (* FIXME : must fix for multiplayer *)
	    if (gm#get_player_layer (if gm#get_iam=1 then 2 else 1))#is_object dx dy==true then
	      curs#set_state "can_attack" else curs#set_state "normal";
	    
	  
	 );
	*)

       (* game map event *)
       if a.ebut=2 then 
	 (
	  (gm#get_player_layer gm#get_iam)#set_construct_requested 0;
	 );

      
       if ((gm#get_player_layer gm#get_iam)#get_construct_requested)<>0 then 
	 (
	  let o=gm#get_construct gm#get_iam ((gm#get_player_layer gm#get_iam)#get_construct_requested) in
	  o#move (a.ex/ !tile_w + gm#get_vrect#get_x/ !tile_w) 
	    (a.ey/ !tile_h + gm#get_vrect#get_y/ !tile_w); 

	  if a.etype="mouse" && a.eval= "pressed" && a.ebut=0 then 	    
	    (
	     if (gm#can_construct_here gm#get_iam 
		   (a.ex/ !tile_w + gm#get_vrect#get_x/32) 
		   (a.ey/ !tile_h + gm#get_vrect#get_y/32))==true then
	       (gm#get_player_layer gm#get_iam)#set_construct_now true;		 

	    );
	 ) 
       else 
	 (

          (* multiselection *)
	  let msel=gm#get_mul_selection in
	  if get_mouse_button(1)==true then msel#set_pressed true
	  else msel#set_pressed false;
	  if msel#get_pressed==true then 
	    (
	     if msel#get_start_sel==false then 
	       (
		msel#get_rect#set_size a.ex a.ey;
		msel#get_rect#set_position a.ex a.ey;
		msel#set_start_sel true;
	       )
	     else 
	       (
		msel#get_rect#set_size a.ex a.ey;
	       )
	    )
	  else
	    (
	     if msel#get_start_sel==true then (
	       let rx=ref 0 and ry=ref 0 and rw=ref 0 and rh=ref 0 in
	       if msel#get_rect#get_w >= msel#get_rect#get_x then 
		 (rx:= msel#get_rect#get_x;rw:= msel#get_rect#get_w) 
	       else (rx:= msel#get_rect#get_w;rw:= msel#get_rect#get_x); 
	       if msel#get_rect#get_h >= msel#get_rect#get_y then 
		 (ry:= msel#get_rect#get_y;rh:= msel#get_rect#get_h) 
	       else (ry:= msel#get_rect#get_h;rh:= msel#get_rect#get_y);      
	       
	       rx:=!rx+ gm#get_vrect#get_x;
	       ry:=!ry+ gm#get_vrect#get_y;
	       rw:=!rw+ gm#get_vrect#get_x; 
	       rh:=!rh+ gm#get_vrect#get_y; 
	       
	       for i= !rx/32 to !rw/32 do
		 for j= !ry/32 to !rh/32 do
		   let num=gm#get_unit_num gm#get_iam i j in 
		   
		   if num>0 then (
		     let u=gm#get_unit gm#get_iam num in
		     if u#get_can_mulsel==true then (
		       gm#get_selection#add_selected num; 
		       u#play_snd "select" gm#get_vrect#get_x gm#get_vrect#get_y;
		      )
		    );
		 done;
	       done;
	       
	       msel#get_rect#set_position 0 0;
	       msel#set_start_sel false;
	      )
	    );     
      
         (* events from mouse *)

	  (* mouse pressed *)
	  if a.etype="mouse" && a.eval= "pressed" then 
	    (
	     (* button 0 pressed *)
	     if a.ebut=0 && gm#is_move_requested=false then 
	       (
		let num=(gm#get_unit_num gm#get_iam (a.ex/ !tile_w + gm#get_vrect#get_x/ !tile_h ) (a.ey/ !tile_w + gm#get_vrect#get_y/ !tile_h))
		in 
		if num<>0  then 
		  (
		   gm#get_selection#del_all_selected();
		   gm#get_selection#add_selected num;
		   let unit=gm#get_unit gm#get_iam num in
(*		   unit#on_select_action();  *)
		   unit#play_snd "select" gm#get_vrect#get_x gm#get_vrect#get_y;
		  )
		else
		  (
(*		   
		   for i=0 to gm#get_selection#get_nb_selected do
		     let u=gm#get_unit gm#get_iam (gm#get_selection#get_selected i) in
		     u#on_deselect_action();
		   done;
*)		   
		   gm#get_selection#del_all_selected();	         
		   
		   for i=1 to (gm#get_player_layer gm#get_iam)#get_obj_layer#get_cur_obj do
		     let u=gm#get_unit gm#get_iam i in
		     u#on_deselect_action();
		   done;
		  )
	       );   

	     (* button 2 pressed *)
	     if a.ebut=2 || gm#is_move_requested=true then (
	       gm#set_move_requested false;
	       let fx=ref 0 and fy=ref 0 and nx=ref 0 and ny=ref 0 in
	       if gm#get_selection#get_selected 0<>0 then
		 (
		   (* get first selected unit *)		   
		   let funit=gm#get_unit gm#get_iam (gm#get_selection#get_selected 0) in
		     fx:=funit#get_rect#get_x;
		     fy:=funit#get_rect#get_y;
		     
		  (* foreach selected unit *)
		  for n=0 to gm#get_selection#get_nb_selected do
		    (
		     if (gm#get_selection#get_selected n)<>0 then
		       (
			let dx=(a.ex/ !tile_w + gm#get_vrect#get_x/ !tile_w + !nx) and
			    dy=(a.ey/ !tile_h + gm#get_vrect#get_y/ !tile_h + !ny) in
			
			let cunit=gm#get_unit gm#get_iam (gm#get_selection#get_selected n) in
(*			nx:=cunit#get_rect#get_x - !fx;
			ny:=cunit#get_rect#get_y - !fy;
*)
			  nx:=n-(!ny*4) - 1;
			  if n mod 4=0 then ny:=n/4 + 1;
			(* out of map ? *)
			if dx>0 && dx<gm#get_rect#get_w && dy>0 && dy<gm#get_rect#get_h then 
			  (
			    if (gm#is_unit dx dy)=false then
			      (
				cunit#play_snd "move" gm#get_vrect#get_x gm#get_vrect#get_y; 
				if cunit#get_prect#get_x<8 && cunit#get_prect#get_x>(-8) 
				  && cunit#get_prect#get_y<8 && cunit#get_prect#get_y>(-8) then 
				    (
				      if (gm#get_resource_num dx dy)<>0 then
					(
					  if (gm#get_resource_by_position dx dy)#get_blocking==false then
					    gm#unit_move (gm#get_iam) (gm#get_selection#get_selected n) dx dy;
					)
				      else				
					gm#unit_move (gm#get_iam) (gm#get_selection#get_selected n) dx dy;
				    )
			      )
			    else 
			      (
				if (gm#get_unit_player dx dy)<>gm#get_iam then
				  (
				    let aplayer=gm#get_unit_player dx dy in
				    let pnum=gm#get_unit_num aplayer dx dy in
				      cunit#play_snd "attack" gm#get_vrect#get_x gm#get_vrect#get_y;
				      cunit#set_aunit pnum;
				      gm#unit_move_for_attack (gm#get_iam) (gm#get_selection#get_selected n) dx dy; 
				  )
			      )
			  )
		       )
		    )
		  done;
		 ) 
	     )
	    );
	 )
      )
    else
      (
(* game iface event *)
       

       
       if a.etype="mouse" && a.eval= "pressed" then (
	 if a.ex>(f_size_w 80) && a.ex<(f_size_w 152) && a.ey>(f_size_h 485) && a.ey<(f_size_h 557) then
	   (
	    let nx=int_of_float((float_of_int(a.ex - (f_size_w 80))/.72.0)*.float_of_int(gm#get_rect#get_w))*32
	    and ny=int_of_float((float_of_int(a.ey - (f_size_h 485))/.72.0)*.float_of_int(gm#get_rect#get_h))*32
     in
	    if nx<(gm#get_rect#get_w*32 - !scr_w) && ny<(gm#get_rect#get_h*32 - !scr_h)  then  
	      gm#get_vrect#set_position nx ny
	    else (
	      if(nx<(gm#get_rect#get_w*32 - !scr_w)) then
		gm#get_vrect#set_position nx  (gm#get_rect#get_h*32 - !scr_h - 1);
	      if (ny<(gm#get_rect#get_h*32 - !scr_h))  then  
       		gm#get_vrect#set_position (gm#get_rect#get_w*32 - !scr_w - 1) ny;
	      if (nx<(gm#get_rect#get_w*32 - !scr_w)==false) 
		  && (ny<(gm#get_rect#get_h*32 - !scr_h)==false)  then  
		gm#get_vrect#set_position (gm#get_rect#get_w*32 - !scr_w - 1) (gm#get_rect#get_h*32 - !scr_h - 1);
	     )
	   );
	 
	 gm#get_iface#click a.ex a.ey;
	);
              
       
      );

(* map scrolling *)
    if a.etype="mouse" && a.eval= "motion" then (

      curs#set_state "normal";
      if a.ex<5 then (motion_left:=1;curs#set_state "left") 
      else (motion_left:=0);
      if a.ey<5 then (motion_top:=1;curs#set_state "top") 
      else (motion_top:=0);
      if a.ex>( !scr_w-30) then (motion_right:=1;curs#set_state "right")
      else (motion_right:=0);
      if a.ey>( !scr_h-30) then (motion_bottom:=1;curs#set_state "bottom") 
      else (motion_bottom:=0);
      
     );


    if a.etype="mouse" && a.eval= "released" then (
      curs#set_state "normal";
     );

    if a.etype="mouse" && a.eval= "pressed" then (
      curs#set_state "clicked";
     );

    Stack.pop !ev_a;

  done;;

