(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open File;;
open Config;;
open Video;;
open Object;;
open Event;;
open Music;;
open Stage;;
open Interface;;
open Iface_event;;
open Network;;

open Game_main;;
open Game_select;;
open Game_object;;
open Game_event;;
open Game_map;;
open Game_stage;;


let serv_fun ic oc=() in

start_server serv_fun (get_my_addr()) 5000;;
