(*
    Battle For Rashitoul - The ultimate strategy/arcade game
    Copyright (C) 2003 POC 

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*)
open Low;;
open Rect;;
open Video;;
open Object;;
open Interface;;
open Game_object;;
open Game_layer;;
open Game_object_layer;;

(* FIXME : 
 - must contain a special color
*)

(* name techtree resources *)
class game_player_type c (tt : (string, (int * string) array) Hashtbl.t) (r : string array) iface_bg=
object (self)
  val mutable name=c
  val mutable resources=r
  val mutable repr=new graphic_object 12 12 (if c="none" then "none" else "medias/misc/"^ c ^".png") false false;
  val mutable tech_tree=tt

  method get_name=name
  method set_name n=name<-n
  method get_tech_tree=tech_tree
  method get_resources=resources
  method create_iface=new interface iface_bg 800 600
			 
  method get_repr=repr
  method put_repr x y=repr#move x y;repr#put();
			  
end
;;


class game_player_types=
  object
    val mutable player_types=let a=Hashtbl.create 2 in Hashtbl.add a "none" (function()->(new game_player_type "none" (let a=Hashtbl.create 2 in 
		     Hashtbl.add a "none" [|(0,"none")|];a)) [|"none"|] "none");a

    val mutable players=let a=Hashtbl.create 2 in Hashtbl.add a "none" (new game_player_type "none" (let a=Hashtbl.create 2 in 
		     Hashtbl.add a "none" [|(0,"none")|];a) [|"none"|] "none");a
    method add_player_type nm obj=
      if(Hashtbl.mem players nm)==false then
	Hashtbl.add players nm (obj());	  
      Hashtbl.add player_types nm obj
    method get_player_type nm=(Hashtbl.find player_types nm)()
    method get_player nm=(Hashtbl.find players nm)
    method is_player_type nm=(Hashtbl.mem players nm)
end;;

(* must be loaded in XML *)
let player_neonaz_type=function()->(new game_player_type "neonaz" 
		      (let a=Hashtbl.create 2 in 
			 Hashtbl.add a "none" [|(0,"gymnaz")|];
			 Hashtbl.add a "gymnaz" [|(0,"labo");(0,"caserne");(2,"brad_lab")|];
			 Hashtbl.add a "labo" [|(1,"pollution")|];
			 Hashtbl.add a "caserne" [|(2,"patrouilleur");(2,"mitrailleur");(2,"bazooka");(0,"mirador");(0,"usine_vehicule")|];
			 Hashtbl.add a "usine_vehicule" [|(2,"super_guerrier")|];
			 Hashtbl.add a "mirador" [||];
			 a) 
  [|"pollution";"boucherie"|] 
  "medias/interface/bgs/interface_neonaz_800x600.png");;

let player_rebel_type=function()->(new game_player_type "rebel" 
		 (let a=Hashtbl.create 2 in 
		    Hashtbl.add a "none" [|(0,"taverne")|];
		    Hashtbl.add a "taverne" [|(0,"ferme");(0,"festival");(2,"zork")|];
		    Hashtbl.add a "ferme" [|(1,"nature")|];
		    Hashtbl.add a "festival" [|(2,"lance_pepino");(2,"punkette");(2,"guerrier_bud");(0,"tourelle");(0,"squat")|];
		    Hashtbl.add a "squat" [|(2,"abdel")|];
		    Hashtbl.add a "tourelle" [||];
		    a) 
  [|"nature";"boucherie"|]
  "medias/interface/bgs/interface_rebel_800x600.png");;


let player_types=new game_player_types;;
(* for now only 2 type *)
player_types#add_player_type "rebel" player_rebel_type;;
player_types#add_player_type "neonaz" player_neonaz_type;;


(* some useful functions *)



